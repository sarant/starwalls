uniform float viewportDist;
uniform vec3 limit;

uniform float renderZ;
attribute vec4 pos;

varying float scaleFactor;

void main() {
    float regZ = max(-0.75 * viewportDist , pos.z - renderZ);
    scaleFactor = viewportDist / (viewportDist + regZ);

    gl_Position = vec4(
         (scaleFactor * pos.x) / limit.x,
        -(scaleFactor * pos.y) / limit.y,
        regZ / limit.z,
        1.0
    );

    gl_PointSize = pos.w;
}
