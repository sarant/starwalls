varying float scaleFactor;

void main() {
    float greyLevel = min(scaleFactor, 1.0);
    greyLevel = greyLevel * greyLevel * (3.0 - 2.0 * greyLevel);

    float alphaScale = max(1.0, scaleFactor);
    float alphaBase = 1.0 / (1.0 + (alphaScale - 1.0) * (alphaScale - 1.0));
    float alphaLevel = (alphaBase * alphaBase) * (alphaBase * alphaBase);

    if (alphaLevel < 0.05) {
        alphaLevel = 0.0;
    }

    gl_FragColor = vec4(greyLevel, greyLevel, greyLevel, alphaLevel);
}
