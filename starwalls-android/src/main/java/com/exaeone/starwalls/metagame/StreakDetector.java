package com.exaeone.starwalls.metagame;

import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.Game;
import com.exaeone.starwalls.pool.DefaultPool;

/**
 * Created by kathryn on 29/03/15.
 */
public class StreakDetector implements AchievementHandler {

    public void onEvent(Game.OnStreakChanged evt) {
        int streak = evt.getStreak();

        switch (streak) {
            case 10:
                EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.Streak10), this);
                break;
            case 25:
                EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.Streak25), this);
                break;
            case 50:
                EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.Streak50), this);
                break;
        }

        // MESSAGING

        if (streak > 1) {
            EventBus.DEFAULT.post(DefaultPool.obtain(Game.OnTextMessage.class)
                    .setDisplayTime(1000)
                    .setMessage(streak + " walls streak! <span style=\"color: lime; font-weight: 700\">(+" + evt.getBonus() + ")</span>"), this);
        }
    }

    @Override
    public void reset() {

    }
}
