package com.exaeone.starwalls.metagame;

import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.Game;

public class FirstWallDeathDetector implements AchievementHandler {
    private int wallsPassed = 0;

    public void onEvent(Game.OnWallPassed evt) {
        wallsPassed++;
    }

    public void onEvent(Game.OnDeath evt) {
        // Technically one "passes through" a wall when one is destroyed by it.
        // Therefore, meeting the second wall disqualifies you from this achievement.
        if (wallsPassed < 2) {
            EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.FirstWallDeath), this);
        }
    }

    @Override
    public void reset() {
        wallsPassed = 0;
    }
}
