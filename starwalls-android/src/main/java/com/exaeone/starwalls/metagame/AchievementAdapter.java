package com.exaeone.starwalls.metagame;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.R;

/**
 * Created by kathryn on 28/03/15.
 */
public class AchievementAdapter extends BaseAdapter {

    private final Achievement[] achievements = Achievement.values();
    private Drawable emptyDrawable;

    public AchievementAdapter() {
        Resources res = App.getContext().getResources();
        emptyDrawable = res.getDrawable(R.drawable.empty);
    }

    @Override
    public int getCount() {
        return achievements.length;
    }

    @Override
    public Object getItem(int position) {
        return achievements[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Resources res = App.getContext().getResources();

        RowView rowView;
        if (convertView == null) {
            rowView = new RowView(parent.getContext());
        }
        else {
            rowView = (RowView) convertView;
        }

        Achievement achievement = achievements[position];

        if (App.getDataManagers().getAchievementManager().isAchieved(achievement)) {
            rowView.title.setTextColor(res.getColor(R.color.achievement_unlocked_title));
            rowView.title.setText(achievement.nameResource);

            rowView.detail.setTextColor(res.getColor(R.color.achievement_unlocked_descr));
            rowView.detail.setText(achievement.descriptionResource);

            rowView.icon.setImageResource(achievement.iconResource);
        }
        else {
            rowView.title.setTextColor(res.getColor(R.color.achievement_locked));
            rowView.title.setText(R.string.achievement_locked);

            rowView.detail.setTextColor(res.getColor(R.color.achievement_locked));
            rowView.detail.setText(achievement.descriptionResource);

            rowView.icon.setImageDrawable(emptyDrawable);
        }

        return rowView;
    }

    @Override
    public boolean isEnabled(int position) {
            return false;
    }


    private static class RowView extends LinearLayout {

        private TextView title;
        private TextView detail;
        private ImageView icon;

        public RowView(Context context) {
            super(context);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.achievement_row, this, true);

            title = (TextView) this.findViewById(R.id.title);
            detail = (TextView) this.findViewById(R.id.detail);
            icon =(ImageView) this.findViewById(R.id.icon);
        }

    }
}
