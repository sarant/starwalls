package com.exaeone.starwalls.metagame;

import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.Game;
import com.exaeone.starwalls.pool.DefaultPool;

/**
 * Created by kathryn on 29/03/15.
 */
public class ShieldStatusDetector implements AchievementHandler {

    boolean shieldsHaveBeenLow = false;
    boolean shieldsFull = true;

    public void onEvent(Game.OnWallPassed evt) {

        float damage = evt.getDamage();

        if (damage < 0.01 && damage > 0) {
            EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.NearHit), this);
        }

        //ACHIEVEMENT

        float shields = evt.getShield();

        if (shields < 0.3) {
            shieldsHaveBeenLow = true;
        }

        if (shields == 1.0) {
            shieldsFull = true;
        }
        else {
            shieldsFull = false;
        }

        if (shieldsFull && shieldsHaveBeenLow) {
            EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.BackFromDead), this);
        }

        // MESSAGING

        if (shields < 0.3 && shields > 0) {
            EventBus.DEFAULT.post(DefaultPool.obtain(Game.OnTextMessage.class)
                    .setDisplayTime(0)
                    .setMessage("<span style=\"color: yellow; font-weight: 700\">SHIELDS LOW</span>"), this);
        }
        else if (shields == 0) {
            EventBus.DEFAULT.post(DefaultPool.obtain(Game.OnTextMessage.class)
                    .setDisplayTime(0)
                    .setMessage("<span style=\"color: red; font-weight: 700\">SHIELDS FAILED!</span>"), this);
        }
        else {
            EventBus.DEFAULT.post(DefaultPool.obtain(Game.OnTextMessage.class)
                    .setDisplayTime(0)
                    .setMessage(""), this);
        }

    }

    @Override
    public void reset() {
        shieldsHaveBeenLow = false;
        shieldsFull = true;
    }

}
