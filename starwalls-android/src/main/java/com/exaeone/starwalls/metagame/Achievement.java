package com.exaeone.starwalls.metagame;

import com.exaeone.starwalls.R;
import com.exaeone.starwalls.event.Event;

public enum Achievement {

    FirstWallDeath (0, R.drawable.planetanymore,
            R.string.firstwalldeath_title, R.string.firstwalldeath_descr, FirstWallDeathDetector.class),

    BarrelRoll (1, R.drawable.barrelroll,
                    R.string.barrelroll_title, R.string.barrelroll_descr, BarrelRollDetector.class),

    BackFromDead (2, R.drawable.backfromdead,
            R.string.backfromdead_title, R.string.backfromdead_descr, ShieldStatusDetector.class),

    NearHit (3, R.drawable.nearhit,
            R.string.nearhit_title, R.string.nearhit_descr, ShieldStatusDetector.class),

    Streak10 (4, R.drawable.tenwallstreak,
            R.string.streak10_title, R.string.streak10_descr, StreakDetector.class),

    Streak25 (5, R.drawable.twentyfivewallstreak,
            R.string.streak25_title, R.string.streak25_descr, StreakDetector.class),

    Streak50 (6, R.drawable.fiftywallstreak,
            R.string.streak50_title, R.string.streak50_descr, StreakDetector.class),

    HundredWalls (7, R.drawable.hundredwalls,
            R.string.hundredwalls_title, R.string.hundredwalls_descr, WallCountDetector.class),

    FiftyWalls (8, R.drawable.fiftywalls,
                  R.string.fiftywalls_title, R.string.fiftywalls_descr, WallCountDetector.class),

    TwentyWalls (9, R.drawable.twentywalls,
                  R.string.twentywalls_title, R.string.twentywalls_descr, WallCountDetector.class);

    // This is persisted on the phone, so must remain the same even if the icons or name or
    // description change.
    public final int permanentId;
    public final int iconResource;
    public final int nameResource;
    public final int descriptionResource;
    public final Class<? extends AchievementHandler> handlerClass;

    Achievement (int permanentId, int iconResource, int nameResource, int descriptionResource, Class handlerClass) {
        this.iconResource = iconResource;
        this.nameResource = nameResource;
        this.permanentId = permanentId;
        this.descriptionResource = descriptionResource;
        this.handlerClass = handlerClass;
    }

    public static class OnAchieved extends Event {
        public Achievement achievement = null;

        @Override
        public void onRecycle() {
            this.achievement = null;
            super.onRecycle();
        }

        public OnAchieved(Achievement achievement) {
            this.achievement = achievement;
        }
    }

}
