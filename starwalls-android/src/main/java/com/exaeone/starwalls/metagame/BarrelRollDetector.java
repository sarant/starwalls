package com.exaeone.starwalls.metagame;

import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.Game;

/**
 * Created by kathryn on 29/03/15.
 */
public class BarrelRollDetector implements AchievementHandler {

    private static final double TWO_PI = 2 * Math.PI;

    private float previousRotation = Float.MIN_VALUE;
    private double integratedAngle = 0;

    public void onEvent(Game.OnRotation evt) {

        float currentRotation = evt.getRoll();

        if (previousRotation != Float.MIN_VALUE) {
            double deltaAngle = normalizeDelta(currentRotation - previousRotation);
            integratedAngle += deltaAngle;
        }

        previousRotation = currentRotation;

        if (Math.abs(integratedAngle) > TWO_PI) {
            EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.BarrelRoll),this);
        }
    }

    public void onEvent(Game.OnWallPassed evt) {
        float damage = evt.getDamage();
        if(damage > 0){
            integratedAngle = 0;
        }
    }

    private double normalizeDelta(double angle) {
        double normalized = angle;
        while (normalized < -Math.PI) normalized += TWO_PI;
        while (normalized >= Math.PI) normalized -= TWO_PI;
        return normalized;
    }

    @Override
    public void reset() {
        integratedAngle = 0;
        previousRotation = Float.MIN_VALUE;
    }

}
