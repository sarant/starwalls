package com.exaeone.starwalls.metagame;

import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.Game;

public class WallCountDetector implements AchievementHandler {

    private int wallsPassed = 0;

    public void onEvent(Game.OnWallPassed evt) {
        wallsPassed++;

        switch (wallsPassed){
            case 20:
                EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.TwentyWalls), this);
                break;
            case 50:
                EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.FiftyWalls), this);
                break;
            case 100:
                EventBus.DEFAULT.post(new Achievement.OnAchieved(Achievement.HundredWalls), this);
                break;
        }

    }

    @Override
    public void reset() {
        wallsPassed = 0;
    }

}
