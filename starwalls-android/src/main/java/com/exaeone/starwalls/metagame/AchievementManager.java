package com.exaeone.starwalls.metagame;

import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.Game;
import com.exaeone.starwalls.app.PersistentStorage;
import com.exaeone.starwalls.game.GameExecutor;
import com.exaeone.starwalls.pool.DefaultPool;

import java.util.*;

/*
 * Ensures that achievement handlers are alive and listening when they need to be.
 */
public class AchievementManager {

    private final PersistentStorage persistentStorage;

    private final List<Boolean> isAchieved = new ArrayList<>();
    private final Map<Class<? extends AchievementHandler>, Boolean> handlersRequired = new HashMap<>();
    private final Map<Class<? extends AchievementHandler>, AchievementHandler> achievementHandlers = new HashMap<>();

    public AchievementManager(PersistentStorage persistentStorage) {
        this.persistentStorage = persistentStorage;
        init();
        EventBus.DEFAULT.subscribe(this);
    }

    private void init() {
        for (Achievement a : Achievement.values()) {
            if (a.permanentId != isAchieved.size()) {
                throw new RuntimeException("There is a mismatch between the position of the achievement " + a.name() + " in the Enum and its permanentID");
            }

            isAchieved.add(persistentStorage.getBoolean(getAchievedKeyForAchievement(a)));
        }
        invalidateHandlersList();
    }

    public boolean isAchieved(Achievement a) {
        return this.isAchieved.get(a.permanentId);
    }

    public void onEvent(GameExecutor.OnStart e) {
        resetHandlers();
    }

    public void onEvent(Achievement.OnAchieved evt) {
        if (!isAchieved.get(evt.achievement.permanentId)) {
            toggle(evt.achievement);
            EventBus.DEFAULT.post(DefaultPool.obtain(Game.OnTextMessage.class)
                    .setDisplayTime(2000)
                    .setMessage("<span style=\"color: lime; font-weight: 700\">ACHIEVEMENT!</span>"), this);
            EventBus.DEFAULT.post(DefaultPool.obtain(OnNewAchievement.class),this);
        }
    }

    private void resetHandlers() {
        for (AchievementHandler handler : achievementHandlers.values()) {
            handler.reset();
        }
    }

    private void invalidateHandlersList() {
        handlersRequired.clear();

        for (Achievement a : Achievement.values()) {
            Boolean alreadyRequired = handlersRequired.get(a.handlerClass);

            if (alreadyRequired == null) {
                handlersRequired.put(a.handlerClass, !isAchieved(a));
            }
            else if (!alreadyRequired && !isAchieved(a)) {
                handlersRequired.put(a.handlerClass, true);
            }
        }

        for (Class<? extends AchievementHandler> handlerClass : handlersRequired.keySet()) {
            boolean isRequired = handlersRequired.get(handlerClass);
            AchievementHandler handlerInstance = achievementHandlers.get(handlerClass);

            if (isRequired && handlerInstance == null) {
                try {
                    handlerInstance = handlerClass.newInstance();
                    EventBus.DEFAULT.subscribe(handlerInstance);
                    achievementHandlers.put(handlerClass, handlerInstance);
                }
                catch (InstantiationException e) {
                    throw new RuntimeException(e);
                }
                catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            else if (!isRequired && handlerInstance != null) {
                EventBus.DEFAULT.unsubscribe(handlerInstance);
                achievementHandlers.remove(handlerClass);
            }
        }
    }

    // For each achievement, this key maps to a status (how much of this achievement has been earned?)
    private String getStatusKeyForAchievement(Achievement achievement) {
        return achievement.permanentId + "_status";
    }

    // For each achievement, this key maps to a boolean (has this achievement been earned?)
    private String getAchievedKeyForAchievement(Achievement achievement) {
        return achievement.permanentId + "_achieved";
    }


    public void toggle(Achievement achievement){
        boolean isActive = isAchieved.get(achievement.permanentId);
        isAchieved.set(achievement.permanentId, !isActive);
        persistentStorage.putBoolean(getAchievedKeyForAchievement(achievement), !isActive);
        invalidateHandlersList();
    }

    public static class OnNewAchievement extends Event {
    }

}
