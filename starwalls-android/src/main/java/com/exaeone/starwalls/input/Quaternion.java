package com.exaeone.starwalls.input;

/**
 * Created by saran on 02/05/2014.
 */
public class Quaternion extends Vector {

	public Quaternion() {
		super(4);
	}

	public static Quaternion addInto(Quaternion q1, Quaternion q2, Quaternion out) {
		out.v[0] = q1.v[0] + q2.v[0];
		out.v[1] = q1.v[1] + q2.v[1];
		out.v[2] = q1.v[2] + q2.v[2];
		out.v[3] = q1.v[3] + q2.v[3];
		return out;
	}

	public Quaternion add(Quaternion q) {
		return addInto(q, this, this);
	}

	public static Quaternion multiplyInto(Quaternion q1, Quaternion q2, Quaternion out) {
		final float qr = q1.v[0] * q2.v[0] - q1.v[1] * q2.v[1] - q1.v[2] * q2.v[2] - q1.v[3] * q2.v[3];
		final float qi = q1.v[0] * q2.v[1] + q1.v[1] * q2.v[0] + q1.v[2] * q2.v[3] - q1.v[3] * q2.v[2];
		final float qj = q1.v[0] * q2.v[2] - q1.v[1] * q2.v[3] + q1.v[2] * q2.v[0] + q1.v[3] * q2.v[1];
		final float qk = q1.v[0] * q2.v[3] + q1.v[1] * q2.v[2] - q1.v[2] * q2.v[1] + q1.v[3] * q2.v[0];
		out.v[0] = qr;
		out.v[1] = qi;
		out.v[2] = qj;
		out.v[3] = qk;
		return out;
	}

	public Quaternion preMultiply(Quaternion q) {
		return multiplyInto(q, this, this);
	}

	public Quaternion postMultiply(Quaternion q) {
		return multiplyInto(this, q, this);
	}

	public void toRotationMatrix(float[] R) {
		R[0] = 1 - 2 * (v[2] * v[2] + v[3] * v[3]);
		R[1] = 2 * (v[1] * v[2] - v[0] * v[3]);
		R[2] = 2 * (v[1] * v[3] + v[0] * v[2]);

		R[3] = 2 * (v[1] * v[2] + v[0] * v[3]);
		R[4] = 1 - 2 * (v[1] * v[1] + v[3] * v[3]);
		R[5] = 2 * (v[2] * v[3] - v[0] * v[1]);

		R[6] = 2 * (v[1] * v[3] - v[0] * v[2]);
		R[7] = 2 * (v[2] * v[3] + v[0] * v[1]);
		R[8] = 1 - 2 * (v[1] * v[1] + v[2] * v[2]);
	}

	public Quaternion fromRotationMatrix(float[] R) {
		// From http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche52.html

		v[0] = (1 + R[0] + R[4] + R[8]) / 4;
		v[1] = (1 + R[0] - R[4] - R[8]) / 4;
		v[2] = (1 - R[0] + R[4] - R[8]) / 4;
		v[3] = (1 - R[0] - R[4] + R[8]) / 4;

		v[0] = (v[0] > 0) ? (float) Math.sqrt(v[0]) : 0;
		v[1] = (v[1] > 0) ? (float) Math.sqrt(v[1]) : 0;
		v[2] = (v[2] > 0) ? (float) Math.sqrt(v[2]) : 0;
		v[3] = (v[3] > 0) ? (float) Math.sqrt(v[3]) : 0;

		float max = v[0];
		float maxCpt = 0;
		for (int i = 1; i < 4; ++i) {
			if (v[i] > max) {
				max = v[i];
				maxCpt = i;
			}
		}

		if (maxCpt == 0) {
			v[1] = conditionalSignFlip(v[1], R[7] - R[5]);
			v[2] = conditionalSignFlip(v[2], R[2] - R[6]);
			v[3] = conditionalSignFlip(v[3], R[3] - R[1]);
		}
		else if (maxCpt == 1) {
			v[0] = conditionalSignFlip(v[0], R[7] - R[5]);
			v[2] = conditionalSignFlip(v[2], R[3] + R[1]);
			v[3] = conditionalSignFlip(v[3], R[2] + R[6]);
		}
		else if (maxCpt == 2) {
			v[0] = conditionalSignFlip(v[0], R[2] - R[6]);
			v[1] = conditionalSignFlip(v[1], R[3] + R[1]);
			v[3] = conditionalSignFlip(v[3], R[7] + R[5]);
		}
		else {
			v[0] = conditionalSignFlip(v[0], R[3] - R[1]);
			v[1] = conditionalSignFlip(v[1], R[2] + R[6]);
			v[2] = conditionalSignFlip(v[2], R[7] + R[5]);
		}

		return (Quaternion) this.normalize();
	}

	private static float conditionalSignFlip(float magnitude, double sign) {
		if (sign > 0) {
			return magnitude;
		}
		else if (sign < 0) {
			return -magnitude;
		}
		else {
			return 0;
		}
	}

	public Quaternion divideAngleInto(float n, Quaternion out) {
		double halfAngle = Math.acos(v[0]);
		double sineFactor = Math.sin(halfAngle / n) / Math.sin(halfAngle);

		if (Double.isNaN(sineFactor)) {
			return this.cloneInto(out);
		}

		out.v[0] = (float) Math.cos(halfAngle / n);
		out.v[1] = (float) (v[1] * sineFactor);
		out.v[2] = (float) (v[2] * sineFactor);
		out.v[3] = (float) (v[3] * sineFactor);

		return (Quaternion) out.normalize();
	}

	public Quaternion divideAngle(float n) {
		return divideAngleInto(n, this);
	}

	public float getAngle() {
		return (float) (2 * Math.acos(v[0]));
	}

	public Quaternion setAngleInto(float theta, Quaternion out) {
		double halfAngle = Math.acos(v[0]);
		double sineFactor = Math.sin(theta / 2) / Math.sin(halfAngle);

		if (Double.isNaN(sineFactor)) {
			return this.cloneInto(out);
		}

		out.v[0] = (float) Math.cos(theta / 2);
		out.v[1] = (float) (v[1] * sineFactor);
		out.v[2] = (float) (v[2] * sineFactor);
		out.v[3] = (float) (v[3] * sineFactor);

		return (Quaternion) out.normalize();
	}

	public Quaternion setAngle(float theta) {
		return setAngleInto(theta, this);
	}

	public Quaternion negateInto(Quaternion q) {
		q.v[0] = -v[0];
		q.v[1] = -v[1];
		q.v[2] = -v[2];
		q.v[3] = -v[3];
		return q;
	}

	public Quaternion negate() {
		return negateInto(this);
	}

	public Quaternion invertInto(Quaternion q) {
		q.v[0] = v[0];
		q.v[1] = -v[1];
		q.v[2] = -v[2];
		q.v[3] = -v[3];
		return q;
	}

	public Quaternion invert() {
		return invertInto(this);
	}

	public Quaternion canonicalizeInto(Quaternion q) {
		if (v[0] < 0) {
			q.v[0] = -v[0];
			q.v[1] = -v[1];
			q.v[2] = -v[2];
			q.v[3] = -v[3];
		}
		else {
			this.cloneInto(q);
		}
		return (Quaternion) q.normalize();
	}

	public Quaternion canonicalize() {
		return canonicalizeInto(this);
	}

	public Quaternion cloneInto(Quaternion q) {
		if (q == this) {
			return this;
		}
		else {
			q.v[0] = v[0];
			q.v[1] = v[1];
			q.v[2] = v[2];
			q.v[3] = v[3];
			return q;
		}
	}

	public Quaternion clone() {
		return cloneInto(new Quaternion());
	}

	public Quaternion slerpTo(Quaternion q, float t) {
		final float qr = q.v[0];
		final float qi = q.v[1];
		final float qj = q.v[2];
		final float qk = q.v[3];

		q.postMultiply(this.invert()).divideAngle(1f / t);
		this.invert().preMultiply(q);

		q.v[0] = qr;
		q.v[1] = qi;
		q.v[2] = qj;
		q.v[3] = qk;

		return this;
	}

	public Quaternion setUnit() {
		v[0] = 1;
		v[1] = 0;
		v[2] = 0;
		v[3] = 0;
		return this;
	}

}
