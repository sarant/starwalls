package com.exaeone.starwalls.input;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.pool.DefaultPool;

/**
 * Created by saran on 27/04/2014.
 */
public class SensorInput implements SensorEventListener {

	private static final String TAG = "SensorInput";
	private SensorManager sensorManager;

	private boolean started = false;
	private boolean calibrated;

    // These parameters tune the sensitivity of the sensor input
    private final float yawMultiplier = 5f;
    private final float pitchMultiplier = 5f;
    private final float rollMultiplier = 1f;

	private Quaternion zeroQuaternion = new Quaternion();
	private Quaternion positionQuaternion = new Quaternion();
	private float[] rotationMatrix = new float[9];
	private float[] orientation = new float[3];

	private float x;
	private float y;
	private float roll;

	public SensorInput(SensorManager sensorManager) {
		this.sensorManager = sensorManager;
        EventBus.DEFAULT.subscribe(this);
	}

	public synchronized void start() {
		if (!started) {
            recenter();
            sensorManager.registerListener(this,
                    sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR),
                    SensorManager.SENSOR_DELAY_GAME);
            Log.i(TAG, "Started");
			started = true;
		}
	}

	public synchronized void stop() {
		if (started) {
			sensorManager.unregisterListener(this);
			started = false;
            Log.i(TAG, "Stopped");
		}
	}

    public void recenter() {
        calibrated = false;
        x = 0;
        y = 0;
        roll = 0;
        positionQuaternion.setUnit();
        zeroQuaternion.setUnit();
        EventBus.DEFAULT.post(
                DefaultPool.obtain(OnUpdate.class)
                        .setPosition(0, 0, 0),
                this);
    }

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
            case (Sensor.TYPE_GAME_ROTATION_VECTOR): {
                processRotation(event);
                break;
            }
		}
	}

    private void processRotation(SensorEvent event) {

	    SensorManager.getQuaternionFromVector(positionQuaternion.v,  event.values);
		positionQuaternion.canonicalize();

	    if (!calibrated) {
		    positionQuaternion.invertInto(zeroQuaternion);
		    calibrated = true;
	    }

        positionQuaternion.toRotationMatrix(rotationMatrix);
        SensorManager.getOrientation(rotationMatrix, orientation);
        float roll = orientation[2];

	    positionQuaternion.preMultiply(zeroQuaternion).normalize();
		positionQuaternion.toRotationMatrix(rotationMatrix);
        SensorManager.getOrientation(rotationMatrix, orientation);
        float yaw = orientation[0];
        float pitch = orientation[1];

        x = yawMultiplier * yaw / (float) Math.PI;
        if (x > 1) {
            x = 1;
        }
        else if (x < -1) {
            x = -1;
        }

        y = pitchMultiplier * pitch / (float) Math.PI;
        if (y > 1) {
            y = 1;
        }
        else if (y < -1) {
            y = -1;
        }

        this.roll = rollMultiplier * roll;

        EventBus.DEFAULT.postSynchronously(
                DefaultPool.obtain(OnUpdate.class)
                        .setPosition(x, y, roll),
                this);
    }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

    public static class OnUpdate extends Event {
        public float x;
        public float y;
        public float roll;

        private OnUpdate setPosition(float x, float y, float roll) {
            this.x = x;
            this.y = y;
            this.roll = roll;
            return this;
        }
    }

}
