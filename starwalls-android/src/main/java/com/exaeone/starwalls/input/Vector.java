package com.exaeone.starwalls.input;

import java.util.Arrays;

/**
 * Created by saran on 04/05/2014.
 */
public class Vector {

	public final int length;
	public final float[] v;

	public Vector(int length) {
		this.length = length;
		v = new float[length];
		Arrays.fill(v, 0);
	}

	public Vector assignValues(float[] w) {
		for (int i = 0; i < length; ++i) {
			v[i] = w[i];
		}
		return this;
	}

	public Vector multiplyInto(float n, Vector out) {
		for (int i = 0; i < length; ++i) {
			out.v[i] = v[i] * n;
		}
		return out;
	}

	public Vector multiply(float n) {
		return multiplyInto(n, this);
	}

	public Vector divideInto(float n, Vector out) {
		for (int i = 0; i < length; ++i) {
			out.v[i] = v[i] / n;
		}
		return out;
	}

	public Vector divide(float n) {
		return divideInto(n, this);
	}

	public float norm() {
		double accum = 0;
		for (int i = 0; i < length; ++i) {
			accum += v[i] * v[i];
		}
		return (float) Math.sqrt(accum);
	}

	public Vector normalizeInto(Vector out) {
		return divideInto(this.norm(), out);
	}

	public Vector normalize() {
		return normalizeInto(this);
	}

	public float dot(Vector w) {
		if (w.length != length) {
			throw new IllegalArgumentException();
		}

		double accum = 0;
		for (int i = 0; i < length; ++i) {
			accum += v[i] * w.v[i];
		}

		return (float) accum;
	}

	public Vector projectInto(Vector w, Vector out) {
		double dotProduct = this.dot(w);
		double norm = w.norm();

		for (int i = 0; i < length; ++i) {
			out.v[i] = (float) (dotProduct * w.v[i] / norm);
		}
		return this;
	}

	public Vector project(Vector w) {
		return projectInto(w, this);
	}

	public Vector orthogInto(Vector w, Vector out) {
		double dotProduct = this.dot(w);
		double norm = w.norm();

		for (int i = 0; i < length; ++i) {
			out.v[i] = (float) (v[i] - dotProduct * w.v[i] / norm);
		}
		return out;
	}

	public Vector orthog(Vector w) {
		return orthogInto(w, this);
	}


}
