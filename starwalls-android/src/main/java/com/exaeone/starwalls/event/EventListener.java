package com.exaeone.starwalls.event;

/**
 * Created by saran on 23/06/2014.
 */
public interface EventListener<E extends Event> {

	void incomingEvent(E event);

}
