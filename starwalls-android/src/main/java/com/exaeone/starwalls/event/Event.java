package com.exaeone.starwalls.event;

import com.exaeone.starwalls.pool.Pool;
import com.exaeone.starwalls.pool.Poolable;
import com.exaeone.starwalls.pool.PoolableFactory;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saran on 23/06/2014.
 */
public abstract class Event extends Poolable {

	Object source = null;
	public final Object source() {
		return source;
	}

    @Override
    public void onObtain() {
        super.onObtain();
        source = null;
    }

    @Override
    public void onRecycle() {
        source = null;
        super.onRecycle();
    }
}
