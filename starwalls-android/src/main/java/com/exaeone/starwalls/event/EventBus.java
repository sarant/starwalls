package com.exaeone.starwalls.event;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.exaeone.starwalls.pool.DefaultPool;
import com.exaeone.starwalls.pool.Poolable;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by saran on 23/06/2014.
 */
public class EventBus {

	public static final EventBus DEFAULT = new EventBus();

    public static final String TAG = "EventBus";

    private class EventListener {
        final Method method;
        final WeakReference<Object> obj;

        EventListener(Method method, WeakReference<Object> obj) {
            this.method = method;
            this.obj = obj;
        }
    }

	private final ConcurrentHashMap<Class<? extends Event>, AtomicReference<ArrayList<EventListener>>> listenersMap = new ConcurrentHashMap<>();

    private Thread thread;
	private Handler handler;

	public EventBus() {
	}

	public void start() {
		thread = new Thread(new Runnable() {
			@Override
			public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE);
                Looper.prepare();
				handler = new Handler(Looper.myLooper()) {
					@Override
					public void handleMessage(Message msg) {
						EventBus.this.handleMessage(msg);
					}
				};
				Looper.loop();
			}
		}, "EventBus");
		thread.start();

        //noinspection StatementWithEmptyBody
        while (handler == null) {}
	}

	public void stop() {
		if (handler != null) {
			handler.getLooper().quitSafely();
			handler = null;
			thread = null;
		}
	}

	public void subscribe(Object listener) {
        final Method[] methods = listener.getClass().getDeclaredMethods();

        WeakReference<Object> weakListenerRef = new WeakReference<>(listener);

        for (final Method method : methods) {
            if (method.getName().equals("onEvent") && method.getReturnType().equals(void.class)) {
                final Class[] params = method.getParameterTypes();
                if (params.length == 1 && Event.class.isAssignableFrom(params[0])) {

                    //noinspection unchecked
                    Class<? extends Event> eventClass = params[0];
                    AtomicReference<ArrayList<EventListener>> listenersRef = listenersMap.get(eventClass);
                    EventListener wrapper = new EventListener(method, weakListenerRef);

                    boolean casLoop = true;

                    // No one tried to subscribe to this event before, try making a new HashMap entry
                    if (listenersRef == null) {
                        ArrayList<EventListener> listeners = new ArrayList<>();
                        listeners.add(wrapper);
                        listenersRef = listenersMap.putIfAbsent(eventClass, new AtomicReference<>(listeners));

                        // Successfully created a new HashMap entry, no need for CAS loop
                        if (listenersRef == null) {
                            casLoop = false;
                        }
                    }

                    if (casLoop) {
                        ArrayList<EventListener> oldListeners, listeners;
                        do {
                            oldListeners = listenersRef.get();
                            // noinspection unchecked
                            listeners = (ArrayList) oldListeners.clone();

                            boolean duplicate = false;

                            for (EventListener l : listeners) {
                                if (l.obj.get() == listener) {
                                    duplicate = true;
                                    break;
                                }
                            }

                            if (!duplicate) {
                                listeners.add(wrapper);
                            }
                        } while (!listenersRef.compareAndSet(oldListeners, listeners));
                    }
                }
            }
        }
	}

    public void unsubscribe(Object listener) {
        //Log.v(TAG, "Unsubscribe: " + listener.getClass().getName() + "@" + listener.toString());

        for (Class<? extends Event> evtClass : listenersMap.keySet()) {
            AtomicReference<ArrayList<EventListener>> listenersRef = listenersMap.get(evtClass);
            ArrayList<EventListener> oldListeners, listeners;
            do {
                oldListeners = listenersRef.get();
                // noinspection unchecked
                listeners = (ArrayList) oldListeners.clone();

                Iterator<EventListener> it = listeners.iterator();
                while (it.hasNext()) {
                    if (it.next().obj.get() == listener) {
                        it.remove();
                    }
                }
            } while (!listenersRef.compareAndSet(oldListeners, listeners));
        }
    }

    /**
     * Asynchronously posts the Event to the EventBus.
     *
     * @param event The Event to be posted.
     * @param source The object which is posting this event. Use {@code this} here unless there is a good reason otherwise.
     */
	public void post(Event event, Object source) {
        if (handler == null) {
            return;
        }

		Message msg = Message.obtain();

		if (msg == null) {
			msg = new Message();
		}

		event.source = source;
		msg.obj = event;
		handler.sendMessage(msg);
	}

    /**
     * Posts the Event to the EventBus. Event listeners are executed on the same thread as the one posting the event.
     *
     * @param event The Event to be posted.
     * @param source The object which is posting this event. Use {@code this} here unless there is a good reason otherwise.
     */
	public void postSynchronously(Event event, Object source) {
		event.source = source;
		dispatchEvent(event);
	}

	private void handleMessage(Message msg) {
		Event event = (Event) msg.obj;

		if (event != null) {
			dispatchEvent(event);
		}
	}

    private void dispatchEvent(Event event) {

        //Log.v(TAG, "Event: " + event.getClass() + " (thread " + Thread.currentThread().getName() + ")");

		final AtomicReference<ArrayList<EventListener>> listenersRef = listenersMap.get(event.getClass());
		if (listenersRef == null) {
			return;
		}

        final ArrayList<EventListener> listeners = listenersRef.get();
        if (listeners == null) {
            return;
        }

		int count = listeners.size();
		for (int i = 0; i < count; ++i) {
			final EventListener listener = listeners.get(i);
			final Object obj = listener.obj.get();

			if (obj == null) {
                listeners.remove(i);
				count = listeners.size();
				--i;
			}
			else {
                // TODO: EventBus currently still delivers messages to objects who unsubscribe "mid-cycle"
                try {
                    //Log.v(TAG, "Target: " + obj.getClass().getName() + "@" + obj.toString() + " (thread " + Thread.currentThread().getName() + ")");

                    // Otherwise, the reflection API creates a temporary Object[] each time when we call invoke!
                    SingleObjectArray s = DefaultPool.obtain(SingleObjectArray.class);
                    listener.method.invoke(obj, s.wrap(event));
                    s.release();
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e.getCause());
                }
            }
		}

        event.release();
	}

    public static class SingleObjectArray extends Poolable {
        private final Object[] array;

        public SingleObjectArray() {
            array = new Object[1];
        }

        public Object[] wrap(Object obj) {
            array[0] = obj;
            return array;
        }

        @Override
        public void onRecycle() {
            array[0] = null;
            super.onRecycle();
        }
    }


}
