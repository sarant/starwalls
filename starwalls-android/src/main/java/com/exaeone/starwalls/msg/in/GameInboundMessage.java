package com.exaeone.starwalls.msg.in;

import android.util.Log;
import android.util.SparseArray;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.util.PoolableBuffer;
import com.exaeone.starwalls.pool.DefaultPool;

/**
 * Created by saran on 10/02/15.
 */
public abstract class GameInboundMessage extends Event {

    public static final String TAG = "GameInboundMessage";

    public static GameInboundMessage deserialize(PoolableBuffer buf) {
        int cmd = buf.getUnsignedShort();
        Class<? extends GameInboundMessage> commandClass = getCommandClass(cmd);
        if (commandClass != null) {
            return DefaultPool.obtain(commandClass).deserialize0(buf);
        }
        else {
            return null;
        }
    }

    protected abstract GameInboundMessage deserialize0(PoolableBuffer buf);

    public boolean isRealtime() {
        return false;
    }

    private static final SparseArray<Class<? extends GameInboundMessage>> commandArray = new SparseArray<>();
    static {
        // 2xxx: remote-to-host commands

        // 20xx: game setup
        commandArray.put(2000, ClientHello.class);
        commandArray.put(2001, VideoSize.class);
        commandArray.put(2002, WindowOnLoad.class);

        // 21xx: game control
        commandArray.put(2100, GameSync.class);

        // 22xx: video control
        commandArray.put(2200, MediaSourceReady.class);
        commandArray.put(2201, VideoBufferLevel.class);

    }

    public static Class<? extends GameInboundMessage> getCommandClass(int cmd) {
        Class<? extends GameInboundMessage> commandClass = commandArray.get(cmd);
        if (commandClass == null) {
            Log.i(TAG, "No class registered for command " + cmd);
        }
        return commandClass;
    }

}
