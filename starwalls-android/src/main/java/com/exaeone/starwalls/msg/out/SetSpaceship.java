package com.exaeone.starwalls.msg.out;

import com.exaeone.starwalls.net.BinaryWebSocketFrame;

/**
 * Created by saran on 10/02/15.
 */
public class SetSpaceship extends GameOutboundMessage {

    private int width = 0;
    private int height = 0;
    private byte[] gfxName = null;

    public SetSpaceship setSize(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public SetSpaceship setGraphics(String name) {
        this.gfxName = name.getBytes();
        return this;
    }

    @Override
    public void onRecycle() {
        this.gfxName = null;
    }

    @Override
    public BinaryWebSocketFrame serialize() {
        BinaryWebSocketFrame buf = getSerializationBuffer();
        buf.putShort(this.width);
        buf.putShort(this.height);
        buf.putShort(gfxName.length);
        buf.putBytes(gfxName);
        return buf;
    }

}
