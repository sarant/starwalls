package com.exaeone.starwalls.msg.in;

import com.exaeone.starwalls.util.PoolableBuffer;

/**
 * Created by saran on 06/04/15.
 */
public class VideoBufferLevel  extends GameInboundMessage {

    private long bufferLevel = 0;

    public long getBufferLevel() {
        return bufferLevel;
    }

    @Override
    protected GameInboundMessage deserialize0(PoolableBuffer buf) {
        bufferLevel = buf.getUnsignedInt();
        return this;
    }

}
