package com.exaeone.starwalls.msg.in;

import com.exaeone.starwalls.util.PoolableBuffer;

/**
 * Created by saran on 05/03/15.
 */
public class ClientHello extends GameInboundMessage {

    private int minimumProtocolVersion;

    public int getMinimumProtocolVersion() {
        return minimumProtocolVersion;
    }

    @Override
    protected GameInboundMessage deserialize0(PoolableBuffer buf) {
        minimumProtocolVersion = buf.getInt();
        return this;
    }


}
