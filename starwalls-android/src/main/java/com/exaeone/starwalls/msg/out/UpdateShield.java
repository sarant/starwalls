package com.exaeone.starwalls.msg.out;

import android.graphics.Bitmap;
import com.exaeone.starwalls.net.BinaryWebSocketFrame;
import com.exaeone.starwalls.util.BufferStreamAdapter;
import com.exaeone.starwalls.util.LargeWebSocketFramePool;

/**
 * Created by saran on 10/02/15.
 */
public class UpdateShield extends GameOutboundMessage {

    private final BufferStreamAdapter stream = new BufferStreamAdapter();

    private float shield;

    private boolean hit = false;
    private int hitX;
    private int hitY;
    private long hitTime;
    private Bitmap hitBitmap;

    @Override
    protected BinaryWebSocketFrame obtainWebSocketFrame() {
        return LargeWebSocketFramePool.obtain();
    }

    @Override
    public void onRecycle() {
        hit = false;
        hitBitmap = null;
        super.onRecycle();
    }

    public UpdateShield setShield(float shield) {
        this.shield = shield;
        return this;
    }

    public UpdateShield setHit(int x, int y, long time, Bitmap bitmap) {
        this.hit = true;
        this.hitX = x;
        this.hitY = y;
        this.hitTime = time;
        this.hitBitmap = bitmap;
        return this;
    }

    @Override
    public BinaryWebSocketFrame serialize() {
        BinaryWebSocketFrame buf = getSerializationBuffer();
        buf.putFloat(this.shield);

        if (hit) {
            buf.putInt(hitX);
            buf.putInt(hitY);
            buf.putInt((int) hitTime);

            if (hitBitmap != null) {
                buf.putInt(hitBitmap.getWidth());
                buf.putInt(hitBitmap.getHeight());

                stream.buffer = buf;
                hitBitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
                stream.buffer = null;
            }
        }

        return buf;
    }

}
