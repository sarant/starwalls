package com.exaeone.starwalls.msg.out;

import com.exaeone.starwalls.net.BinaryWebSocketFrame;

/**
 * Created by saran on 10/02/15.
 */
public class PlayerMove extends GameOutboundMessage {

    private float x;
    private float y;
    private float roll;

    public PlayerMove setPosition(float x, float y, float roll) {
        this.x = x;
        this.y = y;
        this.roll = roll;
        return this;
    }

    @Override
    public BinaryWebSocketFrame serialize() {
        BinaryWebSocketFrame buf = getSerializationBuffer();
        buf.putFloat(x);
        buf.putFloat(y);
        buf.putFloat(roll);
        return buf;
    }
}
