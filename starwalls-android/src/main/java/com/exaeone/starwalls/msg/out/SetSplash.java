package com.exaeone.starwalls.msg.out;

import com.exaeone.starwalls.net.BinaryWebSocketFrame;

/**
 * Created by kathryn on 04/04/15.
 */
public class SetSplash extends GameOutboundMessage {

    private byte[] imgName;

    public SetSplash setSplashImage(String input){
        this.imgName = input.getBytes();
        return this;
    }

    @Override
    public void onRecycle() {
        imgName = null;
        super.onRecycle();
    }

    @Override
    public BinaryWebSocketFrame serialize() {
        BinaryWebSocketFrame buf = getSerializationBuffer();
        buf.putShort(imgName.length);
        buf.putBytes(imgName);
        return buf;
    }
}
