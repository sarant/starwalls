package com.exaeone.starwalls.msg.out;

import com.exaeone.starwalls.net.BinaryWebSocketFrame;

/**
 * Created by saran on 10/02/15.
 */
public class GameStop extends GameOutboundMessage {

    private int finalScore;
    private int bestStreak;
    private byte[] endGameGraphics = null;

    @Override
    public void onRecycle() {
        this.endGameGraphics = null;
        super.onRecycle();
    }


    public GameStop setFinalScore(int score){
        this.finalScore = score;
        return this;
    }

    public GameStop setBestStreak(int streak){
        this.bestStreak = streak;
        return this;
    }

    public GameStop setEndGameGraphics(String gfxName) {
        endGameGraphics = gfxName.getBytes();
        return this;
    }

    @Override
    public BinaryWebSocketFrame serialize() {
        BinaryWebSocketFrame buf = getSerializationBuffer();
        buf.putInt(this.finalScore);
        buf.putInt(this.bestStreak);
        buf.putShort(endGameGraphics.length);
        buf.putBytes(endGameGraphics);
        return buf;
    }

}
