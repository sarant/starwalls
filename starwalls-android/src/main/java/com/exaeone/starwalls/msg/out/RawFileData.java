package com.exaeone.starwalls.msg.out;

import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.net.BinaryWebSocketFrame;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Created by saran on 15/02/15.
 */
public abstract class RawFileData extends GameOutboundMessage {
    private byte[] name = null;
    private int id = 0;

    public RawFileData setName(String name) {
        this.name = name.getBytes();
        return this;
    }

    public RawFileData setResourceID(int id) {
        this.id = id;
        return this;
    }

    @Override
    public void onRecycle() {
        this.name = null;
        this.id = 0;
        super.onRecycle();
    }

    @Override
    public BinaryWebSocketFrame serialize() {
        BinaryWebSocketFrame buf = getSerializationBuffer();
        buf.putShort(name.length);
        buf.putBytes(name);
        try {
            InputStream inputStream = App.getContext().getResources().openRawResource(id);
            int fileLength = inputStream.available();

            if (fileLength > buf.writableBytes()) {
                BinaryWebSocketFrame grownBuf = new BinaryWebSocketFrame(buf.writerIndex() + fileLength);
                grownBuf.putBytes(buf);
                buf.release();
                buf = grownBuf;
            }

            ReadableByteChannel fileChannel = Channels.newChannel(inputStream);
            int bytesRead = 0;

            do {
                ByteBuffer nioBuffer = buf.nioBufferLockForWrite();
                bytesRead += fileChannel.read(nioBuffer);
                buf.nioBufferUnlock(nioBuffer);
            } while (bytesRead < fileLength);

            fileChannel.close();
            inputStream.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return buf;
    }
}
