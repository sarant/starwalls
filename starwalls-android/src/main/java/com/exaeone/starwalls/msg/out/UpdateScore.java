package com.exaeone.starwalls.msg.out;


import com.exaeone.starwalls.net.BinaryWebSocketFrame;

/**
 * Created by kathryn on 26/03/2015.
 */
public class UpdateScore extends GameOutboundMessage {

    private int score;

    public UpdateScore setScore(int input){
        this.score = input;
        return this;
    }


    @Override
    public BinaryWebSocketFrame serialize() {
        BinaryWebSocketFrame buf = getSerializationBuffer();
        buf.putInt(this.score);
        return buf;
    }

}
