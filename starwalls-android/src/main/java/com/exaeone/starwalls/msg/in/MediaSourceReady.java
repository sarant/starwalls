package com.exaeone.starwalls.msg.in;

import com.exaeone.starwalls.util.PoolableBuffer;

/**
* Created by saran on 05/03/15.
*/
public class MediaSourceReady extends GameInboundMessage {

    private int streamSerialNumber = 0;

    public int getStreamSerialNumber() {
        return streamSerialNumber;
    }

    @Override
    protected GameInboundMessage deserialize0(PoolableBuffer buf) {
        streamSerialNumber = buf.getInt();
        return this;
    }

}
