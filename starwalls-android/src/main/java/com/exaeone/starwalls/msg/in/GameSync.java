package com.exaeone.starwalls.msg.in;

import com.exaeone.starwalls.util.PoolableBuffer;

/**
* Created by saran on 05/03/15.
*/
public class GameSync extends GameInboundMessage {

    private long gameTimeMillis = 0;
    private float x = 0;
    private float y = 0;
    private float roll = 0;

    public long gameTimeMillis() {
        return gameTimeMillis;
    }

    public float x() {
        return x;
    }

    public float y() {
        return y;
    }

    public float roll() {
        return roll;
    }

    @Override
    public boolean isRealtime() {
        return true;
    }

    @Override
    protected GameInboundMessage deserialize0(PoolableBuffer buf) {
        gameTimeMillis = buf.getUnsignedInt();
        x = buf.getFloat();
        y = buf.getFloat();
        roll = buf.getFloat();
        return this;
    }

}
