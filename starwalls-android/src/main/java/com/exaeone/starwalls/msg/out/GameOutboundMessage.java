package com.exaeone.starwalls.msg.out;

import android.util.Log;
import com.exaeone.starwalls.net.BinaryWebSocketFrame;
import com.exaeone.starwalls.pool.DefaultPool;
import com.exaeone.starwalls.pool.Poolable;
import com.exaeone.starwalls.video.VideoFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by saran on 10/02/15.
 */
public abstract class GameOutboundMessage extends Poolable {

    public static final String TAG = "GameMessage";

    public final int COMMAND;
    public GameOutboundMessage() {
        COMMAND = getCommandCode(this.getClass());
    }

    public abstract BinaryWebSocketFrame serialize();

    protected BinaryWebSocketFrame obtainWebSocketFrame() {
        return DefaultPool.obtain(BinaryWebSocketFrame.class);
    }

    protected final BinaryWebSocketFrame getSerializationBuffer() {
        BinaryWebSocketFrame buf = obtainWebSocketFrame();
        buf.putShort(this.COMMAND);
        return buf;
    }

    private static final Map<Class, Integer> commandMap = new HashMap<>();
    static {
        // 1xxx: host-to-remote commands

        // 10xx: game setup
        commandMap.put(ServerHello.class, 1000);
        commandMap.put(SetSpaceship.class, 1001);
        commandMap.put(GraphicsData.class, 1002);
        commandMap.put(SetSplash.class, 1003);

        // 11xx: game control
        commandMap.put(GameStart.class, 1100);
        commandMap.put(GameStop.class, 1101);
        commandMap.put(PlayerMove.class, 1102);
        commandMap.put(UpdateShield.class, 1103);
        commandMap.put(UpdateScore.class, 1104);
        commandMap.put(GameText.class, 1105);

        // 12xx: video control
        commandMap.put(ClearVideoBuffer.class, 1200);
        commandMap.put(VideoFragment.class, 1201);
    }

    public static int getCommandCode(Class c) {
        if (commandMap.containsKey(c)) {
            return commandMap.get(c);
        }
        else {
            Log.i(TAG, "No command registered for class " + c.getName());
            return 0;
        }
    }

}
