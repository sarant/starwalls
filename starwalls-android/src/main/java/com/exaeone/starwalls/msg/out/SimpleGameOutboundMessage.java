package com.exaeone.starwalls.msg.out;

import com.exaeone.starwalls.net.BinaryWebSocketFrame;

/**
 * Created by saran on 10/02/15.
 */
public abstract class SimpleGameOutboundMessage extends GameOutboundMessage {

    @Override
    public BinaryWebSocketFrame serialize() {
        BinaryWebSocketFrame buf = getSerializationBuffer();
        return buf;
    }
}
