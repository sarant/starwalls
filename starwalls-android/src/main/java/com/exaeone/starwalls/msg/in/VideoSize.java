package com.exaeone.starwalls.msg.in;

import com.exaeone.starwalls.util.PoolableBuffer;

/**
* Created by saran on 05/03/15.
*/
public class VideoSize extends GameInboundMessage {

    private int stageWidth = 0;
    private int stageHeight = 0;

    public int getStageWidth() {
        return stageWidth;
    }

    public int getStageHeight() {
        return stageHeight;
    }

    @Override
    protected GameInboundMessage deserialize0(PoolableBuffer buf) {
        stageWidth = buf.getUnsignedShort();
        stageHeight = buf.getUnsignedShort();
        return this;
    }

}
