package com.exaeone.starwalls.msg.out;

import com.exaeone.starwalls.net.BinaryWebSocketFrame;

/*
* Created by Kathryn 25/3/15
*/

public class GameText extends GameOutboundMessage {

	private byte[] message;
	private int displayTime;

	public GameText setMessage(String input){
		this.message = input.getBytes();
		return this;
	}

	public GameText setDisplayTime(int input){
		this.displayTime = input;
		return this;
	}

	@Override
	public void onRecycle() {
		message = null;
		super.onRecycle();
	}

	@Override
	public BinaryWebSocketFrame serialize() {
		BinaryWebSocketFrame buf = getSerializationBuffer();
		buf.putShort(this.displayTime);
		buf.putShort(message.length);
		buf.putBytes(message);
		return buf;
	}

}