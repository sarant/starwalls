package com.exaeone.starwalls.msg.in;

import com.exaeone.starwalls.util.PoolableBuffer;

/**
 * Created by saran on 10/02/15.
 */
public abstract class SimpleGameInboundMessage extends GameInboundMessage {

    @Override
    protected GameInboundMessage deserialize0(PoolableBuffer buf) {
        return this;
    }
}
