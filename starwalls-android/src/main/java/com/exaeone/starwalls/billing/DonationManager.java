package com.exaeone.starwalls.billing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by saran on 05/04/15.
 */
public class DonationManager implements
        IabHelper.OnIabSetupFinishedListener,
        IabHelper.QueryInventoryFinishedListener,
        IabHelper.OnIabPurchaseFinishedListener,
        IabHelper.OnConsumeFinishedListener {

    private final String base64EncodedPublicKey =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3Pcv+rs5tb3uxwnEkDPf" +
                    "eZrWCRjUdxFEKnpYh3oo5uerILHrMaSjKjUyQ1KGZkIpasInxWeASP7eoUtiXFPw" +
                    "9W6Anioc3Bamgg5ggKRfVyOr350oCWTbiSt+pR/+ZWfFGoM5gsUPCdS8mMh3GVd7" +
                    "oI0yPubTrBFvU7BDgwvF/7w5apeHKqClCeVDmvONPTSCNq9IF9xduTp0wa/HDG3F" +
                    "Ns0UT4+lnhZngeXnqRS368sz6YJPQoszQPTDiYaw2J5fewapHARhzi5IutDaR6a9" +
                    "GnPyHcfbFdFE/ZQYmqDzgmxgB7BmPlRhyMGat5VLeBLF75we/P/pmrZUiGzDtZ+3XQIDAQAB";


    private static final String TAG = "DonationManager";

    private final IabHelper mHelper;
    private volatile boolean ready = false;

    public enum DonationSize {
        Small (R.string.donation_small, "smalldonation", 1),
        Medium (R.string.donation_medium, "mediumdonation", 2),
        Large (R.string.donation_large, "largedonation", 3),
        XLarge (R.string.donation_xlarge, "xlargedonation", 4);

        public final int buttonTextResID;
        public final String skuName;
        public final int requestCode;

        DonationSize(int buttonTextResID, String skuName, int requestCode) {
            this.buttonTextResID = buttonTextResID;
            this.skuName = skuName;
            this.requestCode = requestCode;
        }

        public static List<String> getSkus() {
            List<String> skus = new ArrayList<>();
            for (DonationSize d : DonationSize.values()) {
                skus.add(d.skuName);
            }
            return skus;
        }

        public static DonationSize getFromSku(String sku) {
            for (DonationSize d : DonationSize.values()) {
                if (d.skuName.equals(sku)) {
                    return d;
                }
            }
            return null;
        }
    }

    private final Map<DonationSize, String> donationPrices = new HashMap<>();

    public DonationManager(Context context) {
        mHelper = new IabHelper(context, base64EncodedPublicKey);
        mHelper.startSetup(this);
    }

    public synchronized boolean isReady() {
        return ready;
    }

    public synchronized String getDonationPriceForSize(DonationSize donationSize) {
        return donationPrices.get(donationSize);
    }

    public synchronized void donate(final DonationSize donationSize, final Activity activity) {
        // Do we need to post this to the main thread? I'm just being cautious here.
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mHelper.launchPurchaseFlow(
                        activity,
                        donationSize.skuName,
                        donationSize.requestCode,
                        DonationManager.this,
                        donationSize.skuName);
            }
        });
    }

    public synchronized boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return mHelper.handleActivityResult(requestCode, resultCode, data);
    }

    @Override
    public synchronized void onIabSetupFinished(IabResult result) {
        if (result.isSuccess()) {
            Log.i(TAG, "In-app billing setup successful");
            mHelper.queryInventoryAsync(true, DonationSize.getSkus(), this);
        }
        else {
            Log.e(TAG, "In-app billing setup failed");
        }
    }

    @Override
    public synchronized void onQueryInventoryFinished(IabResult result, Inventory inv) {
        if (result.isSuccess()) {
            donationPrices.clear();
            for (DonationSize donationSize : DonationSize.values()) {
                SkuDetails details = inv.getSkuDetails(donationSize.skuName);
                if (details != null) {
                    boolean addToList = true;
                    Purchase purchase = inv.getPurchase(donationSize.skuName);
                    if (purchase != null) {
                        try {
                            mHelper.consume(purchase);
                        }
                        catch (IabException e) {
                            addToList = false;
                        }
                    }

                    if (addToList) {
                        donationPrices.put(donationSize, details.getPrice());
                    }
                }
            }

            ready = true;
            EventBus.DEFAULT.post(new OnReady(), this);
        }
        else {
            Log.e(TAG, "Failed to query the inventory");
        }
    }

    @Override
    public synchronized void onIabPurchaseFinished(IabResult result, Purchase purchase) {
        if (result.isSuccess()) {
            mHelper.consumeAsync(purchase, this);
            EventBus.DEFAULT.post(new OnPurchaseFinished().setSuccess(true), this);
        }
        else {
            EventBus.DEFAULT.post(new OnPurchaseFinished().setSuccess(false), this);
        }
    }

    @Override
    public synchronized void onConsumeFinished(Purchase purchase, IabResult result) {

    }

    public static class OnReady extends Event {

    }

    public static class OnPurchaseFinished extends Event {

        private boolean success = false;

        @Override
        public void onRecycle() {
            success = false;
            super.onRecycle();
        }

        public OnPurchaseFinished setSuccess(boolean success) {
            this.success = success;
            return this;
        }

        public boolean isSuccess() {
            return success;
        }

    }

}
