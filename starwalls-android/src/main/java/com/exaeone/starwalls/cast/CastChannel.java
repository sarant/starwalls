package com.exaeone.starwalls.cast;

import com.exaeone.starwalls.event.EventBus;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;

import java.io.IOException;

/**
 * Created by saran on 16/02/15.
 */
public class CastChannel implements Cast.MessageReceivedCallback {

    public static final String CHANNEL_NAMESPACE = "urn:x-cast:com.exaeone.starwalls";

    /*package*/ CastConnector connector;

    public CastChannel(CastConnector connector) {
        this.connector = connector;
    }

    public boolean open() {
        if (connector.googleApiCallback != null) {
            try {
                Cast.CastApi.setMessageReceivedCallbacks(connector.googleApiClient, CHANNEL_NAMESPACE, this);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        else {
            return false;
        }
    }

    public boolean close() {
        if (connector.googleApiCallback != null) {
            try {
                Cast.CastApi.removeMessageReceivedCallbacks(connector.googleApiClient, CHANNEL_NAMESPACE);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        else {
            return false;
        }
    }

    @Override
    public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
        if (namespace.equals(CHANNEL_NAMESPACE) && message.equals("window.onload")) {
            connector.readyForMessage = true;
            EventBus.DEFAULT.post(new CastConnector.OnCastReady(), connector);
        }
    }

    public void sendMessage(String message) {
        if (connector.googleApiCallback != null) {
            Cast.CastApi
                    .sendMessage(connector.googleApiClient, CHANNEL_NAMESPACE, message);
        }
    }

}
