package com.exaeone.starwalls.cast;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.pool.DefaultPool;
import com.exaeone.starwalls.util.PoolableBuffer;
import com.google.android.gms.cast.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.nio.ByteOrder;

/**
 * Created by saran on 16/02/15.
 */
public class CastConnector {

    public static final String TAG = "CastConnector";

    private String castAppID = App.getContext().getString(R.string.cast_app_id);

    private CastFinder castFinder = new CastFinder();
    private CastChannel channel = new CastChannel(this);

    /*package*/ Handler uiThreadHandler = new Handler(Looper.getMainLooper());
    /*package*/ MediaRouter mediaRouter = null;
    /*package*/ MediaRouteSelector routeSelector = null;

    /*package*/ MediaRouterCallback mediaRouterCallback = new MediaRouterCallback();
    /*package*/ GoogleApiCallback googleApiCallback = new GoogleApiCallback();
    /*package*/ CastListener castListener = new CastListener();

    /*package*/ CastDevice castDevice = null;
    /*package*/ GoogleApiClient googleApiClient = null;
    /*package*/ boolean launched = false;
    /*package*/ boolean readyForMessage = false;
    /*package*/ String sessionID = null;
    /*package*/ Inet4Address localAddress = queryLocalAddress();

    public CastConnector() {
        uiThreadHandler = new Handler(Looper.getMainLooper());

        uiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                mediaRouter = MediaRouter.getInstance(App.getContext());
                routeSelector = new MediaRouteSelector.Builder()
                        .addControlCategory(CastMediaControlIntent.categoryForCast(castAppID))
                        .build();
            }
        });

        while (mediaRouterCallback == null) {}
    }

    public MediaRouteSelector getRouteSelector() {
        return routeSelector;
    }

    public void startScanning() {
        EventBus.DEFAULT.subscribe(this);
        castFinder.start();
        uiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                mediaRouter.addCallback(routeSelector,
                        mediaRouterCallback,
                        MediaRouter.CALLBACK_FLAG_PERFORM_ACTIVE_SCAN);
            }
        });
    }

    public void stopScanning() {
        castFinder.stop();
        uiThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                mediaRouter.removeCallback(mediaRouterCallback);
            }
        });
        EventBus.DEFAULT.unsubscribe(this);
    }

    public void onEvent(CastFinder.OnDiscover evt) {
        CastDevice castDevice = evt.getDevice();
        Log.i(TAG, "Discovered " + castDevice.getFriendlyName() + " via CastFinder");

        EventBus.DEFAULT.post(new OnDiscover()
                        .setChromecast(new Chromecast(castDevice, Chromecast.Discoverer.CASTFINDER)),
                this);
    }

    public void sendMessage(String message) {
        channel.sendMessage(message);
    }

    private void teardown() {
        readyForMessage = false;

        if (launched) {
            launched = false;
            EventBus.DEFAULT.post(new OnCastDisconnected(), this);
        }

        if (sessionID != null) {
            channel.close();
            Cast.CastApi.stopApplication(googleApiClient, sessionID);
        }

        if (googleApiClient != null) {
            try {
                googleApiClient.disconnect();
            }
            catch (RuntimeException e){
                Log.e(TAG, "error disconnecting from Google API");
                e.printStackTrace();
            }
        }

        castDevice = null;
        sessionID = null;
        googleApiClient = null;
        localAddress = queryLocalAddress();
    }

    public void connectToCastDevice(CastDevice castDevice) {
        if (castDevice == null) {
            return;
        }
        else {
            teardown();
            this.castDevice = castDevice;
        }

        Cast.CastOptions.Builder apiOptionsBuilder = Cast.CastOptions
                .builder(castDevice, castListener);

        googleApiClient = new GoogleApiClient.Builder(App.getContext())
                .addApi(Cast.API, apiOptionsBuilder.build())
                .addConnectionCallbacks(googleApiCallback)
                .addOnConnectionFailedListener(googleApiCallback)
                .build();

        googleApiClient.connect();
    }

    public Inet4Address getLocalAddress() {
        return this.localAddress;
    }

    private Inet4Address queryLocalAddress() {
        Inet4Address address = null;

        try {
            // Try parsing netstat first. This guarantees that we're getting the address that's connected to the Chromecast
            Process process;
            BufferedReader reader;
            String castAddress = castDevice.getIpAddress().getHostAddress();


            process = Runtime.getRuntime().exec("netstat -n");
            process.getOutputStream().close();
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains("ESTABLISHED") && line.contains(castAddress)) {
                    String[] words = line.split("\\s+");
                    String[] splitAtColon = words[3].split(":");
                    address = (Inet4Address) Inet4Address.getByName(splitAtColon[splitAtColon.length - 2]);
                    break;
                }
            }

            reader.close();
        }
        catch (IOException e) {
        }
        catch (RuntimeException e) {
        }

        // If the previous attempt failed, try WiFi state access instead
        // http://stackoverflow.com/questions/16730711
        if (address == null) {
            try {
                WifiManager wifiManager = (WifiManager) App.getContext().getSystemService(Context.WIFI_SERVICE);
                int ipInt = wifiManager.getConnectionInfo().getIpAddress();

                // Convert little-endian to big-endian, if needed
                if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                    ipInt = Integer.reverseBytes(ipInt);
                }

                byte[] ipByteArray = BigInteger.valueOf(ipInt).toByteArray();
                address = (Inet4Address) Inet4Address.getByAddress(ipByteArray);
            }
            catch (IOException e) {
            }
            catch (RuntimeException e) {
            }
        }

        return address;
    }

    public boolean isCastConnected() {
        return launched;
    }

    public boolean isCastReady() {
        return readyForMessage;
    }

    private class MediaRouterCallback extends MediaRouter.Callback {
        @Override
        public void onRouteAdded(MediaRouter router, MediaRouter.RouteInfo routeInfo) {
            CastDevice castDevice = CastDevice.getFromBundle(routeInfo.getExtras());
            if (castDevice != null) {
                Log.i(TAG, "Discovered " + castDevice.getFriendlyName() + " via Google API");
                EventBus.DEFAULT.post(new OnDiscover()
                                .setChromecast(new Chromecast(castDevice, Chromecast.Discoverer.GOOGLE)),
                        this);
            }
        }
    }

    private class GoogleApiCallback implements
            GoogleApiClient.ConnectionCallbacks,
            GoogleApiClient.OnConnectionFailedListener,
            ResultCallback<Cast.ApplicationConnectionResult>
    {

        @Override
        public void onConnected(Bundle bundle) {
            LaunchOptions options = new LaunchOptions.Builder()
                    .setRelaunchIfRunning(true)
                    .build();

            Cast.CastApi
                    .launchApplication(googleApiClient, castAppID, options)
                    .setResultCallback(this);
        }

        @Override
        public void onConnectionSuspended(int i) {
            //teardown();
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            if (connectionResult.getErrorCode() == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
                EventBus.DEFAULT.post(new OnGMSUpdateRequired(), this);
            }
            else {
                EventBus.DEFAULT.post(new OnConnectionFailure(), this);
            }

            teardown();
        }

        @Override
        public void onResult(Cast.ApplicationConnectionResult result) {
            Status status = result.getStatus();

            if (status.isSuccess() && channel.open()) {
                launched = true;
                localAddress = queryLocalAddress();
                sessionID = result.getSessionId();
                EventBus.DEFAULT.post(new OnCastConnected(), this);
            }
            else {
                EventBus.DEFAULT.post(new OnConnectionFailure(), this);
                teardown();
            }
        }
    }

    private class CastListener extends Cast.Listener {
        @Override
        public void onApplicationDisconnected(int statusCode) {
            sessionID = null;
            teardown();
        }
    }

    public static class OnDiscover extends Event {
        private Chromecast chromecast = null;

        @Override
        public void onRecycle() {
            chromecast = null;
            super.onRecycle();
        }

        /*package*/ OnDiscover setChromecast(Chromecast chromecast) {
            this.chromecast = chromecast;
            return this;
        }

        public Chromecast getChromecast() {
            return chromecast;
        }
    }

    public static class OnCastConnected extends Event {

    }

    public static class OnCastReady extends Event {

    }

    public static class OnCastDisconnected extends Event {

    }

    public static class OnGMSUpdateRequired extends Event {

    }

    public static class OnConnectionFailure extends Event {

    }

}
