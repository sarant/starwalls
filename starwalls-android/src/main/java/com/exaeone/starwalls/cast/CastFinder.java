package com.exaeone.starwalls.cast;

import android.content.Context;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.mdns.*;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastDeviceFactory;
import com.google.android.gms.common.images.WebImage;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class CastFinder {

    public static final String TAG = "CastFinder";

    private static Inet4Address mDNSAddress = null;
    static {
        try {
            mDNSAddress = (Inet4Address) Inet4Address.getByName("224.0.0.251");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private AtomicBoolean running = new AtomicBoolean(false);
    private AtomicBoolean notifyClose = new AtomicBoolean(false);
    private AtomicReference<MulticastSocket> socketRef = new AtomicReference<>(null);

    // Make an object to synchronise multicastLock.release()
    private final Object multicastLockSync = new Object();
    private WifiManager.MulticastLock multicastLock = null;

    private final Map<String, CastDevice> discovered = new HashMap<>();

    public void start() {
        if (running.compareAndSet(false, true)) {
            if (mDNSAddress == null) {
                Log.e(TAG, "Failed to create an Inet4Address corresponding to the mDNS multicast address");
                return;
            }

            if (multicastLock == null) {
                WifiManager wifiManager = (WifiManager) App.getContext().getSystemService(Context.WIFI_SERVICE);
                multicastLock = wifiManager.createMulticastLock(TAG);
            }

            multicastLock.acquire();

            if (!multicastLock.isHeld()) {
                Log.e(TAG, "Cannot acquire a multicast lock");
            }

            // No network on main thread!
            new Thread(new Runnable() {
                @Override
                public void run() {
                    MulticastSocket socket = null;

                    try {
                        socket = new MulticastSocket(5353);
                        socketRef.set(socket);
                    }
                    catch (IOException e) {
                        Log.e(TAG, "Cannot start mDNS discovery due to an IOException");
                        e.printStackTrace();
                        multicastLock.release();
                        teardown();
                    }

                    if (socket != null) {
                        try {
                            socket.setReuseAddress(true);
                            socket.joinGroup(mDNSAddress);
                            socket.setSoTimeout(1);

                            new Thread(new MulticastReceiver(), "MulticastReceiver").start();

                            byte[] query = makeQuery();
                            socket.send(new DatagramPacket(query, 0, query.length, mDNSAddress, 5353));

                            if (notifyClose.compareAndSet(false, true)) {
                                Log.i("Castfinder", "Started");
                                EventBus.DEFAULT.post(new OnStart(), this);
                            }
                        } catch (IOException e) {
                            Log.e(TAG, "Cannot start mDNS discovery due to an IOException");
                            e.printStackTrace();
                            teardown();
                        }
                    }
                }
            }).start();

        }
    }

    public void stop() {
        teardown();
    }

    private void teardown() {
        if (running.compareAndSet(true, false)) {
            discovered.clear();
        }

        if (notifyClose.compareAndSet(true, false)) {
            Log.i("Castfinder", "Stopped");
            EventBus.DEFAULT.post(new OnStop(), this);
        }

        MulticastSocket socket = socketRef.get();
        if (socket != null && socketRef.compareAndSet(socket, null)) {
            try {
                socket.leaveGroup(mDNSAddress);
                socket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        synchronized (multicastLockSync) {
            if (multicastLock.isHeld()) {
                multicastLock.release();
            }
        }
    }

    private byte[] makeQuery() {
        String name1 = "_googlecast";
        String name2 = "_tcp";
        String name3 = "local";

        ByteBuffer buf = ByteBuffer.allocate(1500);

        // HEADER
        buf.putShort((short) 0x0000);   // id = 0
        buf.putShort((short) 0x0000);   // flags
        buf.putShort((short) 0x0001);   // # questions
        buf.putShort((short) 0x0000);   // # answers
        buf.putShort((short) 0x0000);   // # authority
        buf.putShort((short) 0x0000);   // # additional

        buf.put((byte) name1.length());
        buf.put(name1.getBytes());
        buf.put((byte) name2.length());
        buf.put(name2.getBytes());
        buf.put((byte) name3.length());
        buf.put(name3.getBytes());
        buf.put((byte) 0);
        buf.putShort((short) 0x000C);   // record type = PTR
        buf.putShort((short) 0x0001);   // "QM" question
        buf.flip();

        byte[] array = new byte[buf.remaining()];
        buf.get(array);

        return array;
    }

    public void castDeviceFromDNSResponse(DNSResponse response) {
        TXTRecord txt = (TXTRecord) response.additionalSection().find(TXTRecord.class);
        SRVRecord srv = (SRVRecord) response.additionalSection().find(SRVRecord.class);
        ARecord a = (ARecord) response.additionalSection().find(ARecord.class);

        if (a == null || srv == null || txt == null) {
            return;
        }

        // This seems to be what the Play API does. No idea what it is.
        int versionCode = 3;

        String deviceId = txt.get("id");
        if (deviceId == null) {
            return;
        }

        String hostAddress = a.getAddr().getHostAddress();

        String friendlyName = txt.get("fn");
        if (friendlyName == null) {
            return;
        }

        String modelName = txt.get("md");
        if (modelName == null) {
            modelName = "Chromecast";
        }

        String deviceVersion = txt.get("ve");
        if (deviceVersion == null) {
            deviceVersion = "04";
        }

        int servicePort = srv.getPort();

        List<WebImage> icons = new ArrayList<>();
        try {
            String ic[] = txt.get("ic").split(",");
            for (String url : ic) {
                icons.add(new WebImage(Uri.parse("http://" + hostAddress + ":8008" + url)));
            }
        }
        catch (NullPointerException e) {
            return;
        }

        int capabilities;
        try {
            capabilities = Integer.valueOf(txt.get("ca"));
        }
        catch (RuntimeException e) {
            capabilities = 5;
        }

        int status;
        try {
            status = Integer.valueOf(txt.get("st"));
        }
        catch (RuntimeException e) {
            status = 0;
        }

        CastDevice castDevice = CastDeviceFactory.create(
                versionCode, deviceId, hostAddress, friendlyName, modelName,
                deviceVersion, servicePort, icons, capabilities, status);

        if (!discovered.containsKey(deviceId) ||
                !discovered.get(deviceId).getIpAddress().equals(castDevice.getIpAddress()))
        {
            discovered.put(deviceId, castDevice);
            EventBus.DEFAULT.post(new OnDiscover().setDevice(castDevice), this);
        }
    }

    public static class OnStart extends Event {

    }

    public static class OnStop extends Event {

    }

    public static class OnDiscover extends Event {

        private CastDevice castDevice = null;

        public CastDevice getDevice() {
            return castDevice;
        }

        /*package*/ OnDiscover setDevice(CastDevice castDevice) {
            this.castDevice = castDevice;
            return this;
        }

        @Override
        public void onRecycle() {
            castDevice = null;
            super.onRecycle();
        }

    }

    class MulticastReceiver implements Runnable {

        @Override
        public void run() {
            DatagramPacket packet = new DatagramPacket(new byte[1500], 0, 1500);


            while (running.get()) {
                try {
                    MulticastSocket socket = socketRef.get();
                    if (socket == null) {
                        break;
                    }

                    socket.receive(packet);
                    if (packet.getLength() > 0) {
                        DNSResponse response = DNSSDResponseDecoder.decode(
                                ByteBuffer.wrap(packet.getData(), packet.getOffset(), packet.getLength())
                        );

                        if (response != null) {
                            PTRRecord answer = (PTRRecord) response.answerSection().find(PTRRecord.class);
                            if (answer != null && answer.getRecordName().equals("_googlecast._tcp.local")) {
                                castDeviceFromDNSResponse(response);
                            }
                        }
                    }
                }
                catch (IOException e) {
                    if (!SocketTimeoutException.class.isAssignableFrom(e.getClass()))
                    teardown();
                }
            }
        }
    }

}
