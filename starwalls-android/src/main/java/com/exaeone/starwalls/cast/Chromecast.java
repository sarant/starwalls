package com.exaeone.starwalls.cast;

import com.google.android.gms.cast.CastDevice;

/**
 * Created by saran on 18/03/15.
 */
public class Chromecast {

    private final CastDevice castDevice;
    private final Discoverer discoverer;
    private boolean connecting = false;

    /*package*/ Chromecast(CastDevice castDevice, Discoverer discoverer) {
        this.castDevice = castDevice;
        this.discoverer = discoverer;
    }

    public CastDevice getCastDevice() {
        return castDevice;
    }

    public Discoverer getDiscoverer() {
        return discoverer;
    }

    @Override
    public String toString() {
        return castDevice.getFriendlyName() + " (discovered by " + discoverer.name + ")";
    }

    public static enum Discoverer {
        GOOGLE (0, "Google API"),
        CASTFINDER (1, "CastFinder");

        public final int precedence;
        public final String name;

        Discoverer(int precedence, String name) {
            this.precedence = precedence;
            this.name = name;
        }
    }

    public boolean isConnecting() {
        return connecting;
    }

    public void setConnecting(boolean connecting) {
        this.connecting = connecting;
    }
}
