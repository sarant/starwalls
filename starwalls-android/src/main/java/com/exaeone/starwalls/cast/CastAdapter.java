package com.exaeone.starwalls.cast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.cast.Chromecast;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saran on 19/03/15.
 */
public class CastAdapter extends ArrayAdapter<Chromecast> {

    private Map<String, Chromecast> castMap = new HashMap<>();

    private LayoutInflater inflater = (LayoutInflater) App.getContext()
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    private boolean connecting = false;

    private static final int itemResourceID = R.layout.item_cast;

    private static final Comparator<Chromecast> comparator = new Comparator<Chromecast>() {
        @Override
        public int compare(Chromecast lhs, Chromecast rhs) {
            return lhs.getCastDevice().getFriendlyName().compareTo(rhs.getCastDevice().getFriendlyName());
        }
    };

    public CastAdapter(Context context) {
        super(context, itemResourceID);
    }

    public void setConnecting(int position) {
        connecting = true;
        int count = getCount();

        for (int i = 0; i < count; ++i) {
            if (position == i) {
                getItem(i).setConnecting(true);
            }
            else {
                getItem(i).setConnecting(false);
            }
        }
        notifyDataSetChanged();
    }

    public boolean isConnecting() {
        return connecting;
    }

    @Override
    public void clear() {
        connecting = false;
        castMap.clear();
        super.clear();
    }

    private void doAdd(Chromecast cast) {
        String castID = cast.getCastDevice().getDeviceId();
        Chromecast oldCast = castMap.get(castID);
        if (oldCast == null) {
            super.add(cast);
            castMap.put(castID, cast);
        }
        else if (cast.getDiscoverer().precedence < oldCast.getDiscoverer().precedence ||
                !oldCast.getCastDevice().getIpAddress().equals(cast.getCastDevice().getIpAddress())) {
            int oldPos = super.getPosition(oldCast);
            super.insert(cast, oldPos);
            super.remove(oldCast);
            castMap.put(castID, cast);
        }
    }

    @Override
    public void add(Chromecast cast) {
        setNotifyOnChange(false);
        doAdd(cast);
        notifyDataSetChanged();
    }

    @Override
    public void addAll(Collection<? extends Chromecast> collection) {
        setNotifyOnChange(false);
        for (Chromecast cast : collection) {
            doAdd(cast);
        }
        notifyDataSetChanged();
    }

    @Override
    public void addAll(Chromecast... items) {
        setNotifyOnChange(false);
        for (Chromecast cast : items) {
            doAdd(cast);
        }
        notifyDataSetChanged();
    }

    @Override
    public void insert(Chromecast cast, int index) {
        setNotifyOnChange(false);
        String castID = cast.getCastDevice().getDeviceId();
        Chromecast oldCast = castMap.get(castID);
        if (oldCast == null) {
            super.insert(cast, index);
            castMap.put(castID, cast);
        }
        else if (cast.getDiscoverer().precedence < oldCast.getDiscoverer().precedence ||
                !oldCast.getCastDevice().getIpAddress().equals(cast.getCastDevice().getIpAddress())) {
            super.insert(cast, index);
            super.remove(oldCast);
            castMap.put(castID, cast);
        }
        notifyDataSetChanged();
    }

    @Override
    public void remove(Chromecast object) {
        super.remove(object);
        castMap.remove(object.getCastDevice().getDeviceId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            view = inflater.inflate(itemResourceID, parent, false);
        } else {
            view = convertView;
        }

        TextView nameView = (TextView) view.findViewById(R.id.cast_name);
        //TextView srcView = (TextView) view.findViewById(R.id.cast_src);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.cast_progress);

        Chromecast chromecast = getItem(position);

        if (connecting) {
            if (chromecast.isConnecting()) {
                progressBar.setVisibility(View.VISIBLE);
                view.setEnabled(true);
            }
            else {
                progressBar.setVisibility(View.INVISIBLE);
                view.setEnabled(false);
            }
        }
        else {
            progressBar.setVisibility(View.INVISIBLE);
            view.setEnabled(true);
        }

        nameView.setText(chromecast.getCastDevice().getFriendlyName());
        //srcView.setText(chromecast.getDiscoverer().name);

        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        if (connecting) {
            return false;
        }
        else {
            return true;
        }
    }
}
