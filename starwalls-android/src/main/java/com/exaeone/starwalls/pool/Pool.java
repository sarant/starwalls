package com.exaeone.starwalls.pool;

import android.util.Log;
import com.exaeone.starwalls.net.BinaryWebSocketFrame;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Pool for objects in Scryer framework. Available objects are stored in a stack which supports
 * thread-safe, lock-free obtain and recycle operations.
 */
public class Pool<T extends Poolable> {

	private final AtomicReference<PoolHelper> top = new AtomicReference<>(null);
	private final PoolableFactory<T> factory;

    private final int INITIAL_SIZE = 25;

    private final AtomicInteger nAlloc = new AtomicInteger(0);
    private final AtomicInteger nObtain = new AtomicInteger(0);
    private final AtomicInteger nRecycle = new AtomicInteger(0);

	public Pool(PoolableFactory<T> factory) {
		this.factory = factory;
        this.grow(INITIAL_SIZE);
	}

    private void grow(int size) {
        for (int i = 0; i < size; ++i) {
            PoolHelper newObject = factory.newInstance().setPool(this).poolHelper();
            PoolHelper currentTop;
            do {
                currentTop = top.get();
                newObject.next = currentTop;
            } while (!top.compareAndSet(currentTop, newObject));
        }
    }

	/*package*/ final void recycle(PoolHelper obj) {
		boolean success = false;

		while (!success) {
			final PoolHelper currentTop = top.get();
            obj.next = currentTop;
            success = top.compareAndSet(currentTop, obj);
		}

        nRecycle.getAndIncrement();

	}

	public final T obtain() {
		boolean success = false;
		PoolHelper candidate = null;

		while (!success) {
            candidate = top.get();
            if (candidate == null) {
                candidate = factory.newInstance().setPool(this).poolHelper();
                nAlloc.getAndIncrement();

                if (candidate.obj.getClass() == BinaryWebSocketFrame.class) {
                    Log.e("Pool", "Allocated");
                    //new Exception().printStackTrace();
                }

                success = true;
            }
            else {
                success = top.compareAndSet(candidate, candidate.next);
                if (success) {
                    nObtain.getAndIncrement();
                }
            }
		}

		candidate.prepareObject();

        //noinspection unchecked
		return (T) candidate.obj;
	}

    public void report() {
        Log.i("Pool", "Alloc = " + nAlloc.getAndSet(0) + ", Obtain = " + nObtain.getAndSet(0) + ", Recycle = " + nRecycle.getAndSet(0));
    }

}
