package com.exaeone.starwalls.pool;

/**
 * Created by saran on 24/06/2014.
 */
public abstract class Poolable {

	private PoolHelper poolHelper = null;

	/*package*/ final Poolable setPool(Pool pool) {
		this.poolHelper = new PoolHelper(this, pool);
		return this;
	}

	public final PoolHelper poolHelper() {
		return this.poolHelper;
	}

	public final void retain() {
        if (poolHelper != null) {
            this.poolHelper.retain();
        }
	}

	public final void release() {
		if (poolHelper != null) {
            this.poolHelper.release();
        }
        else {
            this.onRecycle();
        }
	}

    public void onObtain() {

    }

    public void onRecycle() {

    }

}
