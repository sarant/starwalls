package com.exaeone.starwalls.pool;

/**
 * Created by saran on 24/06/2014.
 */
public interface PoolableFactory<T> {

	public T newInstance();

}
