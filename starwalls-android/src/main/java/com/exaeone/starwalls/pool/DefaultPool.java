package com.exaeone.starwalls.pool;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saran on 10/02/15.
 */
public abstract class DefaultPool {
    public static Map<Class<? extends Poolable>, Pool<? extends Poolable>> poolsMap = new HashMap<>();

    public static <T extends Poolable> T obtain(Class<T> poolableClass) {
        Pool<? extends Poolable> pool = poolsMap.get(poolableClass);
        if (pool == null) {
            pool = new Pool<>(createFactoryForClass(poolableClass));
            poolsMap.put(poolableClass, pool);
        }

        //noinspection unchecked
        return (T) pool.obtain();
    }

    private static <T extends Poolable> PoolableFactory<T> createFactoryForClass(final Class<T> poolableClass) {
        try {
            return new PoolableFactory<T>() {
                private final Constructor<T> ctor = poolableClass.getConstructor();

                @Override
                public T newInstance() {
                    try {
                        return ctor.newInstance();
                    }
                    catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            };
        }
        catch (NoSuchMethodException e) {
            throw new RuntimeException(poolableClass.getSimpleName() + " does not have a default constructor");
        }
    }
}
