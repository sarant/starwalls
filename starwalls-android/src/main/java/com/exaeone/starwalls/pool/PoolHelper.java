package com.exaeone.starwalls.pool;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Helper class implementing reference counting for poolable objects.
 * This object also serves as nodes in the lock-free stack.
 */
/*package*/ final class PoolHelper {

	/*package*/ final Poolable obj;
	/*package*/ final Pool pool;

	/**
	 * The pool holder of the next object in the pool.
	 * Null when object is removed from the pool, or is at the tail of the pool's queue.
	 * */
	/*package*/ volatile PoolHelper next = null;

	private final AtomicInteger refCount = new AtomicInteger();

	/*package*/ PoolHelper(Poolable obj, Pool pool) {
		if (obj == null || pool == null) {
			throw new NullPointerException();
		}

		this.obj = obj;
		this.pool = pool;
	}

	/*package*/ void prepareObject() {
        next = null;
		refCount.set(1);
		obj.onObtain();
	}

	/*package*/ void retain() {
		int currentCount;
		do {
			currentCount = refCount.get();

			if (currentCount <= 0) {
				throw new UnsupportedOperationException("Cannot retain: refCount <= 0");
			}

		} while (!refCount.compareAndSet(currentCount, currentCount + 1));
	}

	/*package*/ void release() {
		int currentCount;
		do {
			currentCount = refCount.get();

			if (currentCount <= 0) {
				throw new UnsupportedOperationException("Cannot release: refCount <= 0");
			}

		} while (!refCount.compareAndSet(currentCount, currentCount - 1));

		if (currentCount == 1) {
			obj.onRecycle();
			pool.recycle(this);
		}
	}

}
