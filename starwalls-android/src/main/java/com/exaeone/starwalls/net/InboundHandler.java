package com.exaeone.starwalls.net;

import java.io.IOException;

/**
 * Created by saran on 14/02/15.
 */
public interface InboundHandler {

    public void handle(WebSocketConnection conn) throws IOException;

}
