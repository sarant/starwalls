package com.exaeone.starwalls.net;

import android.util.Base64;
import com.exaeone.starwalls.pool.DefaultPool;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by saran on 14/02/15.
 */
public class WebSocketHandshakeHandler implements InboundHandler {

    // HTTP RFC date format, see http://stackoverflow.com/questions/7707555/
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);

    private MessageDigest sha1;
    private String subprotocol = null;

    public WebSocketHandshakeHandler() {
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            sha1 = MessageDigest.getInstance("SHA1");
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public WebSocketHandshakeHandler setSubprotocol(String subprotocol) {
        /**
         * /subprotocol/ ... The empty string is not the
         *  same as the null value for these purposes and is not a legal
         *  value for this field.
         */
        if (subprotocol == null || subprotocol.length() == 0) {
            this.subprotocol = null;
        }
        else {
            this.subprotocol = subprotocol;
        }

        return this;
    }

    private ByteBuffer singleByte = ByteBuffer.allocate(1);

    @Override
    public void handle(WebSocketConnection conn) throws IOException {
        if (conn.ch.read(singleByte) == 1) {
            singleByte.rewind();
            if (addToLine(singleByte.get(0))) {
                if (parseLine()) {
                    if (headerComplete) {
                        if (validateHandshake()) {
                            success(conn);
                        } else {
                            fail(conn);
                        }
                    }
                    else {
                        conn.touch();
                    }
                }
                else {
                    conn.handshakeFailed();
                }
            }
            else if (linePos == MAX_LINE_LENGTH) {
                conn.handshakeFailed();
            }
            else {
                conn.touch();
            }
        }
    }

    private boolean firstReceived = false;
    private final Pattern firstPattern = Pattern.compile("(GET|HEAD|POST|PUT|DELETE|TRACE|CONNECT)\\s+([^\\s]+)\\s+HTTP/([1-9][0-9]*)\\.([1-9][0-9]*)");
    private final Pattern restPattern = Pattern.compile("([A-Za-z0-9\\-]+)\\s*:\\s*(.+)");

    private String method;
    private String path;
    private int majorVersion;
    private int minorVersion;

    private Map<String, String> headerFields = new HashMap<>();
    private boolean headerComplete = false;


    /**
     * Parses the string {@code line}
     * @return {@code true} if line is valid for an HTTP request header
     */
    private boolean parseLine() {
        if (!firstReceived) {
            Matcher m = firstPattern.matcher(line);
            if (m.matches()) {
                firstReceived = true;
                method = m.group(1);
                path = m.group(2);
                majorVersion = Integer.decode(m.group(3));
                minorVersion = Integer.decode(m.group(4));
                return true;
            } else {
                return false;
            }
        }
        else if (line.length() > 0) {
            Matcher m = restPattern.matcher(line);
            if (m.matches()) {
                headerFields.put(m.group(1).toLowerCase(), m.group(2));
                return true;
            }
            else {
                return false;
            }
        }
        else {
            headerComplete = true;
            return true;
        }
    }

    private String webSocketAccept;

    /**
     * Reference:
     * [RFC] The WebSocket Protocol
     * RFC6455, December 2011
     * https://tools.ietf.org/html/rfc645
     */

    private boolean validateHandshake() {
        return checkHTTP() && checkHost() &&
                checkUpgrade() && checkConnection() &&
                checkSecWebSocketKey() && checkSecWebSocketVersion() &&
                checkOrigin() && checkSecWebSocketProtocol();
    }

    /**
     * [RFC] 4.2.1.  Reading the Client's Opening Handshake
     *
     * ...
     *
     * The client's opening handshake consists of the following parts.  If
     * the server, while reading the handshake, finds that the client did
     * not send a handshake that matches the description below (note that as
     * per [RFC2616], the order of the header fields is not important, the server MUST stop
     * processing the client's handshake and return an HTTP response with an
     * appropriate error code (such as 400 Bad Request).
     */

    /**
     * 1.   An HTTP/1.1 or higher GET request, including a "Request-URI"
     *      [RFC2616] that should be interpreted as a /resource name/
     *      defined in Section 3 (or an absolute HTTP/HTTPS URI containing
     */
    private boolean checkHTTP() {
        return method.equals("GET") && (majorVersion > 1 || (majorVersion == 1 && minorVersion >= 1));
    }

    /**
     * 2.   A |Host| header field containing the server's authority.
     */
    private boolean checkHost() {
        return headerFields.containsKey("host");
    }

    /**
     * 3.   An |Upgrade| header field containing the value "websocket",
     *      treated as an ASCII case-insensitive value.
     */
    private boolean checkUpgrade() {
        String value = headerFields.get("upgrade");
        if (value != null && value.toLowerCase().equals("websocket")) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 4.   A |Connection| header field that includes the token "Upgrade",
     *      treated as an ASCII case-insensitive value.
     */
    private boolean checkConnection() {
        String value = headerFields.get("connection");
        if (value != null && value.toLowerCase().equals("upgrade")) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 5.   A |Sec-WebSocket-Key| header field with a base64-encoded (see
     *      Section 4 of [RFC4648]) value that, when decoded, is 16 bytes in
     *      length.
     */
    private boolean checkSecWebSocketKey() {
        String value = headerFields.get("sec-websocket-key");
        if (value != null) {

            /**
             * [RFC] S 4.2.2:
             *
             * 4.  Establish the following information:
             *     ...
             *     /key/
             *     The |Sec-WebSocket-Key| header field in the client's handshake
             *     includes a base64-encoded value that, if decoded, is 16 bytes
             *     in length.  This (encoded) value is used in the creation of
             *     the server's handshake to indicate an acceptance of the
             *     connection.  It is not necessary for the server to base64-
             *     decode the |Sec-WebSocket-Key| value.
             *
             * 5.  If the server chooses to accept the incoming connection, it MUST
             *     reply with a valid HTTP response indicating the following.
             *     ...
             *     4.  A |Sec-WebSocket-Accept| header field.  The value of this
             *         header field is constructed by concatenating /key/, defined
             *         above in step 4 in Section 4.2.2, with the string "258EAFA5-
             *         E914-47DA-95CA-C5AB0DC85B11", taking the SHA-1 hash of this
             *         concatenated value to obtain a 20-byte value and base64-
             *         encoding (see Section 4 of [RFC4648]) this 20-byte hash.
             */

            byte[] digest = sha1.digest((value + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes());
            webSocketAccept = Base64.encodeToString(digest, Base64.NO_WRAP);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 7.   Optionally, an |Origin| header field.  This header field is sent
     *      by all browser clients.  A connection attempt lacking this
     *      header field SHOULD NOT be interpreted as coming from a browser
     *      client.
     */
    private boolean checkOrigin() {
        // Chromecast IS a browser client, so we require it.
        return headerFields.containsKey("origin");
    }

    /**
     * 6.   A |Sec-WebSocket-Version| header field, with a value of 13.
     */
    private boolean checkSecWebSocketVersion() {
        String value = headerFields.get("sec-websocket-version");
        if (value != null && value.equals("13")) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 8.   Optionally, a |Sec-WebSocket-Protocol| header field, with a list
     *      of values indicating which protocols the client would like to
     *      speak, ordered by preference.
     */
    private boolean checkSecWebSocketProtocol() {
        String value = headerFields.get("sec-websocket-protocol");
        if (subprotocol == null) {
            return true;
        }
        else if (value != null) {
            String[] protocols = value.replaceAll("\\s+", "").split(",");
            for (int i = 0; i < protocols.length; ++i) {
                if (protocols[i].equals(subprotocol)) {
                    return true;
                }
            }
            return false;
        }
        else {
            return false;
        }
    }

    private static final int MAX_LINE_LENGTH = 16384;
    private String line;
    private byte lineBuffer[] = new byte[MAX_LINE_LENGTH];
    private int linePos = 0;
    private boolean waitingForLF = false;

    /**
     * Attempt to add a character to line
     * @param c character to add to line
     * @return {@code true} iff line is complete. In this case the variable {@code line} is set to the line just read.
     */
    private boolean addToLine(byte c) {
        if (linePos < MAX_LINE_LENGTH) {
            if (waitingForLF) {
                if (c == '\n') {
                    line = new String(lineBuffer, 0, linePos);
                    linePos = 0;
                    waitingForLF = false;
                    return true;
                }
                else {
                    waitingForLF = false;
                    lineBuffer[linePos++] = '\r';
                    if (linePos < MAX_LINE_LENGTH) {
                        lineBuffer[linePos++] = c;
                    }
                }

            }
            else {
                if (c == '\r') {
                    waitingForLF = true;
                }
                else {
                    lineBuffer[linePos++] = c;
                }
            }
        }

        return false;
    }

    private String dateString() {
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    private void success(WebSocketConnection conn) {
        if (subprotocol != null) {
            conn.sendBuffer((NetworkBuffer) DefaultPool.obtain(NetworkBuffer.class).putBytes((
                    "HTTP/1.1 101 Switching Protocols\r\n" +
                            "Connection: Upgrade\r\n" +
                            "Date: " + dateString() + "\r\n" +
                            "Sec-WebSocket-Accept: " + webSocketAccept + "\r\n" +
                            "Sec-WebSocket-Protocol: " + subprotocol + "\r\n" +
                            "Upgrade: websocket\r\n" +
                            "\r\n"
            ).getBytes()));
        }
        else {
            conn.sendBuffer((NetworkBuffer) DefaultPool.obtain(NetworkBuffer.class).putBytes((
                    "HTTP/1.1 101 Switching Protocols\r\n" +
                            "Connection: Upgrade\r\n" +
                            "Date: " + dateString() + "\r\n" +
                            "Sec-WebSocket-Accept: " + webSocketAccept + "\r\n" +
                            "Upgrade: websocket\r\n" +
                            "\r\n"
            ).getBytes()));
        }

        conn.handshakeSuccess();
    }

    /**
     * [RFC] 4.4.  Supporting Multiple Versions of WebSocket Protocol
     * If the server doesn't support the requested version, it MUST respond with a
     * |Sec-WebSocket-Version| header field ... containing all versions it is willing to use.
     */
    private void fail(WebSocketConnection conn) {
        conn.sendBuffer((NetworkBuffer) DefaultPool.obtain(NetworkBuffer.class).putBytes((
                "HTTP/1.1 400 Bad Request\r\n" +
                        "Content-Length: 0\r\n" +
                        "Date: " + dateString() + "\r\n" +
                        "Sec-WebSocket-Version: 13\r\n" +
                        "\r\n"
        ).getBytes()));

        conn.handshakeFailed();
    }

}
