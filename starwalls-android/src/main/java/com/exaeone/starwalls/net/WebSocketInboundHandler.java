package com.exaeone.starwalls.net;

import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.pool.DefaultPool;
import com.exaeone.starwalls.util.PoolableBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by saran on 14/02/15.
 */
public class WebSocketInboundHandler implements InboundHandler {

    // Bail if a frame is bigger than 50M
    private static final long MAX_FRAME_SIZE = 50 * 1024 * 1024;

    private static final int OPCODE_CONTINUE = 0;
    private static final int OPCODE_TEXT = 1;
    private static final int OPCODE_BINARY = 2;
    private static final int OPCODE_CLOSE = 8;
    private static final int OPCODE_PING = 9;
    private static final int OPCODE_PONG = 10;

    PoolableBuffer headerBuffer = new PoolableBuffer(32);
    PoolableBuffer payloadBuffer = null;

    private boolean continuing = false;

    private State state;

    public WebSocketInboundHandler() {
        setState(State.HEADER);
    }

    @Override
    public void handle(WebSocketConnection conn) throws IOException {
        /**
         *     0               1               2               3
         *     0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
         *    +-+-+-+-+-------+-+-------------+-------------------------------+
         * 0  |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
         *    |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
         *    |N|V|V|V|       |S|             |   (if payload len==126/127)   |
         *    | |1|2|3|       |K|             |                               |
         *    +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
         * 4  |     Extended payload length continued, if payload len == 127  |
         *    + - - - - - - - - - - - - - - - +-------------------------------+
         * 8  |                               |Masking-key, if MASK set to 1  |
         *    +-------------------------------+-------------------------------+
         * 12 | Masking-key (continued)       |          Payload Data         |
         *    +-------------------------------- - - - - - - - - - - - - - - - +
         */

        switch (state) {
            case HEADER:
                readHeader(conn);
                break;
            case X_PAYLOAD_LEN:
                readXPayloadLen(conn);
                break;
            case MASKING_KEY:
                readMaskingKey(conn);
                break;
            case PAYLOAD:
                readPayload(conn);
                break;
        }
    }

    private boolean fin = false;
    private boolean rsv1 = false;
    private boolean rsv2 = false;
    private boolean rsv3 = false;
    private int opcode = 0;
    private boolean mask = false;
    private long payloadLen = 0;
    private int maskingKey = 0;

    private void readHeader(WebSocketConnection conn) throws IOException {
        ByteBuffer nioBuffer = headerBuffer.nioBufferLockForWrite();
        int bytesRead = conn.ch.read(nioBuffer);
        headerBuffer.nioBufferUnlock(nioBuffer);

        if (bytesRead > 0) {
            if (headerBuffer.writerIndex() == 2) {
                int raw = headerBuffer.getShort();

                fin  = ((raw & 32768) != 0);
                rsv1 = ((raw & 16384) != 0);
                rsv2 = ((raw & 8192 ) != 0);
                rsv3 = ((raw & 4096 ) != 0);
                mask = ((raw & 128  ) != 0);

                opcode = (raw & 3840) >> 8;
                payloadLen = raw & 127;

                if (!validateHeader()) {
                    conn.close();
                }

                if (!fin) {
                    continuing = true;
                }
                else if (opcode == OPCODE_CONTINUE || opcode == OPCODE_TEXT || opcode == OPCODE_BINARY) {
                    continuing = false;
                }

                if (payloadLen > 125) {
                    setState(State.X_PAYLOAD_LEN);
                }
                else if (mask) {
                    setState(State.MASKING_KEY);
                }
                else {
                    setState(State.PAYLOAD);
                }
            }

            conn.touch();
        }
    }

    private boolean validateHeader() {
        boolean valid = true;

        if (rsv1 || rsv2 || rsv3) {
            /**
             * [RFC] 5. Data Framing
             *
             * 5.2.  Base Framing Protocol
             *
             *    RSV1, RSV2, RSV3:  1 bit each
             *
             *      MUST be 0 unless an extension is negotiated that defines meanings
             *      for non-zero values.  If a nonzero value is received and none of
             *      the negotiated extensions defines the meaning of such a nonzero
             *      value, the receiving endpoint MUST _Fail the WebSocket
             *      Connection_.
             */

            valid = false;
        }
        else if (!mask) {
            /**
             * [RFC] 5. Data Framing
             *
             * 5.1.  Overview
             *
             *    The server MUST close the connection upon receiving a
             *    frame that is not masked.
             */

            valid = false;
        }
        else if ((opcode >= 3 && opcode <= 7) || (opcode >= 11 && opcode <= 15)) {
            /**
             * [RFC] 5. Data Framing
             *
             * 5.2.  Base Framing Protocol
             *
             *    Opcode:  4 bits
             *
             *       Defines the interpretation of the "Payload data".  If an unknown
             *       opcode is received, the receiving endpoint MUST _Fail the
             *       WebSocket Connection_.
             *
             *       *  %x3-7 are reserved for further non-control frames
             *       *  %xB-F are reserved for further control frame
             */

            valid = false;
        }
        else if (opcode == OPCODE_CLOSE) {
            /**
             * [RFC] 7.  Closing the Connection
             *
             * 7.1.1.  Close the WebSocket Connection
             *
             *    To _Close the WebSocket Connection_, an endpoint closes the
             *    underlying TCP connection.  An endpoint SHOULD use a method that
             *    cleanly closes the TCP connection, as well as the TLS session, if
             *    applicable, discarding any trailing bytes that may have been
             *    received.  An endpoint MAY close the connection via any means
             *    available when necessary, such as when under attack.
             */

            System.out.println("client sent a close frame");
            valid = false;
        }
        else if (continuing && (opcode == OPCODE_TEXT || opcode == OPCODE_BINARY)) {
            valid = false;
        }
        else if (!continuing && opcode == OPCODE_CONTINUE) {
            valid = false;
        }
        else if (!fin && (opcode == OPCODE_PING || opcode == OPCODE_PONG)) {
            /**
             * [RFC] 5. Data Framing
             *
             * 5.4.  Fragmentation
             *
             *    The following rules apply to fragmentation:
             *
             *    o  Control frames themselves MUST NOT be fragmented.
             */

            valid = false;
        }

        return valid;
    }

    private void readXPayloadLen(WebSocketConnection conn) throws IOException {
        ByteBuffer nioBuffer = headerBuffer.nioBufferLockForWrite();
        int bytesRead = conn.ch.read(nioBuffer);
        headerBuffer.nioBufferUnlock(nioBuffer);

        if (bytesRead > 0) {
            boolean complete = false;

            if (payloadLen == 126 && headerBuffer.writerIndex() == 2) {
                payloadLen = headerBuffer.getUnsignedShort();
                complete = true;
            }
            else if (payloadLen == 127 && headerBuffer.writerIndex() == 8) {
                payloadLen = headerBuffer.getLong();
                complete = true;
            }

            if (complete) {
                if (mask) {
                    setState(State.MASKING_KEY);
                }
                else {
                    setState(State.PAYLOAD);
                }
            }

            conn.touch();
        }
    }

    private void readMaskingKey(WebSocketConnection conn) throws IOException {
        ByteBuffer nioBuffer = headerBuffer.nioBufferLockForWrite();
        int bytesRead = conn.ch.read(nioBuffer);
        headerBuffer.nioBufferUnlock(nioBuffer);

        if (bytesRead > 0) {
            if (headerBuffer.writerIndex() == 4) {
                maskingKey = headerBuffer.getInt();
                setState(State.PAYLOAD);
            }

            conn.touch();
        }
    }

    private void readPayload(WebSocketConnection conn) throws IOException {
        ByteBuffer nioBuffer = payloadBuffer.nioBufferLockForWrite();
        int bytesRead = conn.ch.read(nioBuffer);
        payloadBuffer.nioBufferUnlock(nioBuffer);

        if (payloadLen == 0 || bytesRead > 0) {
            while (payloadBuffer.writerIndex() - payloadBuffer.readerIndex() >= 4) {
                int masked = payloadBuffer.getInt();
                payloadBuffer.writeInt(payloadBuffer.readerIndex() - 4, masked ^ maskingKey);
            }

            if (payloadBuffer.writerIndex() == payloadLen) {
                if (payloadBuffer.readerIndex() < payloadBuffer.writerIndex()) {
                    payloadBuffer.limit(paddedLen(payloadLen));
                    int masked = payloadBuffer.getInt();
                    payloadBuffer.writeInt(payloadBuffer.readerIndex() - 4, masked ^ maskingKey);
                    payloadBuffer.limit((int) payloadLen);
                }

                payloadBuffer.rewind();

                if (opcode == OPCODE_BINARY) {
                    payloadBuffer.rewind();
                    EventBus.DEFAULT.postSynchronously(
                            DefaultPool.obtain(WebSocketConnection.OnIncomingBinary.class)
                                    .setPayload(payloadBuffer),
                            conn
                    );
                }
                else {
                    payloadBuffer.release();
                }

                payloadBuffer = null;
                setState(State.HEADER);
            }

            conn.touch();
        }
    }

    private boolean setState(State state) {
        this.state = state;
        headerBuffer.clear();

        boolean success;

        if (state == State.HEADER) {
            headerBuffer.limit(2);
            success = true;
        }
        else if (state == State.X_PAYLOAD_LEN) {
            if (payloadLen == 126) {
                headerBuffer.limit(2);
                success = true;
            }
            else if (payloadLen == 127) {
                headerBuffer.limit(8);
                success = true;
            }
            else {
                success = false;
            }
        }
        else if (state == State.MASKING_KEY) {
            headerBuffer.limit(4);
            success = true;
        }
        else if (state == State.PAYLOAD) {
            if (!continuing) {
                if (payloadLen < 0 || payloadLen > MAX_FRAME_SIZE) {
                    success = false;
                }
                else if (paddedLen(payloadLen) > PoolableBuffer.DEFAULT_CAPACITY) {
                    payloadBuffer = new PoolableBuffer(paddedLen(payloadLen));
                    payloadBuffer.limit((int) payloadLen);
                    success = true;
                }
                else {
                    payloadBuffer = DefaultPool.obtain(PoolableBuffer.class);
                    payloadBuffer.limit((int) payloadLen);
                    success = true;
                }
            }
            else {
                if (payloadLen < 0 || payloadBuffer.limit() + payloadLen > MAX_FRAME_SIZE) {
                    success = false;
                }
                else if (payloadBuffer.limit() + paddedLen(payloadLen) > payloadBuffer.capacity()) {
                    PoolableBuffer grownBuffer = new PoolableBuffer(payloadBuffer.limit() + paddedLen(payloadLen));
                    grownBuffer.putBytes(payloadBuffer);
                    grownBuffer.readerIndex(grownBuffer.writerIndex());
                    payloadBuffer.release();
                    payloadBuffer = grownBuffer;
                    payloadLen += payloadBuffer.limit();
                    grownBuffer.limit((int) payloadLen);
                    success = true;
                }
                else {
                    payloadBuffer.limit(payloadBuffer.limit() + paddedLen(payloadLen));
                    payloadLen += payloadBuffer.limit();
                    success = true;
                }
            }
        }
        else {
            success = false;
        }

        return success;
    }

    private int paddedLen(long len) {
        return 4 * (int) Math.ceil(len / 4.);
    }

    private enum State {
        HEADER,
        X_PAYLOAD_LEN,
        MASKING_KEY,
        PAYLOAD
    }

}
