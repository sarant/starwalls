package com.exaeone.starwalls.net;

import android.util.Log;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.util.ConcurrentLinkedDeque;
import com.exaeone.starwalls.util.PoolableBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;
/**
 * Created by saran on 14/02/15.
 */
public class WebSocketConnection {

    public static final String TAG = "WebSocketConnection";

    private volatile int idleTimeout = 1000;
    private volatile long lastTouch;
    private volatile InboundHandler inboundHandler;
    private final Thread inboundThread = new Thread(new ConnectionInboundRunnable(), "WebSocketConnection inbound");
    private final Thread outboundThread = new Thread(new ConnectionOutboundRunnable(), "WebSocketConnection outbound");

    /*package*/ SocketChannel ch;

    private final AtomicReference<State> state = new AtomicReference<>(State.HANDSHAKE_WAIT);
    private final AtomicBoolean isCloseNotified = new AtomicBoolean(false);

    private enum State {
        HANDSHAKE_WAIT (true, false),
        ACCEPT_WAIT (true, true),
        ACCEPTED (true, true),
        DRAINING (false, false),
        CLOSED (false, false);

        final boolean active;
        final boolean canSend;

        State(boolean active, boolean canSend) {
            this.active = active;
            this.canSend = canSend;
        }
    }

    private final ConcurrentLinkedDeque<NetworkBuffer> outboundQueue = new ConcurrentLinkedDeque<>();

    // Keep a separate list of HashMap keys to avoid allocating an iterator in the outbound loop!
    private final List<Class> realtimeClassList = new ArrayList<>();
    private final ConcurrentHashMap<Class, NetworkBuffer> realtimeMap = new ConcurrentHashMap<>();
    private volatile boolean hasRealtime = false;

    // ConcurrentHashMap does not allow null values, so we use this singleton as a placeholder.
    private static final NetworkBuffer EMPTY_BUFFER = new NetworkBuffer(0);

    public static WebSocketConnection start(SocketChannel ch, String subprotocol) {
        if (ch == null) {
            Log.e(TAG, "SocketChannel is null");
            return null;
        }
        else if (!ch.isBlocking()) {
            Log.e(TAG, "SocketChannel must be in blocking mode");
            return null;
        }
        else {
            WebSocketConnection conn = new WebSocketConnection();
            conn.ch = ch;

            conn.inboundHandler = new WebSocketHandshakeHandler().setSubprotocol(subprotocol);
            conn.touch();

            conn.inboundThread.start();
            conn.outboundThread.start();

            return conn;
        }
    }

    private WebSocketConnection() {
    }

    public void accept() {
        State currentState = state.get();
        if (currentState == State.ACCEPT_WAIT || currentState == State.ACCEPTED) {
            state.compareAndSet(currentState, State.ACCEPTED);
        }
        else if (!currentState.canSend) {
            throw new IllegalStateException("Cannot accept() a connection which is not ready");
        }
    }

    /*package*/ void sendBuffer(NetworkBuffer buf) {
        if (state.get().active) {
            outboundQueue.add(buf);
        }
    }

    public void send(NetworkBuffer buf) {
        if (state.get().canSend) {
            outboundQueue.add(buf);
            LockSupport.unpark(outboundThread);
        }
    }

    public void sendRealtime(Class realtimeClass, NetworkBuffer buf) {
        if (state.get().canSend) {
            NetworkBuffer oldBuffer = realtimeMap.put(realtimeClass, buf);
            hasRealtime = true;

            if (oldBuffer != null && oldBuffer != EMPTY_BUFFER) {
                oldBuffer.release();
            }
            else if (oldBuffer == null) {
                // Keep a separate list of hashmap keys to avoid allocating an iterator in a tight loop!
                realtimeClassList.add(realtimeClass);
            }

            LockSupport.unpark(outboundThread);
        }
    }

    public void close() {
        notifyClose();
        State currentState = state.get();
        if (currentState.active && state.compareAndSet(currentState, State.DRAINING)) {
            LockSupport.unpark(outboundThread);
        }
    }

    private void teardown() {
        notifyClose();
        State currentState = state.get();
        if (currentState != State.CLOSED && state.compareAndSet(currentState, State.CLOSED)) {
            try {
                ch.close();
                LockSupport.unpark(outboundThread);
            } catch (IOException e) {
                // Don't do anything...
            } finally {
                ch = null;
            }
        }
    }

    private void notifyClose() {
        if (isCloseNotified.compareAndSet(false, true)) {
            if (state.get() == State.ACCEPTED) {
                EventBus.DEFAULT.post(new OnClose(), this);
            }
        }
    }

    /*package*/ void handshakeSuccess() {
        State currentState = state.get();
        if (currentState == State.HANDSHAKE_WAIT) {
            touch();
            inboundHandler = new WebSocketInboundHandler();
            if (state.compareAndSet(currentState, State.ACCEPT_WAIT)) {
                idleTimeout = 1000000;
                EventBus.DEFAULT.post(new OnOpen(), this);
            }
        }
    }

    /*package*/ void handshakeFailed() {
        close();
    }

    /*package*/ void touch() {
        lastTouch = System.currentTimeMillis();
    }

    public static class OnOpen extends Event {
        @Override
        public void onRecycle() {
            WebSocketConnection source = (WebSocketConnection) source();
            if (source != null && source.state.get() == State.ACCEPT_WAIT) {
                Log.w(TAG, "No one accept() this connection, so we are just going to close it.");
                source.close();
            }
            super.onRecycle();
        }
    }

    public static class OnClose extends Event {
    }

    public static class OnIncomingBinary extends Event {
        private PoolableBuffer payload = null;

        @Override
        public void onRecycle() {
            if (payload != null) {
                payload.release();
                payload = null;
            }
            super.onRecycle();
        }

        /*package*/ OnIncomingBinary setPayload(PoolableBuffer payload) {
            this.payload = payload;
            return this;
        }

        public PoolableBuffer getPayload() {
            return this.payload;
        }
    }

    private class ConnectionInboundRunnable implements Runnable {

        @Override
        public void run() {
            while (state.get().active) {
                try {
                    if (!ch.isOpen()) {
                        teardown();
                    }
                    else {
                        inboundHandler.handle(WebSocketConnection.this);
                    }
                }
                catch (Exception e) {
                    if (!(e instanceof AsynchronousCloseException)) {
                        e.printStackTrace();
                    }
                    teardown();
                }
            }
        }

    }

    private class ConnectionOutboundRunnable implements Runnable {

        private void writeToChannel(NetworkBuffer outBuffer) throws IOException {
            touch();

            ByteBuffer nioBuffer = outBuffer.nioBufferLockForRead();
            while (nioBuffer.remaining() > 0) {
                ch.write(nioBuffer);
            }
            outBuffer.nioBufferUnlock(nioBuffer);

            if (outBuffer.onSendEvent != null) {
                EventBus.DEFAULT.post(outBuffer.onSendEvent, this);
            }

            outBuffer.release();
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_MORE_FAVORABLE);

            State currentState;
            while ((currentState = state.get()) != State.CLOSED) {
                try {
                    if (!ch.isOpen()) {
                        teardown();
                    }
                    else {
                        // Write out urgent realtime updates to socket first
                        if (hasRealtime) {
                            hasRealtime = false;
                            final int realtimeClassCount = realtimeClassList.size();
                            for (int i = 0; i < realtimeClassCount; ++i) {
                                NetworkBuffer outBuffer = realtimeMap.replace(realtimeClassList.get(i), EMPTY_BUFFER);
                                if (outBuffer != null && outBuffer != EMPTY_BUFFER) {
                                    writeToChannel(outBuffer);
                                }
                            }
                        }

                        // Then write out stuff from standard queue
                        NetworkBuffer outBuffer = outboundQueue.poll();
                        if (outBuffer != null) {
                            writeToChannel(outBuffer);
                        }

                        boolean proceedImmediately = hasRealtime || !outboundQueue.isEmpty();

                        if (!proceedImmediately) {
                            if (currentState == State.DRAINING) {
                                teardown();
                            }
                            else if (System.currentTimeMillis() - lastTouch > idleTimeout) {
                                close();
                            }
                            else {
                                LockSupport.parkUntil(this, idleTimeout);
                            }
                        }

                    }
                }
                catch (Exception e) {
                    if (!(e instanceof AsynchronousCloseException)) {
                        e.printStackTrace();
                    }
                    teardown();
                }
            }

            while (!outboundQueue.isEmpty()) {
                outboundQueue.poll().release();
            }
        }

    }

}
