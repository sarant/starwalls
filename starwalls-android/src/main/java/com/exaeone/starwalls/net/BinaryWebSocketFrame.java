package com.exaeone.starwalls.net;

import com.exaeone.starwalls.util.PoolableBuffer;

/**
 * Created by saran on 15/02/15.
 */
public class BinaryWebSocketFrame extends NetworkBuffer {

    private boolean committed = false;

    public BinaryWebSocketFrame() {
        super();
    }

    public BinaryWebSocketFrame(int capacity) {
        super(capacity);
    }

    @Override
    public PoolableBuffer clear() {
        committed = false;
        super.clear();
        readerIndex = 10;
        writerIndex = 10;
        return this;
    }

    @Override
    public PoolableBuffer compact() {
        throw new UnsupportedOperationException();
    }

    @Override
    public PoolableBuffer rewind() {
        super.rewind();
        readerIndex = 10;
        return this;
    }

    @Override
    protected void checkLock() {
        if (committed) {
            throw new UnsupportedOperationException("This message has been committed and cannot be modified");
        }
        super.checkLock();
    }

    @Override
    public PoolableBuffer readerIndex(int readerIndex) {
        if (readerIndex < 10) {
            throw new IllegalArgumentException("First ten bytes are reserved for WebSocket header");
        }
        return super.readerIndex(readerIndex);
    }

    @Override
    public PoolableBuffer writerIndex(int writerIndex) {
        if (writerIndex < 10) {
            throw new IllegalArgumentException("First ten bytes are reserved for WebSocket header");
        }
        return super.writerIndex(writerIndex);
    }

    public NetworkBuffer commit() {
        checkLock();

        committed = true;

        limit = writerIndex;
        byteBuffer.limit(limit);

        long payloadLen = writerIndex - 10;

        /**
         *  Default header, 16-bit two's complement = -32256
         *
         *   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
         *  +-+-+-+-+-------+-+-------------+
         *  |1|0|0|0|0 0 1 0|0|0 0 0 0 0 0 0|
         *  | | | | |       | |             |
         *  |F|R|R|R|OPCODE |M| PAYLOAD LEN |
         *  |I|S|S|S| BINARY|A|             |
         *  |N|V|V|V| (= 2) |S|             |
         *  | |1|2|3|       |K|             |
         *  +-+-+-+-+-------+-+-------------+
         *
         */

        short header = (short) -32256;

        if (payloadLen < 126) {
            readerIndex = 8;

            header += payloadLen;
            byteBuffer.position(readerIndex);
            byteBuffer.putShort(header);
        }
        else if (payloadLen < 65536) {
            readerIndex = 6;

            header += 126;
            byteBuffer.position(readerIndex);
            byteBuffer.putShort(header);
            byteBuffer.putShort((short) payloadLen);
        }
        else {
            readerIndex = 0;

            header += 127;
            byteBuffer.position(readerIndex);
            byteBuffer.putShort(header);
            byteBuffer.putLong(payloadLen);
        }

        return this;
    }

}
