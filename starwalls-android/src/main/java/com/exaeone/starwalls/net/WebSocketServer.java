package com.exaeone.starwalls.net;

import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by saran on 14/02/15.
 */
public class WebSocketServer {

    private ServerSocketChannel server;
    private Thread serverThread;
    private String subprotocol;

    private final AtomicBoolean isActive = new AtomicBoolean(false);

    public void start(int port) {
        start(port, null);
    }

    public void start(int port, String subprotocol) {
        if (isActive.compareAndSet(false, true)) {
            try {
                this.subprotocol = subprotocol;

                server = ServerSocketChannel.open();
                server.configureBlocking(true);
                server.socket().bind(new InetSocketAddress(port));

                serverThread = new Thread(new ServerRunnable(), "WebSocketServer");
                serverThread.start();

                EventBus.DEFAULT.post(new OnStart(), this);
            } catch (IOException e) {
                e.printStackTrace();
                isActive.set(false);
                server = null;
            }
        }
    }

    public void stop() {
        if (isActive.compareAndSet(true, false)) {
            EventBus.DEFAULT.post(new OnStop(), this);

            try {
                if (server != null && server.isOpen()) {
                    server.close();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                server = null;
            }
        }
    }

    private class ServerRunnable implements Runnable {
        @Override
        public void run() {
            while (isActive.get()) {
                try {
                    SocketChannel ch = server.accept();
                    ch.configureBlocking(true);
                    //ch.socket().setTcpNoDelay(true);
                    WebSocketConnection.start(ch, subprotocol);
                }
                catch (Exception e){
                    if (!(e instanceof AsynchronousCloseException)) {
                        e.printStackTrace();
                    }
                    stop();
                }
            }
            serverThread = null;
        }
    }

    // Event classes

    public static class OnStart extends Event {}
    public static class OnStop extends Event {}

}
