package com.exaeone.starwalls.net;

import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.util.PoolableBuffer;

/**
 * Created by saran on 28/02/15.
 */
public class NetworkBuffer extends PoolableBuffer {

    /*package*/ Event onSendEvent = null;

    public NetworkBuffer() {
        super();
    }

    @Override
    public void onRecycle() {
        this.onSendEvent = null;
        super.onRecycle();
    }

    public NetworkBuffer(int capacity) {
        super(capacity);
    }

    public void setOnSendEvent(Event evt) {
        this.onSendEvent = evt;
    }

}
