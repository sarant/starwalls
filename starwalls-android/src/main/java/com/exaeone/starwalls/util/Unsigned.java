package com.exaeone.starwalls.util;

/**
 * Created by saran on 17/03/15.
 */
public abstract class Unsigned {

    public static short ubyte(byte b) {
        if (b < 0) {
            return (short) (128 + (b & 0x7f));
        }
        else {
            return b;
        }
    }

    public static int ushort(short s) {
        if (s < 0) {
            return 32768 + (s & 0x7FFF);
        }
        else {
            return s;
        }
    }

    public static long uint(int i) {
        if (i < 0) {
            return 2147483648L + (i & 0x7FFFFFFF);
        }
        else {
            return i;
        }
    }

}
