package com.exaeone.starwalls.util;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by saran on 17/02/15.
 */
public class BufferStreamAdapter extends OutputStream {

    public PoolableBuffer buffer;

    @Override
    public void write(int oneByte) throws IOException {
        if (buffer != null) {
            buffer.putByte((byte) oneByte);
        }
        else {
            throw new NullPointerException();
        }
    }

    @Override
    public void write(byte[] bytes, int offset, int count) throws IOException {
        if (buffer != null) {
            buffer.putBytes(bytes, offset, count);
        }
        else {
            throw new NullPointerException();
        }
    }



}
