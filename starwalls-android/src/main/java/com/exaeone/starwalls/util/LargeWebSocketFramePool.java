package com.exaeone.starwalls.util;

import com.exaeone.starwalls.net.BinaryWebSocketFrame;
import com.exaeone.starwalls.pool.Pool;
import com.exaeone.starwalls.pool.PoolableFactory;

/**
 * Created by saran on 28/02/15.
 */
public class LargeWebSocketFramePool {

    private static Pool<BinaryWebSocketFrame> pool = new Pool<>(
            new PoolableFactory<BinaryWebSocketFrame>() {
                @Override
                public BinaryWebSocketFrame newInstance() {
                    return new BinaryWebSocketFrame(128 * 1024);
                }
            });

    public static BinaryWebSocketFrame obtain() {
        return pool.obtain();
    }

}
