package com.exaeone.starwalls.util;

import com.exaeone.starwalls.pool.Poolable;

import java.nio.ByteBuffer;

/**
 * Created by saran on 14/02/15.
 */
public class PoolableBuffer extends Poolable {

    // Use 1K buffers
    public static final int DEFAULT_CAPACITY = 1 * 1024;

    protected final ByteBuffer byteBuffer;
    private final ByteBuffer signBuffer = ByteBuffer.allocate(8);

    protected int readerIndex;
    protected int writerIndex;
    protected int limit;

    protected boolean nioLocked = false;
    protected LockMode lockMode;

    private enum LockMode {
        READ,
        WRITE
    }

    public PoolableBuffer() {
        this(DEFAULT_CAPACITY);
    }

    public PoolableBuffer(int capacity) {
        byteBuffer = ByteBuffer.allocateDirect(capacity);
        clear();
    }

    @Override
    public void onObtain() {
        super.onObtain();
        clear();
    }

    protected void checkLock() {
        nioCheckLock();
    }

    public PoolableBuffer clear() {
        checkLock();
        byteBuffer.clear();
        readerIndex = 0;
        writerIndex = 0;
        limit = byteBuffer.capacity();
        return this;
    }

    public PoolableBuffer compact() {
        checkLock();

        byteBuffer.position(readerIndex);
        byteBuffer.limit(writerIndex);
        byteBuffer.compact();

        limit -= readerIndex;
        byteBuffer.limit(limit);

        writerIndex -= readerIndex;
        readerIndex = 0;

        return this;
    }

    public PoolableBuffer rewind() {
        checkLock();
        readerIndex = 0;
        return this;
    }

    public int capacity() {
        return byteBuffer.capacity();
    }

    public int limit() {
        return limit;
    }

    public PoolableBuffer limit(int limit) {
        checkLock();
        this.limit = limit;
        byteBuffer.limit(limit);
        if (writerIndex > limit) {
            writerIndex = limit;
        }
        return this;
    }

    public int readableBytes() {
        if (readerIndex >= writerIndex) {
            return 0;
        }
        else {
            return writerIndex - readerIndex;
        }
    }

    public int writableBytes() {
        if (writerIndex >= limit) {
            return 0;
        }
        else {
            return limit - writerIndex;
        }
    }

    public int readerIndex() {
        return readerIndex;
    }

    public int writerIndex() {
        return writerIndex;
    }

    public PoolableBuffer readerIndex(int readerIndex) {
        checkLock();

        if (readerIndex > limit) {
            throw new IndexOutOfBoundsException("readerIndex must not exceed limit");
        }
        else if (readerIndex > writerIndex) {
            throw new IndexOutOfBoundsException("readerIndex must not exceed writerIndex");
        }
        else {
            this.readerIndex = readerIndex;
            return this;
        }
    }

    public PoolableBuffer writerIndex(int writerIndex) {
        checkLock();

        if (writerIndex > limit) {
            throw new IndexOutOfBoundsException("writerIndex must not exceed limit");
        }
        else if (writerIndex < readerIndex) {
            throw new IndexOutOfBoundsException("writerIndex must be less than readerIndex");
        }
        else {
            this.writerIndex = writerIndex;
            return this;
        }
    }

    private final void nioCheckLock() {
        if (nioLocked) {
            throw new UnsupportedOperationException("The buffer is NIO locked");
        }
    }

    public final ByteBuffer nioBufferLockForRead() {
        nioCheckLock();
        byteBuffer.position(readerIndex);
        byteBuffer.limit(writerIndex);
        nioLocked = true;
        lockMode = LockMode.READ;
        return byteBuffer;
    }

    public final ByteBuffer nioBufferLockForWrite() {
        nioCheckLock();
        byteBuffer.position(writerIndex);
        nioLocked = true;
        lockMode = LockMode.WRITE;
        return byteBuffer;
    }

    public final void nioBufferUnlock(ByteBuffer buf) {
        if (buf == byteBuffer && nioLocked) {
            if (lockMode == LockMode.READ) {
                readerIndex = byteBuffer.position();
                byteBuffer.limit(limit);
            }
            else if (lockMode == LockMode.WRITE) {
                writerIndex = byteBuffer.position();
            }
            nioLocked = false;
        }
        else {
            throw new IllegalArgumentException("The buffer specified is not the one that was previously locked");
        }
    }

    public byte readByte(int index) {
        checkLock();
        return byteBuffer.get(index);

    }

    public byte getByte() {
        byte out = readByte(readerIndex);
        readerIndex += 1;
        return out;
    }

    public PoolableBuffer getBytes(byte[] out) {
        checkLock();
        byteBuffer.position(readerIndex);
        byteBuffer.get(out);
        readerIndex = byteBuffer.position();

        return this;
    }

    public char readChar(int index) {
        checkLock();
        return byteBuffer.getChar(index);
    }

    public char getChar() {
        char out = readChar(readerIndex);
        readerIndex += 2;
        return out;
    }

    public short readUnsignedByte(int index) {
        checkLock();
        signBuffer.clear();
        signBuffer.put((byte) 0).put(byteBuffer.get(index)).flip();
        return signBuffer.getShort();
    }

    public short getUnsignedByte() {
        short out = readUnsignedByte(readerIndex);
        readerIndex += 1;
        return out;
    }

    public short readShort(int index) {
        checkLock();
        return byteBuffer.getShort(index);
    }

    public short getShort() {
        short out = readShort(readerIndex);
        readerIndex += 2;
        return out;
    }

    public int readUnsignedShort(int index) {
        checkLock();
        signBuffer.clear();
        signBuffer.putShort((short) 0).putShort(byteBuffer.getShort(index)).flip();
        return signBuffer.getInt();
    }

    public int getUnsignedShort() {
        int out = readUnsignedShort(readerIndex);
        readerIndex += 2;
        return out;
    }

    public int readInt(int index) {
        checkLock();
        return byteBuffer.getInt(index);
    }

    public int getInt() {
        int out = readInt(readerIndex);
        readerIndex += 4;
        return out;
    }

    public long readUnsignedInt(int index) {
        checkLock();
        signBuffer.clear();
        signBuffer.putInt(0).putInt(byteBuffer.getInt(index)).flip();
        return signBuffer.getLong();
    }

    public long getUnsignedInt() {
        long out = readUnsignedInt(readerIndex);
        readerIndex += 4;
        return out;
    }

    public long readLong(int index) {
        checkLock();
        return byteBuffer.getLong(index);
    }

    public long getLong() {
        long out = readLong(readerIndex);
        readerIndex += 8;
        return out;
    }

    public float readFloat(int index) {
        checkLock();
        return byteBuffer.getFloat(index);
    }

    public float getFloat() {
        float out = readFloat(readerIndex);
        readerIndex += 4;
        return out;
    }

    public double readDouble(int index) {
        checkLock();
        return byteBuffer.getDouble(index);
    }

    public double getDouble() {
        double out = readDouble(readerIndex);
        readerIndex += 8;
        return out;
    }

    public PoolableBuffer writeByte(int index, long b) {
        checkLock();
        byteBuffer.put(index, (byte) b);
        return this;
    }

    public PoolableBuffer putByte(byte b) {
        writeByte(writerIndex, b);
        writerIndex += 1;
        return this;
    }

    public PoolableBuffer putBytes(byte[] b) {
        checkLock();
        byteBuffer.position(writerIndex);
        byteBuffer.put(b);
        writerIndex += b.length;
        return this;
    }

    public PoolableBuffer putBytes(byte[] b, int offset, int count) {
        checkLock();
        byteBuffer.position(writerIndex);
        byteBuffer.put(b, offset, count);
        writerIndex += count;
        return this;
    }

    public PoolableBuffer putBytes(ByteBuffer src) {
        checkLock();
        byteBuffer.position(writerIndex);
        byteBuffer.put(src);
        writerIndex = byteBuffer.position();
        return this;
    }

    public PoolableBuffer putBytes(PoolableBuffer src) {
        checkLock();

        ByteBuffer srcBuffer = src.nioBufferLockForRead();
        byteBuffer.position(writerIndex);

        byteBuffer.put(srcBuffer);

        src.nioBufferUnlock(srcBuffer);
        writerIndex = byteBuffer.position();

        return this;
    }

    public PoolableBuffer writeChar(int index, char value) {
        checkLock();
        byteBuffer.putChar(index, value);
        return this;
    }
    
    public PoolableBuffer putChar(char value) {
        writeChar(writerIndex, value);
        writerIndex += 2;
        return this;
    }

    public PoolableBuffer writeShort(int index, long value) {
        checkLock();
        byteBuffer.putShort(index, (short) value);
        return this;
    }

    public PoolableBuffer putShort(long value) {
        writeShort(writerIndex, value);
        writerIndex += 2;
        return this;
    }

    public PoolableBuffer writeInt(int index, long value) {
        checkLock();
        byteBuffer.putInt(index, (int) value);
        return this;
    }

    public PoolableBuffer putInt(int value) {
        writeInt(writerIndex, value);
        writerIndex += 4;
        return this;
    }

    public PoolableBuffer writeLong(int index, long value) {
        checkLock();
        byteBuffer.putLong(index, value);
        return this;
    }

    public PoolableBuffer putLong(long value) {
        writeLong(writerIndex, value);
        writerIndex += 8;
        return this;
    }

    public PoolableBuffer writeFloat(int index, float value) {
        checkLock();
        byteBuffer.putFloat(index, value);
        return this;
    }

    public PoolableBuffer putFloat(float value) {
        writeFloat(writerIndex, value);
        writerIndex += 4;
        return this;
    }

    public PoolableBuffer writeDouble(int index, double value) {
        checkLock();
        byteBuffer.putDouble(index, value);
        return this;
    }

    public PoolableBuffer putDouble(double value) {
        writeDouble(writerIndex, value);
        writerIndex += 8;
        return this;
    }

}
