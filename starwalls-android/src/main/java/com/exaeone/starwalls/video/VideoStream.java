package com.exaeone.starwalls.video;

import com.exaeone.starwalls.event.Event;

/**
 * Created by saran on 10/02/15.
 */
public interface VideoStream {

    public static class OnFragmentReady extends Event {
        private VideoFragment fragment = null;

        /*package*/ OnFragmentReady setFragment(VideoFragment fragment) {
            this.fragment = fragment;
            return this;
        }

        public VideoFragment fragment() {
            return this.fragment;
        }

        @Override
        public void onRecycle() {
            this.fragment.release();
            this.fragment = null;
            super.onRecycle();
        }
    }

}
