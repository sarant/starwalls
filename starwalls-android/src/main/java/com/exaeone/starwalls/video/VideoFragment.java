package com.exaeone.starwalls.video;

import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.msg.out.GameOutboundMessage;
import com.exaeone.starwalls.net.BinaryWebSocketFrame;
import com.exaeone.starwalls.pool.DefaultPool;
import com.exaeone.starwalls.util.LargeWebSocketFramePool;

import java.nio.ByteBuffer;

/**
 * Created by saran on 10/02/15.
 */
public class VideoFragment extends GameOutboundMessage {

    private static final byte[] moof = hexStringToByteArray("000000606d6f6f66");    // 8 bytes
    private static final byte[] mfhd = hexStringToByteArray("000000106d66686400000000");    // 12 bytes
    private static final byte[] traf = hexStringToByteArray("0000004874726166");    // 8 bytes
    private static final byte[] tfhd = hexStringToByteArray("0000001c746668640000003800000001"); // 16 bytes
    private static final byte[] tfdt = hexStringToByteArray("000000107466647400000000");    // 12 bytes
    private static final byte[] trun = hexStringToByteArray("000000147472756e000000010000000100000068");
    private static final byte[] mdat = hexStringToByteArray("6d646174");

    private int streamSerialNumber = 0;

    private ByteBuffer inputBuffer = null;
    private int fragmentNumber;
    private int duration;
    private int decodeTime;

    private boolean isHeader = false;
    private BMFFInitializer initializer = null;

    private BinaryWebSocketFrame outputBuffer = null;

    @Override
    protected BinaryWebSocketFrame obtainWebSocketFrame() {
        return LargeWebSocketFramePool.obtain();
    }

    @Override
    public void onRecycle() {
        this.streamSerialNumber = 0;
        this.inputBuffer = null;
        this.initializer = null;
        this.isHeader = false;
        this.outputBuffer = null;
        super.onRecycle();
    }

    public VideoFragment setStreamSerialNumber(int streamSerialNumber) {
        this.streamSerialNumber = streamSerialNumber;
        return this;
    }

    public VideoFragment setBuffer(ByteBuffer inputBuffer) {
        inputBuffer.position(inputBuffer.position() + 4);
        this.inputBuffer = inputBuffer;
        return this;
    }

    public VideoFragment setFragmentNumber(int fragmentNumber) {
        this.fragmentNumber = fragmentNumber;
        return this;
    }

    public VideoFragment setDuration(int duration) {
        this.duration = duration;
        return this;
    }

    public VideoFragment setDecodeTime(int decodeTime) {
        this.decodeTime = decodeTime;
        return this;
    }

    public VideoFragment setAsHeader(BMFFInitializer initializer) {
        isHeader = true;
        this.initializer = initializer;
        return this;
    }

    @Override
    public BinaryWebSocketFrame serialize() {
        if (outputBuffer != null) {
            return outputBuffer;
        }
        else {
            throw new NullPointerException();
        }
    }

    public static class OnSend extends Event {

    }

    /*package*/ VideoFragment writeFragment() {
        outputBuffer = getSerializationBuffer();
        outputBuffer.setOnSendEvent(DefaultPool.obtain(OnSend.class));

        outputBuffer.putInt(streamSerialNumber);

        if (isHeader) {
            initializer.writeToBuffer(outputBuffer);
        }
        else {
            long size = inputBuffer.remaining();

            outputBuffer.putBytes(moof);     // 8 bytes

            outputBuffer.putBytes(mfhd);     // 12 bytes
            outputBuffer.putInt(fragmentNumber);     // 4 bytes

            outputBuffer.putBytes(traf);     // 8 bytes

            outputBuffer.putBytes(tfhd);     // 16 bytes
            outputBuffer.putInt(duration);  // 4 bytes (duration)
            outputBuffer.putInt((int) (size + 4));     // 4 bytes (AVC frame size + 4)
            outputBuffer.putInt(0x02000000);           // 4 bytes (flags)

            outputBuffer.putBytes(tfdt); // 12 bytes
            outputBuffer.putInt(decodeTime);    // 4 bytes (baseMediaDecodeTime)

            outputBuffer.putBytes(trun);

            outputBuffer.putInt((int) (size + 12));
            outputBuffer.putBytes(mdat);
            outputBuffer.putInt((int) size);
            outputBuffer.putBytes(inputBuffer);
        }

        return this;
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

}
