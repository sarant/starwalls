package com.exaeone.starwalls.video;

import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.os.*;
import android.util.Log;
import android.view.Surface;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by saran on 21/04/2014.
 */
public class AVCEncoder {

	private static final String TAG = "GLEncoder";

    private static final String MIME_TYPE = "video/avc";

    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;
    public static final int BITRATE = (int) (1048576 * 2);
    public static final int FPS = 25;
    public static final int SECONDS_BETWEEN_IFRAMES = 20;

	ByteBuffer[] encoderOutputBuffers = null;

	private MediaCodec codec;
	private MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
	private BMFFByteStream byteStream;

	private CodecInputSurface codecSurface;
	private boolean streamReady = false;

    private AtomicBoolean isActive = new AtomicBoolean(false);

    private final MediaFormat format = MediaFormat.createVideoFormat(MIME_TYPE, WIDTH, HEIGHT);

    public AVCEncoder() {
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
        format.setInteger(MediaFormat.KEY_BIT_RATE, BITRATE);
        format.setInteger(MediaFormat.KEY_FRAME_RATE, FPS);
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, SECONDS_BETWEEN_IFRAMES);
    }

	public BMFFByteStream getByteStream() {
		return this.byteStream;
	}

	public boolean isStreamReady() {
		return streamReady;
	}

	public void start(int streamSerialNumber) {

        try {
            codec = MediaCodec.createEncoderByType(MIME_TYPE);
        }
        catch (IOException e) {
            codec = null;
            return;
        }

        if (isActive.compareAndSet(false, true)) {
            try {
                codec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);

                codecSurface = new CodecInputSurface(codec.createInputSurface());

                byteStream = new BMFFByteStream(streamSerialNumber);

                streamReady = false;
                codec.start();

                encoderOutputBuffers = codec.getOutputBuffers();

                codecSurface.makeCurrent();

                /*
                while (!streamReady) {
                    try { Thread.sleep(1); }
                    catch (InterruptedException e) { }
                    drain(false);
                }
                */

                Thread drainThread = new Thread(new DrainTask(), "Video stream");
                drainThread.start();
            }
            catch (RuntimeException e) {
                EventBus.DEFAULT.post(new OnCrash().setThrowable(e), this);
                stop();
            }
        }
	}

    public CodecInputSurface getSurface() {
        return codecSurface;
    }

	public void stop() {
        if (isActive.compareAndSet(true, false)) {
            if (byteStream != null) {
                byteStream.invalidate();
                byteStream = null;
            }

            /*
             * MediaCodec has a tendency to crash (native coredump) randomly, and I don't fully understand the
             * state of the various objects when this happens. I am resorting to being super careful with
             * exception handling here in order to make sure that we tear things down as completely as possible.
             */

            if (codecSurface != null) {
                try {
                    codecSurface.release();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                } finally {
                    codecSurface = null;
                }
            }

            if (codec != null) {
                try {
                    codec.stop();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        codec.release();
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    } finally {
                        codec = null;
                    }
                }
            }

            streamReady = false;
            Log.i(TAG, "Stopped");
        }
	}

	public void drain(boolean endOfStream) {
		final int timeoutMilliseconds = 1;

		if (endOfStream) {
			codec.signalEndOfInputStream();
		}


		while (true) {
			int status;

            status = codec.dequeueOutputBuffer(bufferInfo, timeoutMilliseconds);

			if (status == MediaCodec.INFO_TRY_AGAIN_LATER) {
                break;
            }
            else if (status == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                encoderOutputBuffers = codec.getOutputBuffers();
            }
			else if (status == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
				if (streamReady) {
					Log.e(TAG, "Unexpected behaviour: codec format changed twice");
					stop();
				}
				else {
					byteStream.start(codec.getOutputFormat());
					streamReady = true;
				}
			}
			else if (status < 0) {
				Log.w(TAG, "Unexpected result from dequeueOutputBuffer: " + status);
			}
			else {
                if (!streamReady) {
                    Log.e(TAG, "Unexpected behaviour: codec format never changed");
                    stop();
                }
                else if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    continue;
                }

                ByteBuffer encodedData;

                encodedData = encoderOutputBuffers[status];
                encodedData.position(bufferInfo.offset);
                encodedData.limit(bufferInfo.offset + bufferInfo.size);

                byteStream.appendVideoFrame(encodedData);
                codec.releaseOutputBuffer(status, false);

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (!endOfStream) {
                        Log.w(TAG, "End-of-Stream reached unexpectedly");
                    }
                    break;
                }

			}
		}
	}

    private class DrainTask implements Runnable {

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LESS_FAVORABLE + 3);

            while (isActive.get()) {
                try {
                    drain(false);
                }
                catch (RuntimeException e) {
                    if (isActive.get()) {
                        EventBus.DEFAULT.post(new OnCrash().setThrowable(e), this);
                        stop();
                    }
                }

                if (isActive.get()) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        //
                    }
                }
            }
        }

    }

    public static class OnCrash extends Event {
        private Throwable e = null;

        @Override
        public void onRecycle() {
            e = null;
            super.onRecycle();
        }

        public OnCrash setThrowable(Throwable e) {
            this.e = e;
            return this;
        }

        public Throwable getThrowable() {
            return e;
        }
    }

}
