package com.exaeone.starwalls.video;

import android.media.MediaFormat;
import android.util.Log;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.pool.DefaultPool;

import java.nio.ByteBuffer;

/**
 * Created by saran on 11/05/2014.
 */
public class BMFFByteStream implements VideoStream {

	private static final String TAG = "BMFFByteStream";

    private int streamSerialNumber = 0;
    private int fragmentNumber;
    private int frameDuration;

    private boolean isValid = true;

	public BMFFByteStream(int streamSerialNumber) {
        this.streamSerialNumber = streamSerialNumber;
	}

	public void start(MediaFormat format) {
        frameDuration = (int) (1000.0 / AVCEncoder.FPS);
		fragmentNumber = 1;

        BMFFInitializer initializer = new BMFFInitializer(format);
        VideoFragment header = DefaultPool.obtain(VideoFragment.class)
                .setStreamSerialNumber(streamSerialNumber)
                .setAsHeader(initializer)
                .writeFragment();

        EventBus.DEFAULT.post(DefaultPool.obtain(OnFragmentReady.class).setFragment(header), this);

		Log.i(TAG, "Stream started");
	}

    public void invalidate() {
        isValid = false;
    }

	public void appendVideoFrame(ByteBuffer inputBuf) {
        if (isValid) {
            try {
                VideoFragment fragment = DefaultPool.obtain(VideoFragment.class)
                        .setStreamSerialNumber(streamSerialNumber)
                        .setBuffer(inputBuf)
                        .setDuration(frameDuration)
                        .setFragmentNumber(fragmentNumber)
                        .setDecodeTime(frameDuration * (fragmentNumber - 1))
                        .writeFragment();

                EventBus.DEFAULT.post(DefaultPool.obtain(OnFragmentReady.class).setFragment(fragment), this);

                fragmentNumber++;
            } catch (RuntimeException e) {
                // This can happen when the encoder is stopped while we're writing
                isValid = false;
            }
        }
	}



}
