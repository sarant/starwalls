package com.exaeone.starwalls.video;

import com.exaeone.starwalls.util.PoolableBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

/**
 * Created by saran on 11/05/2014.
 */
public class PoolableBufferChannel implements WritableByteChannel {

	private PoolableBuffer buf;

	public void open(PoolableBuffer buf) {
		this.buf = buf;
	}

	@Override
	public int write(ByteBuffer buffer) throws IOException {
		int originalIndex = buf.writerIndex();
		buf.putBytes(buffer);
		return buf.writerIndex() - originalIndex;
	}

	@Override
	public boolean isOpen() {
		return buf != null;
	}

	@Override
	public void close() throws IOException {
		this.buf = null;
	}
}
