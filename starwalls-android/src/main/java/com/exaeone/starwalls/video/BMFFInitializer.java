package com.exaeone.starwalls.video;

import android.media.MediaFormat;
import com.coremedia.iso.boxes.*;
import com.coremedia.iso.boxes.fragment.*;
import com.coremedia.iso.boxes.sampleentry.VisualSampleEntry;
import com.exaeone.starwalls.util.PoolableBuffer;
import com.googlecode.mp4parser.BasicContainer;
import com.googlecode.mp4parser.h264.model.SeqParameterSet;
import com.mp4parser.iso14496.part15.AvcConfigurationBox;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by aeon on 10/05/2014.
 */
public class BMFFInitializer {

    private int width;
    private int height;

    private Date now = new Date();
    private MediaFormat mediaFormat;
    public BasicContainer isoFile;

    public BMFFInitializer(MediaFormat mediaFormat) {
        this.mediaFormat = mediaFormat;
        width = AVCEncoder.WIDTH;
        height = AVCEncoder.HEIGHT;

        isoFile = new BasicContainer();
        isoFile.addBox(createFtyp());
        isoFile.addBox(createMoov());
    }

	public PoolableBuffer writeToBuffer(PoolableBuffer buf) {
		try {
			PoolableBufferChannel ch = new PoolableBufferChannel();
			ch.open(buf);
			isoFile.writeContainer(ch);
			ch.close();
			return buf;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

    private FileTypeBox createFtyp() {
        List<String> minorBrands = new LinkedList<String>();
        minorBrands.add("isom");
	    minorBrands.add("dash");
        FileTypeBox ftyp = new FileTypeBox("isom", 0, minorBrands);
        return ftyp;
    }

    private MovieBox createMoov() {
        MovieBox movieBox = new MovieBox();
        movieBox.addBox(createMvhd());
	    movieBox.addBox(createMvex());
        movieBox.addBox(createTrak());
        return movieBox;
    }

    private MovieHeaderBox createMvhd() {
        MovieHeaderBox movieHeaderBox = new MovieHeaderBox();
        movieHeaderBox.setVersion(0);
        movieHeaderBox.setCreationTime(now);
        movieHeaderBox.setModificationTime(now);
        movieHeaderBox.setTimescale(1000);
        movieHeaderBox.setDuration(0);
        movieHeaderBox.setNextTrackId(2);
        return movieHeaderBox;
    }

	private MovieExtendsBox createMvex() {
		MovieExtendsBox movieExtendsBox = new MovieExtendsBox();
		movieExtendsBox.addBox(createTrex());

		return movieExtendsBox;
	}

	private TrackExtendsBox createTrex() {
		TrackExtendsBox trackExtendsBox = new TrackExtendsBox();
		trackExtendsBox.setTrackId(1);
		trackExtendsBox.setDefaultSampleDescriptionIndex(1);
		trackExtendsBox.setDefaultSampleDuration(0);
		trackExtendsBox.setDefaultSampleSize(0);

		// TODO (if broken): we may need to check the flags

		SampleFlags sf = new SampleFlags();
		/*
		sf.setSampleDependsOn(2);
		sf.setSampleIsDependedOn(2);
		sf.setSampleIsDifferenceSample(false);
		*/
		trackExtendsBox.setDefaultSampleFlags(sf);


		return trackExtendsBox;
	}

    private TrackBox createTrak() {
        TrackBox trackBox = new TrackBox();
        trackBox.addBox(createTkhd());
        trackBox.addBox(createMdia());
        return trackBox;
    }

    private TrackHeaderBox createTkhd() {
        TrackHeaderBox trackHeaderBox = new TrackHeaderBox();
        trackHeaderBox.setVersion(0);
        trackHeaderBox.setCreationTime(now);
        trackHeaderBox.setModificationTime(now);
        trackHeaderBox.setTrackId(1);
        trackHeaderBox.setDuration(0);
        trackHeaderBox.setWidth(width);
        trackHeaderBox.setHeight(height);
	    trackHeaderBox.setFlags(7);
        return trackHeaderBox;
    }

    private MediaBox createMdia() {
        MediaBox mediaBox = new MediaBox();
        mediaBox.addBox(createMdhd());
        mediaBox.addBox(createHdlr());
        mediaBox.addBox(createMinf());
        return mediaBox;
    }

    private MediaHeaderBox createMdhd() {
        MediaHeaderBox mediaHeaderBox = new MediaHeaderBox();
        mediaHeaderBox.setVersion(0);
        mediaHeaderBox.setCreationTime(now);
        mediaHeaderBox.setModificationTime(now);
        mediaHeaderBox.setTimescale(1000);
        mediaHeaderBox.setDuration(0);
        mediaHeaderBox.setLanguage("und");
        return mediaHeaderBox;
    }

    private HandlerBox createHdlr() {
        HandlerBox handlerBox = new HandlerBox();
        handlerBox.setHandlerType("vide");
        return handlerBox;
    }

    private MediaInformationBox createMinf() {
        MediaInformationBox mediaInformationBox = new MediaInformationBox();
        mediaInformationBox.addBox(createVmhd());
        mediaInformationBox.addBox(createDinf());
        mediaInformationBox.addBox(createStbl());
        return mediaInformationBox;
    }

    private VideoMediaHeaderBox createVmhd() {
        VideoMediaHeaderBox videoMediaHeaderBox = new VideoMediaHeaderBox();
        return videoMediaHeaderBox;
    }

    private DataInformationBox createDinf() {
        DataEntryUrlBox url = new DataEntryUrlBox();
        url.setFlags(1);

        DataReferenceBox dref = new DataReferenceBox();
        dref.addBox(url);

        DataInformationBox dinf = new DataInformationBox();
        dinf.addBox(dref);

        return dinf;
    }

    private SampleTableBox createStbl() {
        SampleTableBox sampleTableBox = new SampleTableBox();
        sampleTableBox.addBox(createStsd());
        sampleTableBox.addBox(new TimeToSampleBox()); // STTS
        sampleTableBox.addBox(new SampleToChunkBox()); // STSC
        sampleTableBox.addBox(new SampleSizeBox()); // STSZ
        sampleTableBox.addBox(new StaticChunkOffsetBox()); // STCO
        return sampleTableBox;
    }

    private SampleDescriptionBox createStsd() {
        SampleDescriptionBox sampleDescriptionBox;

        sampleDescriptionBox = new SampleDescriptionBox();
        VisualSampleEntry visualSampleEntry = new VisualSampleEntry("avc1");
        visualSampleEntry.setDataReferenceIndex(1);
        visualSampleEntry.setDepth(24);
        visualSampleEntry.setFrameCount(1);
        visualSampleEntry.setHorizresolution(72);
        visualSampleEntry.setVertresolution(72);
        visualSampleEntry.setWidth(width);
        visualSampleEntry.setHeight(height);
        visualSampleEntry.setCompressorname("");

        AvcConfigurationBox avcConfigurationBox = new AvcConfigurationBox();

        final ByteBuffer spsBuffer = mediaFormat.getByteBuffer("csd-0");
        List<byte[]> spss = getXParameterSets(spsBuffer);

        final ByteBuffer ppsBuffer = mediaFormat.getByteBuffer("csd-1");
        List<byte[]> ppss = getXParameterSets(ppsBuffer);

        InputStream is = new ByteBufferBackedInputStream(spsBuffer);
        SeqParameterSet seqParameterSet;
        try {
            is.read();
            seqParameterSet = SeqParameterSet.read(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

	    byte profileCompatibility = (spss.get(0))[2];

        avcConfigurationBox.setConfigurationVersion(1);
        avcConfigurationBox.setAvcProfileIndication(seqParameterSet.profile_idc);
        avcConfigurationBox.setAvcLevelIndication(seqParameterSet.level_idc);
        avcConfigurationBox.setLengthSizeMinusOne(3);
	    avcConfigurationBox.setProfileCompatibility(profileCompatibility);
        // TODO: File bug report in AvcConfigurationBox:269 getContent()
        // Sets pictureParameterSets.size() instead of sequenceParameterSets.size()
        avcConfigurationBox.setSequenceParameterSets(spss);
        avcConfigurationBox.setPictureParameterSets(ppss);

        avcConfigurationBox.setBitDepthLumaMinus8(seqParameterSet.bit_depth_luma_minus8);
        avcConfigurationBox.setBitDepthChromaMinus8(seqParameterSet.bit_depth_chroma_minus8);
        avcConfigurationBox.setChromaFormat(seqParameterSet.chroma_format_idc.getId());

        visualSampleEntry.addBox(avcConfigurationBox);
        sampleDescriptionBox.addBox(visualSampleEntry);

        return sampleDescriptionBox;
    }

    private List<byte[]> getXParameterSets(ByteBuffer source) {
        source.position(source.position() + 4);
        final byte[] sps = new byte[source.remaining()];
        source.mark();
        source.get(sps);
        source.reset();
        LinkedList<byte[]> spss = new LinkedList<>();
        spss.add(sps);
        return spss;
    }


    public class ByteBufferBackedInputStream extends InputStream {

        private final ByteBuffer buf;

        public ByteBufferBackedInputStream(ByteBuffer buf) {
            // make a coy of the buffer
            this.buf = buf.duplicate();
        }

        public int read() throws IOException {
            if (!buf.hasRemaining()) {
                return -1;
            }
            return buf.get() & 0xFF;
        }

        public int read(byte[] bytes, int off, int len)
                throws IOException {
            if (!buf.hasRemaining()) {
                return -1;
            }

            len = Math.min(len, buf.remaining());
            buf.get(bytes, off, len);
            return len;
        }
    }
}
