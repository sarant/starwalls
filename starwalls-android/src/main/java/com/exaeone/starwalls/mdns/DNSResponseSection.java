package com.exaeone.starwalls.mdns;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by saran on 17/03/15.
 */
public class DNSResponseSection implements Iterable<DNSRecord> {

    protected final List<DNSRecord> records = new ArrayList<>();

    /*package*/ void add(DNSRecord record) {
        records.add(record);
    }

    public int size() {
        return records.size();
    }

    public DNSRecord get(int index) {
        return records.get(index);
    }

    public DNSRecord find(short recordType) {
        for (DNSRecord record : records) {
            if (record.getRecordType() == recordType) {
                return record;
            }
        }
        return null;
    }

    public DNSRecord find(Class<? extends DNSRecord> recordType) {
        for (DNSRecord record : records) {
            if (record.getClass().equals(recordType)) {
                return record;
            }
        }
        return null;
    }

    @Override
    public Iterator<DNSRecord> iterator() {
        return records.iterator();
    }
}
