package com.exaeone.starwalls.mdns;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * Created by saran on 17/03/15.
 */
public class ARecord extends DNSRecord {

    protected Inet4Address addr;

    public Inet4Address getAddr() {
        return addr;
    }

    @Override
    protected void readFromBuffer(ByteBuffer buf) {
        byte[] rawAddr = new byte[4];
        buf.get(rawAddr);
        try {
            this.addr = (Inet4Address) Inet4Address.getByAddress(rawAddr);
        }
         catch (UnknownHostException e) {
             // This should never happen since we specifically pass in four bytes
            throw new RuntimeException(e);
        }
    }

}
