package com.exaeone.starwalls.mdns;

import com.exaeone.starwalls.util.Unsigned;

import java.nio.ByteBuffer;

/**
 * Abstract base class for a DNS record
 *
 * Created by saran on 17/03/15.
 */
public abstract class DNSRecord {

    protected String recordName;
    protected short recordType;
    protected short recordClass;
    protected boolean flushCache;
    protected long ttl;

    protected DNSRecord() {
    }

    /**
     *  DNS Resource Record Name
     */
    public String getRecordName() {
        return recordName;
    }

    /**
     *  DNS Resource Record Class
     *  Virtually always set to 0x0001 for 'INternet'
     */
    public short getRecordClass() {
        return recordClass;
    }

    /**
     *  DNS Resource Record Type
     *  Implemented type constants can be found in the DNSRecordType class.
     */
    public short getRecordType() {
        return recordType;
    }

    /**
     *  Instructs the resolver whether flush its cache.
     *  We don't care about this here.
     */
    public boolean flushCache() {
        return flushCache;
    }

    /**
     *  Time-to-Live for this record
     *  We don't care about this here.
     */
    public long getTTL() {
        return ttl;
    }

    protected abstract void readFromBuffer(ByteBuffer buf);

    /*package*/ static DNSRecord decode(ByteBuffer buf) {
        String recordName = decodeRecordName(buf);
        short recordType = buf.getShort();

        int cacheAndClass = Unsigned.ushort(buf.getShort());
        short recordClass = (short) (cacheAndClass & 0x7FFF);
        boolean cacheFlush = (cacheAndClass & 0x8000) != 0;

        long ttl = Unsigned.uint(buf.getInt());

        int payloadLength = Unsigned.ushort(buf.getShort());
        if (payloadLength > buf.remaining()) {
            return null;
        }

        ByteBuffer payloadBuffer = copyBuffer(buf, payloadLength);

        DNSRecord record;
        switch (recordType) {
            case DNSRecordType.A:
                record = new ARecord();
                break;
            case DNSRecordType.AAAA:
                record = new AAAARecord();
                break;
            case DNSRecordType.PTR:
                record = new PTRRecord();
                break;
            case DNSRecordType.TXT:
                record = new TXTRecord();
                break;
            case DNSRecordType.SRV:
                record = new SRVRecord();
                break;
            default:
                record = new GenericDNSRecord();
                break;
        }

        record.recordName = recordName;
        record.recordType = recordType;
        record.recordClass = recordClass;
        record.flushCache = cacheFlush;
        record.ttl = ttl;
        record.readFromBuffer(payloadBuffer);

        return record;
    }

    private static ByteBuffer copyBuffer(ByteBuffer src, int numBytes) {
        ByteBuffer dst = ByteBuffer.allocate(numBytes);
        int oldLimit = src.limit();
        src.limit(src.position() + dst.remaining());
        dst.put(src).flip();
        src.limit(oldLimit);
        return dst;
    }

    protected static String decodeRecordName(ByteBuffer buf) {
        short length = Unsigned.ubyte(buf.get());
        StringBuilder name = new StringBuilder(255);

        while (length != 0) {
            for (int i = 0; i < length; ++i) {
                name.append((char) buf.get());
            }

            length = Unsigned.ubyte(buf.get());
            if (length != 0) {
                name.append('.');
            }
        }

        return name.toString();
    }

}
