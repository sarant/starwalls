package com.exaeone.starwalls.mdns;

import java.net.Inet6Address;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * Created by saran on 17/03/15.
 */
public class AAAARecord extends DNSRecord {

    protected Inet6Address addr;

    public Inet6Address getAddr() {
        return addr;
    }

    @Override
    protected void readFromBuffer(ByteBuffer buf) {
        byte[] rawAddr = new byte[16];
        buf.get(rawAddr);
        try {
            this.addr = (Inet6Address) Inet6Address.getByAddress(rawAddr);
        }
        catch (UnknownHostException e) {
            // This should never happen since we made the sixteen-byte array ourselves.
            throw new RuntimeException(e);
        }
    }

}
