package com.exaeone.starwalls.mdns;

import java.nio.ByteBuffer;

/**
 * Created by saran on 17/03/15.
 */
public class PTRRecord extends DNSRecord {

    protected String domainName;

    public String getDomainName() {
        return domainName;
    }

    @Override
    protected void readFromBuffer(ByteBuffer buf) {
        domainName = DNSRecord.decodeRecordName(buf);
    }
}
