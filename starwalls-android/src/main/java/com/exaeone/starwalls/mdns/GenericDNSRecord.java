package com.exaeone.starwalls.mdns;

import java.nio.ByteBuffer;

/**
 * Unimplemented DNS record types are instantiated as a GenericDNSRecord.
 * We don't just store the payload data in a ByteBuffer and don't parse it at all.
 *
 * Created by saran on 17/03/15.
 */
public class GenericDNSRecord extends DNSRecord {

    protected ByteBuffer payload;

    public ByteBuffer getPayload() {
        return payload;
    }

    @Override
    protected void readFromBuffer(ByteBuffer buf) {
        payload = buf;
    }

}
