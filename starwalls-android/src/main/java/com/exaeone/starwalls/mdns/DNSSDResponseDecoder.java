package com.exaeone.starwalls.mdns;

import com.exaeone.starwalls.util.Unsigned;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/**
 * Created by saran on 17/03/15.
 */
public class DNSSDResponseDecoder {

    public static DNSResponse decode(ByteBuffer buf) {
        try {
            // RFC: In multicast responses, including unsolicited multicast responses,
            // the Query Identifier MUST be set to zero on transmission, and MUST be ignored on reception.
            int id = Unsigned.ushort(buf.getShort());

            short flagHi = Unsigned.ubyte(buf.get());

            // RFC: In response messages the QR bit MUST be one.
            boolean isResponse = (flagHi & 0x80) != 0;

            // RFC: Multicast DNS messages received with OPCODE other than zero MUST be silently ignored.
            boolean isOpcodeZero = (flagHi & 0x78) == 0;

            // RFC: (paraphrased) Ignore the next 7 bits
            short flagLo = Unsigned.ubyte(buf.get());

            // RFC: Multicast DNS messages received with non-zero Response Codes MUST be silently ignored.
            boolean isRcodeZero = (flagLo & 0x0F) == 0;

            if (!(isResponse && isOpcodeZero && isRcodeZero)) {
                return null;
            }

            int nQuestion = Unsigned.ushort(buf.getShort());
            int nAnswer = Unsigned.ushort(buf.getShort());
            int nAuthority = Unsigned.ushort(buf.getShort());
            int nAdditional = Unsigned.ushort(buf.getShort());

            // RFC: Multicast DNS responses MUST NOT contain any questions in the Question Section.
            // RFC: Any questions in the Question Section of a received Multicast DNS response MUST be silently ignored.
            // TODO: Actually READ, and then ignore questions. For now we bail if there are questions.
            if (nQuestion != 0) {
                return null;
            }

            DNSResponse response = new DNSResponse();

            for (int i = 0; i < nAnswer; ++i) {
                response.answerSection().add(DNSRecord.decode(buf));
            }

            for (int i = 0; i < nAuthority; ++i) {
                response.authoritySection().add(DNSRecord.decode(buf));
            }

            for (int i = 0; i < nAdditional; ++i) {
                response.additionalSection().add(DNSRecord.decode(buf));
            }

            return response;
        }
        catch (BufferUnderflowException e) {
            // Malformed DNS query, ignore it.
            return null;
        }
    }

}
