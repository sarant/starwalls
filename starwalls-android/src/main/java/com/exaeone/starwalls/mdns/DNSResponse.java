package com.exaeone.starwalls.mdns;


/**
 * Created by saran on 17/03/15.
 */
public class DNSResponse {

    protected final DNSResponseSection answerSection = new DNSResponseSection();
    protected final DNSResponseSection authoritySection = new DNSResponseSection();
    protected final DNSResponseSection additionalSection = new DNSResponseSection();

    public DNSResponseSection answerSection() {
        return answerSection;
    }

    public DNSResponseSection authoritySection() {
        return authoritySection;
    }

    public DNSResponseSection additionalSection() {
        return additionalSection;
    }

}
