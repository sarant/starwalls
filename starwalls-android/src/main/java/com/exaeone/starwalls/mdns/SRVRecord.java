package com.exaeone.starwalls.mdns;

import java.nio.ByteBuffer;

/**
 * Created by saran on 17/03/15.
 */
public class SRVRecord extends DNSRecord {

    protected short priority;
    protected short weight;
    protected short port;
    protected String target;

    public short getPriority() {
        return priority;
    }

    public short getWeight() {
        return weight;
    }

    public short getPort() {
        return port;
    }

    public String getTarget() {
        return target;
    }

    @Override
    protected void readFromBuffer(ByteBuffer buf) {
        priority = buf.getShort();
        weight = buf.getShort();
        port = buf.getShort();
        target = DNSRecord.decodeRecordName(buf);
    }

}
