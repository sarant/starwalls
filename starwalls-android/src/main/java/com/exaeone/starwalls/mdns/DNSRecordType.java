package com.exaeone.starwalls.mdns;

/**
 * Created by saran on 17/03/15.
 */
public abstract class DNSRecordType {

    public static final short A = 0x01;
    public static final short AAAA = 0x28;
    public static final short PTR = 0x0C;
    public static final short TXT = 0x10;
    public static final short SRV = 0x21;

}
