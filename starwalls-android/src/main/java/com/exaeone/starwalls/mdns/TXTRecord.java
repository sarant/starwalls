package com.exaeone.starwalls.mdns;

import com.exaeone.starwalls.util.Unsigned;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * DNS-SD TXT Record, as defined in RFC6763
 * https://tools.ietf.org/html/rfc6763#page-11
 *
 * Created by saran on 17/03/15.
 */
public class TXTRecord extends DNSRecord {

    protected final Map<String, String> dict = new HashMap<>();

    @Override
    protected void readFromBuffer(ByteBuffer buf) {
        while (buf.remaining() > 0) {
            short length = Unsigned.ubyte(buf.get());

            StringBuilder key = new StringBuilder(255);
            StringBuilder value = new StringBuilder(255);

            boolean keyEnded = false;

            for (int i = 0; i < length; ++i) {
                char c = (char) buf.get();

                if (c == '=' && !keyEnded) {
                    keyEnded = true;
                }
                else if (!keyEnded) {
                    key.append(c);
                }
                else {
                    value.append(c);
                }
            }

            if (key.length() > 0) {
                dict.put(key.toString(), value.toString());
            }
        }
    }

    // We expose only the accessor functions from the Map interface

    public int size() {
        return dict.size();
    }

    public boolean isEmpty() {
        return dict.isEmpty();
    }

    public boolean containsKey(Object key) {
        return dict.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return dict.containsValue(value);
    }

    public String get(Object key) {
        return dict.get(key);
    }

    public Set<String> keySet() {
        return dict.keySet();
    }

    public Collection<String> values() {
        return dict.values();
    }

    public Set<Map.Entry<String, String>> entrySet() {
        return dict.entrySet();
    }

}
