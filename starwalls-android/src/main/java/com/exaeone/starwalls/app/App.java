package com.exaeone.starwalls.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import com.exaeone.starwalls.billing.DonationManager;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.GameNetworkGateway;
import com.exaeone.starwalls.net.WebSocketServer;

/**
 * Created by saran on 09/02/15.
 */
public class App extends Application {

    private static Context context;
    private static AppDataManagers dataManagers;
    private static DonationManager donationManager;

    private static WebSocketServer server = new WebSocketServer();
    public static final int port = 54321;

    // DO NOT REMOVE!
    // This object subscribes to incoming websocket connection and forms aggregate connections
    public static GameNetworkGateway gameNetworkGateway;

    public static volatile Activity transitioningFrom = null;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        dataManagers = new AppDataManagers(context);
        gameNetworkGateway = new GameNetworkGateway(context);
        donationManager = new DonationManager(context);
    }

    public static Context getContext() {
        return context;
    }

    public static AppDataManagers getDataManagers() {
        return dataManagers;
    }

    public static DonationManager getDonationManager() {
        return donationManager;
    }

    public static void transitionBegin(Activity from) {
        transitioningFrom = from;
    }

    public static boolean globalStart(Activity activity) {
        if (transitioningFrom == null) {
            EventBus.DEFAULT.start();
            server.start(port, GameNetworkGateway.WEBSOCKET_SUBPROTOCOL_NAME);
            return true;
        }
        else {
            if (transitioningFrom != activity) {
                transitioningFrom = null;
            }
            return false;
        }
    }

    public static void globalStop(Activity activity) {
        if (transitioningFrom == null) {
            server.stop();
            EventBus.DEFAULT.stop();
        }
    }

}
