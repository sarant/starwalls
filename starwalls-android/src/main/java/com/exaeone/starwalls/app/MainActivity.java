package com.exaeone.starwalls.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.*;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.cast.CastAdapter;
import com.exaeone.starwalls.cast.CastConnector;
import com.exaeone.starwalls.cast.Chromecast;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.AggregateConnection;
import com.exaeone.starwalls.game.GameNetworkGateway;
import com.exaeone.starwalls.game.HighScoreManager;

public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private CastConnector connector = new CastConnector();

    private TextView wifiView = null;
    private ListView castListView = null;
    private CastAdapter castAdapter = new CastAdapter(App.getContext());

    private TextView highScoreView;
    private TextView bestStreakView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        Bitmap topBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.starwalls_top, options);
        ((ImageView) findViewById(R.id.imageview_top)).setImageBitmap(topBitmap);

        Bitmap bottomBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.starwalls_bottom, options);
        ((ImageView) findViewById(R.id.imageview_bottom)).setImageBitmap(bottomBitmap);

        highScoreView = ((TextView) findViewById(R.id.textview_high_score));
        bestStreakView = ((TextView) findViewById(R.id.textview_best_streak));

        wifiView = (TextView) findViewById(R.id.wifi_text);

        castListView = (ListView)findViewById(R.id.listview_cast);
        castListView.setAdapter(castAdapter);
        castListView.setOnItemClickListener(this);

        EventBus.DEFAULT.subscribe(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.globalStart(this);
        clearCastList();
        setStreakAndScore();
    }

    @Override
    protected void onPause() {
        connector.stopScanning();
        App.globalStop(this);
        super.onPause();
    }

    private void clearCastList() {
        castAdapter.clear();
        wifiView.setVisibility(View.VISIBLE);
        connector.startScanning();
    }

    private void setStreakAndScore() {
        final Resources res = getResources();
        final HighScoreManager highScoreManager = App.getDataManagers().getHighScoreManager();

        int highScore = highScoreManager.getHighScore();
        int bestStreak = highScoreManager.getLongestStreak();

        highScoreView.setText(res.getString(R.string.high_score_label, highScore));
        bestStreakView.setText(res.getString(R.string.best_streak_label, bestStreak));
    }

    public void onEvent(CastConnector.OnDiscover evt) {
        final Chromecast chromecast = evt.getChromecast();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wifiView.setVisibility(View.INVISIBLE);
                castAdapter.add(chromecast);
            }
        });
    }

    public void onEvent(CastConnector.OnCastConnected evt) {
        connector.stopScanning();
        connector.sendMessage("ws://" + connector.getLocalAddress().getHostAddress() + ":" + App.port + "/");
    }

    public void onEvent(AggregateConnection.OnOpen evt) {
        App.transitionBegin(this);
        Intent intent = new Intent(App.getContext(), GameActivity.class);
        startActivityForResult(intent, 0);
    }

    public void onEvent(GameNetworkGateway.OnVersionTooLow evt) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.update_title)
                        .setMessage(R.string.update_text)
                        .setPositiveButton(R.string.update_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(getResources().getString(R.string.app_url)));
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == castListView) {
            connector.stopScanning();
            Chromecast chromecast = (Chromecast) castListView.getAdapter().getItem(position);
            castAdapter.setConnecting(position);
            connector.connectToCastDevice(chromecast.getCastDevice());
        }
    }

    public void onEvent(CastConnector.OnGMSUpdateRequired evt) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.gms_update_title)
                        .setMessage(R.string.gms_update_text)
                        .setPositiveButton(R.string.gms_update_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setData(Uri.parse(getResources().getString(R.string.gms_url)));
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        });
    }

    public void onEvent(CastConnector.OnConnectionFailure evt) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.gms_failure_title)
                        .setMessage(R.string.gms_failure_text)
                        .setPositiveButton(R.string.gms_failure_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                clearCastList();
                            }
                        })
                        .show();
            }
        });
    }

    public void onAchievementsClick(View view) {
        Intent intent = new Intent(App.getContext(), AchievementsActivity.class);
        App.transitionBegin(this);
        startActivity(intent);
    }

    public void onDonationClick(View view) {
        Intent intent = new Intent(App.getContext(), CoffeeActivity.class);
        App.transitionBegin(this);
        startActivity(intent);
    }

}
