package com.exaeone.starwalls.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.billing.*;
import com.exaeone.starwalls.event.EventBus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kathryn on 03/04/15.
 */
public class CoffeeActivity extends ActionBarActivity {

    public static final String TAG = "CoffeeActivity";

    private boolean inPurchase = false;

    private enum UIState {
        NotReady,
        Soliciting,
        Thanks
    }

    private final Map<UIState, View> uiStateViewMap = new HashMap<>();

    // TODO: Use a BiMap here
    private final Map<Button, DonationManager.DonationSize> buttonToDonationSize = new HashMap<>();
    private final Map<DonationManager.DonationSize, Button> donationSizeToButton = new HashMap<>();
    private void bindButtonToDonationSize(DonationManager.DonationSize donationSize, Button button) {
        buttonToDonationSize.put(button, donationSize);
        donationSizeToButton.put(donationSize, button);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coffee);

        uiStateViewMap.put(UIState.NotReady, findViewById(R.id.donation_cannot_connect));
        uiStateViewMap.put(UIState.Soliciting, findViewById(R.id.donation_buttons));
        uiStateViewMap.put(UIState.Thanks, findViewById(R.id.donation_thanks));

        bindButtonToDonationSize(DonationManager.DonationSize.Small, (Button) findViewById(R.id.button_payment_small));
        bindButtonToDonationSize(DonationManager.DonationSize.Medium, (Button) findViewById(R.id.button_payment_medium));
        bindButtonToDonationSize(DonationManager.DonationSize.Large, (Button) findViewById(R.id.button_payment_large));
        bindButtonToDonationSize(DonationManager.DonationSize.XLarge, (Button) findViewById(R.id.button_payment_xlarge));

        EventBus.DEFAULT.subscribe(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!inPurchase) {
            App.globalStart(this);

            if (App.getDonationManager().isReady()) {
                updateUI(UIState.Soliciting);
                updatePrices();
            } else {
                updateUI(UIState.NotReady);
            }
        }
        else {
            inPurchase = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //If this result has nothing to do with billing, pass it to the superclass to be handled
        if (!App.getDonationManager().handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onEvent(DonationManager.OnReady evt) {
        updateUI(UIState.Soliciting);
        updatePrices();
    }

    public void onEvent(DonationManager.OnPurchaseFinished evt) {
        if (evt.isSuccess()) {
            updateUI(UIState.Thanks);
        }
    }

    private void updateUI(final UIState toState) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (UIState state : UIState.values()) {
                    uiStateViewMap.get(state).setVisibility(View.GONE);
                }
                uiStateViewMap.get(toState).setVisibility(View.VISIBLE);
            }
        });
    }

    public void onPaymentClick(View view) {
        Button button = (Button) view;
        DonationManager donationManager = App.getDonationManager();
        DonationManager.DonationSize donationSize = buttonToDonationSize.get(button);
        if (donationSize != null && donationManager.isReady()) {
            inPurchase = true;
            donationManager.donate(donationSize, this);
        }
    }

    public void onWeblinkClick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getResources().getString(R.string.website_url)));
        startActivity(intent);
    }

    private void updatePrices() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Resources res = getResources();
                DonationManager donationManager = App.getDonationManager();

                for (DonationManager.DonationSize donationSize : DonationManager.DonationSize.values()) {
                    Button button = donationSizeToButton.get(donationSize);

                    if (button != null) {
                        String priceString = donationManager.getDonationPriceForSize(donationSize);

                        if (priceString != null) {
                            button.setText(res.getString(donationSize.buttonTextResID, priceString));
                            button.setEnabled(true);
                        } else {
                            button.setText(R.string.donation_default);
                            button.setEnabled(false);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onPause() {
        if (!inPurchase) {
            App.globalStop(this);
        }

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        App.transitionBegin(this);
        super.onBackPressed();
    }


}
