package com.exaeone.starwalls.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.game.AggregateConnection;
import com.exaeone.starwalls.game.GameExecutor;
import com.exaeone.starwalls.game.Spaceship;

import java.util.concurrent.atomic.AtomicBoolean;

public class GameActivity extends ActionBarActivity implements DialogInterface.OnDismissListener {

    private final GameExecutor executor = new GameExecutor();

    View shipPagerDisabler;
    ViewPager shipSelectionPager;
    FragmentPagerAdapter shipPagerAdapter;
    PageChangeListener shipSelectionListener;

    public Class<? extends Spaceship> selectedShip = Spaceship.SHIP_LIST.get(0);

    private TextView statusText;
    private Button startButton;
    private AlertDialog dialog = null;

    private final AtomicBoolean isTornDown = new AtomicBoolean(false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        shipPagerAdapter = new MyShipPagerAdapter(getSupportFragmentManager());
        shipSelectionListener = new PageChangeListener();

        shipPagerDisabler = findViewById(R.id.ship_pager_disabler);
        shipSelectionPager = (ViewPager) findViewById(R.id.shipSelectionPager);
        shipSelectionPager.setAdapter(shipPagerAdapter);
        shipSelectionPager.setOnPageChangeListener(shipSelectionListener);

        statusText = (TextView) findViewById(R.id.text_status);
        startButton = (Button) findViewById(R.id.button_start_game);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isTornDown.set(false);

        if (!App.globalStart(this)) {
            dialog = null;
            AggregateConnection connection = App.gameNetworkGateway.getAggregator();

            if (connection != null) {
                EventBus.DEFAULT.subscribe(this);
                executor.setup(connection);
                disableStartButtonWithMessage(R.string.preparing_game);
                shipSelectorEnabled(true);
            }
            else {
                App.transitionBegin(this);
                super.onBackPressed();
            }
        }
        else {
            teardownAndGoBack();
        }
    }

    @Override
    protected void onPause() {
        teardown();
        App.globalStop(this);
        super.onPause();
    }

    private boolean teardown() {
        if (isTornDown.compareAndSet(false, true)) {
            EventBus.DEFAULT.unsubscribe(this);

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }

            executor.teardown();

            return true;
        }
        else {
            return false;
        }
    }

    private void teardownAndGoBack() {
        if (teardown()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    App.transitionBegin(GameActivity.this);
                    GameActivity.super.onBackPressed();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (dialog == null) {
            dialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.disconnect_title)
                    .setMessage(R.string.disconnect_text)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            teardownAndGoBack();
                        }
                    })
                    .setNegativeButton("No", null)
                    .setOnDismissListener(this)
                    .create();
            dialog.show();
        }
        else {
            dialog.dismiss();
            teardownAndGoBack();
        }
    }

    public void onEvent(GameExecutor.OnReadyToStart evt) {
        enableStartButton();
    }

    public void onEvent(GameExecutor.OnStart evt) {
        disableStartButtonWithMessage(R.string.in_game);
    }

    public void onEvent(GameExecutor.OnStop evt) {
        shipSelectorEnabled(true);
        disableStartButtonWithMessage(R.string.preparing_game);
    }

    public void onEvent(final GameExecutor.OnCrash evt) {
        final boolean givingUp = evt.isGivingUp();

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (dialog != null) {
                    dialog.dismiss();
                }

                if (!givingUp) {
                    dialog = new AlertDialog.Builder(GameActivity.this)
                            .setTitle(R.string.crash_title)
                            .setMessage(R.string.crash_text)
                            .setPositiveButton(R.string.crash_button, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    shipSelectorEnabled(true);
                                    executor.setupForNewGame();
                                }
                            })
                            .setOnDismissListener(GameActivity.this)
                            .create();
                    dialog.show();
                }
                else {
                    dialog = new AlertDialog.Builder(GameActivity.this)
                            .setTitle(R.string.giving_up_title)
                            .setMessage(R.string.giving_up_text)
                            .setPositiveButton(R.string.giving_up_button, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    teardownAndGoBack();
                                }
                            })
                            .setOnDismissListener(GameActivity.this)
                            .create();
                    dialog.show();
                }
            }
        });
    }

    public void onClick(View view) {
        if (view == startButton) {
            shipSelectorEnabled(false);
            disableStartButtonWithMessage(R.string.starting_game);
            executor.startGame(selectedShip);
        }
    }

    private void enableStartButton() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                statusText.setVisibility(View.INVISIBLE);
                startButton.setVisibility(View.VISIBLE);
                startButton.setEnabled(true);
            }
        });
    }

    private void disableStartButtonWithMessage(final int resID) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                // This check MUST happen on the main thread to prevent race condition
                if (isTornDown.get()) {
                    return;
                }

                statusText.setVisibility(View.VISIBLE);
                startButton.setVisibility(View.INVISIBLE);
                startButton.setEnabled(false);
                statusText.setText(resID);
            }
        });
    }

    private void shipSelectorEnabled(final boolean enabled) {
        // Because the alternative is horrendous...
        // http://stackoverflow.com/questions/7814017

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                // This check MUST happen on the main thread to prevent race condition
                if (isTornDown.get()) {
                    return;
                }

                if (enabled) {
                    shipPagerDisabler.setVisibility(View.GONE);
                }
                else {
                    shipPagerDisabler.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void onEvent(GameExecutor.OnRemoteDisconnected evt) {
        teardownAndGoBack();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (dialog == this.dialog) {
            this.dialog = null;
        }
    }

    private class MyShipPagerAdapter extends FragmentPagerAdapter {
        public MyShipPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ShipArrayFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return Spaceship.SHIP_LIST.size();
        }
    }

    public static class ShipArrayFragment extends Fragment {

        int num;

        static ShipArrayFragment newInstance(int num) {
            ShipArrayFragment fragment = new ShipArrayFragment();
            Bundle args = new Bundle();
            args.putInt("num", num);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            num = (getArguments() != null) ? getArguments().getInt("num") : 1;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.ship_fragment_layout, container, false);

            ImageView iv = (ImageView) v.findViewById(R.id.ship);
            iv.setImageBitmap(Spaceship.getBitmap(Spaceship.SHIP_LIST.get(num)));

            if(this.num != 0){
                ImageView leftArrow = (ImageView) v.findViewById(R.id.arrowl);
                leftArrow.setImageResource(R.drawable.arrowl);
            }

            if(this.num != Spaceship.SHIP_LIST.size() - 1){
                ImageView rightArrow = (ImageView) v.findViewById(R.id.arrowr);
                rightArrow.setImageResource(R.drawable.arrowr);
            }

            return v;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }
    }

    private class PageChangeListener extends ViewPager.SimpleOnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            selectedShip = Spaceship.SHIP_LIST.get(position);
        }

    }

}
