package com.exaeone.starwalls.app;

import android.content.Context;
import com.exaeone.starwalls.metagame.AchievementManager;
import com.exaeone.starwalls.game.HighScoreManager;

/*
 * Collection of various components that deal with persistent data.
 */
public class AppDataManagers {

    private final PersistentStorage persistentStorage;
    private final HighScoreManager highScoreManager;
    private final AchievementManager achievementManager;

    public AppDataManagers(Context context) {
        persistentStorage = new PersistentStorage(context);
        highScoreManager = new HighScoreManager(persistentStorage);
        achievementManager = new AchievementManager(persistentStorage);
    }

    public AchievementManager getAchievementManager() {
        return achievementManager;
    }

    public HighScoreManager getHighScoreManager() {
        return highScoreManager;
    }

}
