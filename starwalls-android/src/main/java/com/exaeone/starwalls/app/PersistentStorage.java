package com.exaeone.starwalls.app;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kathryn on 28/03/15.
 */
public class PersistentStorage {

    private static final String STORAGE_NAME = "storage";

    private final SharedPreferences storage;

    public PersistentStorage(Context context) {
        storage = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = storage.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = storage.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return storage.getBoolean(key, defValue);
    }

    public int getInt(String key, int defValue){
        return storage.getInt(key, defValue);
    }

}
