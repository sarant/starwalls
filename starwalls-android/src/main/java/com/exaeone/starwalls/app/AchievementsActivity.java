package com.exaeone.starwalls.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.metagame.Achievement;
import com.exaeone.starwalls.metagame.AchievementAdapter;

public class AchievementsActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private ListView achievementListView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievements);

        achievementListView = (ListView) findViewById(R.id.listview_achievements);
        achievementListView.setAdapter(new AchievementAdapter());
        achievementListView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.globalStart(this);
    }

    @Override
    protected void onPause() {
        App.globalStop(this);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        App.transitionBegin(this);
        super.onBackPressed();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == achievementListView) {
            AchievementAdapter adapter = (AchievementAdapter) parent.getAdapter();
            Achievement a = (Achievement) adapter.getItem(position);
            App.getDataManagers().getAchievementManager().toggle(a);
            adapter.notifyDataSetChanged();
        }
    }
}

