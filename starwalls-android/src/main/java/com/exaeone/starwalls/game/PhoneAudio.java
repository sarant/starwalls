package com.exaeone.starwalls.game;

import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.metagame.AchievementManager;
import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.event.EventBus;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by kathryn on 30/03/15.
 */
public class PhoneAudio {

    private final SoundPool soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);

    private final Map<Integer, Integer> soundIDList = new HashMap<>();

    private GameConfiguration config;

    public PhoneAudio (GameConfiguration conf) {
        config = conf;

        loadSound(R.raw.hit_large1);
        loadSound(R.raw.hit_large2);
        loadSound(R.raw.hit_medium1);
        loadSound(R.raw.hit_medium2);
        loadSound(R.raw.hit_small1);
        loadSound(R.raw.hit_small2);
        loadSound(R.raw.explode);
        loadSound(R.raw.simplebeep);
        loadSound(R.raw.warning);
        loadSound(R.raw.achievement);

        EventBus.DEFAULT.subscribe(this);
    }

    private void loadSound (int resID){
        soundIDList.put(resID, soundPool.load(App.getContext(),resID,1));
    }

    public void onEvent(Game.OnWallPassed evt) {
        float damage = evt.getDamage();
        if (damage == 0) {
            soundPool.play(soundIDList.get(R.raw.simplebeep), 1.0f, 1.0f, 0, 0, 1.0f);
            return;
        }
        else if (damage < 0.3) {
            if (config.random.nextFloat() < 0.5) {
                soundPool.play(soundIDList.get(R.raw.hit_small1), 1.0f, 1.0f, 0, 0, 1.0f);
            } else {
                soundPool.play(soundIDList.get(R.raw.hit_small2), 1.0f, 1.0f, 0, 0, 1.0f);
            }
        }
        else if (damage < 0.6) {
            if (config.random.nextFloat() < 0.5) {
                soundPool.play(soundIDList.get(R.raw.hit_medium1), 1.0f, 1.0f, 0, 0, 1.0f);
            } else {
                soundPool.play(soundIDList.get(R.raw.hit_medium2), 1.0f, 1.0f, 0, 0, 1.0f);
            }
        }
        else {
            if (config.random.nextFloat() < 0.5) {
                soundPool.play(soundIDList.get(R.raw.hit_large1), 1.0f, 1.0f, 0, 0, 1.0f);
            } else {
                soundPool.play(soundIDList.get(R.raw.hit_large2), 1.0f, 1.0f, 0, 0, 1.0f);
            }
        }

        if(evt.getShield() == 0.0 ) {
            soundPool.play(soundIDList.get(R.raw.warning), 1.0f, 1.0f, 0, 0, 1.0f);
        }
    }

    public void onEvent(Game.OnDeath evt) {
        soundPool.play(soundIDList.get(R.raw.explode), 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public void onEvent(AchievementManager.OnNewAchievement evt) {
        soundPool.play(soundIDList.get(R.raw.achievement), 1.0f, 1.0f, 0, 0, 1.0f);
    }
}
