package com.exaeone.starwalls.game;

import android.util.Log;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.msg.out.GameOutboundMessage;
import com.exaeone.starwalls.net.WebSocketConnection;
import com.exaeone.starwalls.util.ConcurrentLinkedDeque;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by saran on 05/03/15.
 */
public class AggregateConnection {

    public static final String TAG = "AggregateConnection";

    private final AtomicBoolean open = new AtomicBoolean(true);
    private final ConcurrentLinkedDeque<WebSocketConnection> connections = new ConcurrentLinkedDeque<>();

    public AggregateConnection(WebSocketConnection connection) {
        connections.add(connection);
        EventBus.DEFAULT.subscribe(this);
        EventBus.DEFAULT.post(new OnOpen(), this);
        Log.i(TAG, "Opened new aggregator");
    }

    public void add(WebSocketConnection connection) {
        if (open.get()) {
            int count = connections.size();
            if (count == 1) {
                connections.add(connection);
                Log.i(TAG, "Accepted secondary WebSocketConnection as a realtime connection");
            } else if (count > 1) {
                Log.e(TAG, "This aggregator already had two connections. Closing the new connection.");
                connection.close();
            }
        }
    }

    public boolean contains(WebSocketConnection connection) {
        if (open.get()) {
            return connections.contains(connection);
        }
        else{
            return false;
        }
    }

    public void onEvent(WebSocketConnection.OnClose evt) {
        if (open.get()) {
            WebSocketConnection conn = (WebSocketConnection) evt.source();
            if (connections.remove(conn)) {
                if (connections.size() == 0) {
                    close();
                    Log.i(TAG, "Main connection closed without a secondary, tearing down");
                }
                else {
                    Log.i(TAG, "A member connection is closed, but there are still others remaining");
                }
            }
        }
    }

    public void close() {
        if (open.get()) {
            open.set(false);

            for (WebSocketConnection conn : connections) {
                conn.close();
            }

            connections.clear();

            EventBus.DEFAULT.unsubscribe(this);
            EventBus.DEFAULT.post(new OnClose(), this);
        }
    }

    public void send(GameOutboundMessage msg) {
        if (open.get()) {
            WebSocketConnection mainConnection = connections.peekFirst();
            if (mainConnection != null) {
                mainConnection.send(msg.serialize().commit());
            }
        }
    }

    public void sendRealtime(GameOutboundMessage msg) {
        if (open.get()) {
            WebSocketConnection realtimeConnection = connections.peekLast();
            if (realtimeConnection != null) {
                realtimeConnection.sendRealtime(msg.getClass(), msg.serialize().commit());
            }
        }
    }

    public static class OnOpen extends Event {

    }

    public static class OnClose extends Event {

    }

}
