package com.exaeone.starwalls.game;

import android.content.Context;
import android.hardware.SensorManager;
import android.util.Log;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.input.SensorInput;
import com.exaeone.starwalls.msg.in.*;
import com.exaeone.starwalls.msg.out.*;
import com.exaeone.starwalls.pool.DefaultPool;
import com.exaeone.starwalls.video.*;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by saran on 11/02/15.
 */
public class GameExecutor {

    public static final String TAG = "GameExecutor";

    private final AtomicReference<AggregateConnection> remoteRef = new AtomicReference<>(null);
    private SensorInput sensorInput = new SensorInput((SensorManager) App.getContext().getSystemService(Context.SENSOR_SERVICE));

    private final GameConfiguration config = new GameConfiguration();
    private final Game game = new Game();
    private final PhoneAudio audioPlayer = new PhoneAudio(config);

    private volatile boolean receivedWindowOnLoad = false;
    private volatile boolean receivedVideoSize = false;
    private volatile boolean receivedMediaSourceReady = false;
    private volatile int streamSerialNumber = 0;

    private volatile long gameTimeMillis;
    private volatile long renderTimeMillis;
    private volatile long availableMillis;

    private final AtomicBoolean started = new AtomicBoolean(false);
    private final AtomicBoolean readyToStartPosted = new AtomicBoolean(false);
    private final AtomicBoolean crashReported = new AtomicBoolean(false);
    private int crashCount = 0;
    private final int MAX_CRASH = 3;

    private final int VIDEO_BUFFER_MIN_LEVEL = 5000;
    private final int VIDEO_BUFFER_RESUME_LEVEL = 10000;
    private final int VIDEO_BUFFER_MAX_LEVEL = 15000;

    private VideoRenderer videoRenderer = new VideoRenderer();

    private AtomicInteger framesPending = new AtomicInteger(0);

    public void setup(AggregateConnection remote) {
        if (remoteRef.compareAndSet(null, remote)) {
            started.set(false);

            // Send a hello to remote so that we can learn the stage size etc.
            remote.send(new ServerHello());

            EventBus.DEFAULT.subscribe(this);
            EventBus.DEFAULT.subscribe(game);
            EventBus.DEFAULT.subscribe(audioPlayer);

            setUpSplash();

            // Send the graphics data for our spaceship
            sendGraphicsForShip(new SpaceshipOne());
            sendGraphicsForShip(new SpaceshipTwo());
            sendGraphicsForShip(new SpaceshipThree());
        }
    }

    public void teardown() {
        AggregateConnection remote = remoteRef.getAndSet(null);
        if (remote != null) {
            EventBus.DEFAULT.unsubscribe(this);
            EventBus.DEFAULT.unsubscribe(game);
            EventBus.DEFAULT.unsubscribe(audioPlayer);

            remote.close();
            stopGame();
        }
    }

    private void setUpSplash() {
        AggregateConnection remote = remoteRef.get();
        if (remote != null){
            remote.send(new GraphicsData()
                            .setName("splash")
                            .setResourceID(R.drawable.splash)
            );
            remote.send(new SetSplash()
                            .setSplashImage("splash")
            );
        }
    }

    private void sendGraphicsForShip(Spaceship ship) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            remote.send(new GraphicsData()
                            .setName(ship.getGraphicsName())
                            .setResourceID(ship.getReducedSizeResourceID())
            );

            remote.send(new GraphicsData()
                            .setName(ship.getEndgameGraphicsName())
                            .setResourceID(ship.getEndgameResourceID())
            );
        }
    }

    public void onEvent(AggregateConnection.OnClose evt) {
        if (evt.source() == remoteRef.get()) {
            EventBus.DEFAULT.post(new OnRemoteDisconnected(), this);
        }
    }

    public void onEvent(VideoSize evt) {
        AggregateConnection remote = remoteRef.get();
        if (remoteRef.get() != null) {
            config.width = evt.getStageWidth();
            config.height = evt.getStageHeight();
            receivedVideoSize = true;
            Log.i(TAG, "VideoSize");
            initForNewGameIfReady();
        }
    }

    public void onEvent(MediaSourceReady evt) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            receivedMediaSourceReady = true;
            streamSerialNumber = evt.getStreamSerialNumber();
            Log.i(TAG, "MediaSourceReady");
            initForNewGameIfReady();
        }
    }

    public boolean isReady() {
        return (remoteRef.get() != null) && receivedVideoSize && receivedMediaSourceReady;
    }

    private void initForNewGameIfReady() {
        if (isReady()) {
            started.set(false);
            crashReported.set(false);
            readyToStartPosted.set(false);

            gameTimeMillis = 0;
            renderTimeMillis = 0;
            availableMillis = 0;

            game.init(config);
            videoRenderer.start();
        }
    }

    public void onEvent(VideoStream.OnFragmentReady evt) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            VideoFragment frag = evt.fragment();
            frag.retain();
            remote.send(frag);
        }
    }

    public void onEvent(VideoFragment.OnSend evt) {
        framesPending.getAndDecrement();
        checkReadyToStart();
    }

    public void onEvent(VideoBufferLevel evt) {
        availableMillis = evt.getBufferLevel();
        checkReadyToStart();
    }


    private void checkReadyToStart() {
        if (remoteRef.get() != null
                && receivedWindowOnLoad
                && (availableMillis - gameTimeMillis) >= VIDEO_BUFFER_MIN_LEVEL
                && readyToStartPosted.compareAndSet(false, true)) {
            EventBus.DEFAULT.post(new OnReadyToStart(), GameExecutor.this);
        }
    }

    public void onEvent(WindowOnLoad evt) {
        receivedWindowOnLoad = true;
        checkReadyToStart();
    }

    public void startGame(Class<? extends Spaceship> selectedShip) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null && started.compareAndSet(false, true)) {
            try {
                config.ship = selectedShip.newInstance();
            } catch (InstantiationException e) {
                config.ship = new SpaceshipOne();
            } catch (IllegalAccessException e) {
                config.ship = new SpaceshipOne();
            }

            remote.sendRealtime(new SetSpaceship()
                    .setGraphics(config.ship.getGraphicsName())
                    .setSize(config.playerWidth, config.playerHeight));

            EventBus.DEFAULT.post(new OnStart(), this);
            game.start();
            sensorInput.start();
            remote.send(new GameStart());
        }
    }

    public void stopGame() {
        boolean notify = false;

        if (started.compareAndSet(true, false)) {
            AggregateConnection remote = remoteRef.get();
            if (remote != null) {
                remote.sendRealtime(DefaultPool.obtain(GameStop.class)
                        .setFinalScore(game.getScore())
                        .setBestStreak(game.getBestStreak())
                        .setEndGameGraphics(game.config.ship.getEndgameGraphicsName()));
            }

            notify = true;
        }

        sensorInput.stop();
        videoRenderer.stop(true);
        game.stop(true);

        if (notify) {
            EventBus.DEFAULT.post(new OnStop(), this);
        }
    }

    public void onEvent(GameSync evt) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            this.gameTimeMillis = evt.gameTimeMillis();
            game.updateAtTime(evt.gameTimeMillis(), evt.x(), evt.y(), evt.roll());
        }
    }

    public void onEvent(Game.OnWallPassed evt) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            float damage = evt.getDamage();

            UpdateShield outEvent = DefaultPool.obtain(UpdateShield.class)
                    .setShield(game.getShield());

            if (damage > 0.0) {
                outEvent.setHit(evt.getPlayerX(), evt.getPlayerY(), evt.getTime(), game.getHitBitmap());
                remote.send(outEvent);
            } else {
                remote.sendRealtime(outEvent);
            }
        }
    }

    public void onEvent(Game.OnScoreChanged evt) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            remote.sendRealtime(DefaultPool.obtain(UpdateScore.class).setScore(evt.getScore()));
        }
    }

    public void onEvent(Game.OnTextMessage evt){
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            if (evt.getDisplayTime() > 0) {
                remote.sendRealtime(DefaultPool.obtain(GameText.class)
                                .setDisplayTime(evt.getDisplayTime())
                                .setMessage(evt.getMessage())
                );
            } else {
                remote.send(DefaultPool.obtain(GameText.class)
                                .setDisplayTime(evt.getDisplayTime())
                                .setMessage(evt.getMessage())
                );
            }
        }
    }

    public void setupForNewGame() {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            receivedMediaSourceReady = false;
            remote.send(DefaultPool.obtain(ClearVideoBuffer.class));
        }
    }

    public void onEvent(Game.OnDeath evt) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            stopGame();
            setupForNewGame();
        }
    }

    public void onEvent(SensorInput.OnUpdate evt) {
        AggregateConnection remote = remoteRef.get();
        if (remote != null) {
            remote.sendRealtime(DefaultPool.obtain(PlayerMove.class)
                            .setPosition(evt.x * (config.width / 2), evt.y * (config.height / 2), evt.roll)
            );
        }
    }

    public void onEvent(AVCEncoder.OnCrash evt) {
        reportCrash(evt.getThrowable());
    }

    private void reportCrash(Throwable e) {
        if (crashReported.compareAndSet(false, true)) {
            crashCount += 1;
            EventBus.DEFAULT.post(new OnCrash()
                    .setThrowable(e)
                    .setGivingUp(crashCount >= MAX_CRASH), this);
            stopGame();
        }
    }

    public static class OnStart extends Event {}

    public static class OnStop extends Event {}

    private class VideoRenderer implements Runnable {

        private Thread thread;
        private final AVCEncoder encoder;
        private boolean active = false;

        VideoRenderer() {
            encoder = new AVCEncoder();
        }

        void start() {
            active = true;
            thread = new Thread(this, "VideoRenderer");
            thread.start();
        }

        void stop() {
            stop(false);
        }

        void stop(boolean synchronous) {
            active = false;
            if (synchronous) {
                //noinspection StatementWithEmptyBody
                while (thread != null && thread.isAlive());
            }
            thread = null;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LESS_FAVORABLE + 3);

            availableMillis = -1000 / AVCEncoder.FPS;
            framesPending.set(1);

            encoder.start(streamSerialNumber);
            CodecInputSurface surface = encoder.getSurface();

            game.initRenderer();

            boolean keepRendering = true;

            while (active) {
                long startTime = System.currentTimeMillis();
                long bufferRendered = renderTimeMillis - gameTimeMillis;

                if (bufferRendered > VIDEO_BUFFER_MAX_LEVEL) {
                    keepRendering = false;
                }
                else if (!keepRendering && bufferRendered < VIDEO_BUFFER_RESUME_LEVEL) {
                    keepRendering = true;
                }

                if (keepRendering && framesPending.get() < 10) {
                    framesPending.getAndIncrement();

                    game.renderAtTime(renderTimeMillis);

                    try {
                        surface.swapBuffers(renderTimeMillis);
                    }
                    catch (RuntimeException e) {
                        reportCrash(e);
                        stop();
                    }

                    renderTimeMillis += (1000 / AVCEncoder.FPS);
                }

                long totalTime = System.currentTimeMillis() - startTime;
                long targetTime = 1000 / AVCEncoder.FPS;
                if (totalTime < targetTime && bufferRendered > VIDEO_BUFFER_MIN_LEVEL) {
                    try {
                        Thread.sleep(targetTime - totalTime);
                    } catch (InterruptedException e) {
                        // Do nothing
                    }
                }
            }

            encoder.stop();
        }
    }

    public static class OnReadyToStart extends Event {

    }

    public static class OnCrash extends Event {
        private Throwable e = null;
        private boolean givingUp = false;

        @Override
        public void onRecycle() {
            e = null;
            givingUp = false;
            super.onRecycle();
        }

        public OnCrash setThrowable(Throwable e) {
            this.e = e;
            return this;
        }

        public OnCrash setGivingUp(boolean givingUp) {
            this.givingUp = givingUp;
            return this;
        }

        public Throwable getThrowable() {
            return e;
        }

        public boolean isGivingUp(){
            return givingUp;
        }
    }

    public static class OnRemoteDisconnected extends Event {

    }

}
