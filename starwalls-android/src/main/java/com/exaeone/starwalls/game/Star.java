package com.exaeone.starwalls.game;

import com.exaeone.starwalls.pool.Poolable;

import java.nio.FloatBuffer;

/**
 * Created by saran on 28/02/15.
 */
public class Star extends Poolable {

    /*package*/ long z;

    /*package*/ double x;
    /*package*/ double y;
    /*package*/ double radius;

    private static final int MIN_RADIUS = 2;
    private static final int MAX_RADIUS = 10;

    private static final double PI_BY_FOUR = 0.7853981633974483096d;
    private static final double THREE_PI_BY_FOUR = 2.3561944901923449288d;

    public Star configure(long z, GameConfiguration config) {
        this.z = z;

        double direction = 2 * Math.PI * (config.random.nextDouble() - 0.5d);
        double distance = 4 * config.random.nextDouble() + 1;

        if (-PI_BY_FOUR < direction && direction <= PI_BY_FOUR) {
            // RIGHT
            this.x = distance * config.width / 2;
            this.y = distance * config.height * Math.tan(direction) / 2;
        }
        else if (PI_BY_FOUR < direction && direction <= THREE_PI_BY_FOUR) {
            // TOP
            this.x = distance * config.width / (2 * Math.tan(direction));
            this.y = distance * config.height / 2;
        }
        else if (direction > THREE_PI_BY_FOUR || direction <= -THREE_PI_BY_FOUR) {
            // LEFT
            this.x = -distance * config.width / 2;
            this.y = distance * config.height * Math.tan(Math.PI - direction) / 2;
        }
        else {
            // BOTTOM
            this.x = distance * config.width / (2 * Math.tan(direction - Math.PI));
            this.y = -distance * config.height / 2;
        }

        this.radius = MIN_RADIUS + (MAX_RADIUS - MIN_RADIUS)  * config.random.nextDouble();

        return this;
    }

    public void writeToVertexBuffer(FloatBuffer vertexBuffer) {
        vertexBuffer.put((float) this.x);
        vertexBuffer.put((float) this.y);
        vertexBuffer.put((float) this.z);
        vertexBuffer.put((float) this.radius);
    }

}
