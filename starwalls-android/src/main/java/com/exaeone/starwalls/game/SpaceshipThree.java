package com.exaeone.starwalls.game;

import android.graphics.Path;
import android.graphics.RectF;
import com.exaeone.starwalls.R;
import com.larvalabs.svgandroid.SVGParser;

/**
 * Created by kathryn on 01/04/15.
 */
public class SpaceshipThree extends Spaceship {

    // SVG path definition for the spaceship. Asset size is 400x160px. Use Matrix to rescale as desired.
    private static final String pathString =
            "M 632.5 179.90625 C 612.45965 180.66728 595.97281 196.20476 586.8125 212.9375 C 563.32641 218.68473 " +
            "533.48247 226.15101 495 230.6875 C 492.95888 227.57935 490.24227 224.92738 487 222.96875 C 486.69551 " +
            "221.75792 485.09897 220.22623 481.625 217.90625 C 466.87183 208.0538 456.15641 193.78287 448.6875 212 " +
            "C 443.26134 225.23473 441.91016 244.26357 448.84375 257.0625 C 447.81612 258.39243 446.8633 260.14817 " +
            "445.9375 262.40625 C 439.29932 278.59718 438.76867 303.41586 452.03125 314.9375 C 454.48285 317.96309 " +
            "457.62542 320.44393 461.34375 322.0625 C 474.21244 327.66416 489.11516 320.95132 494.65625 307.09375 " +
            "C 496.73416 301.89716 497.19236 296.44199 496.28125 291.34375 L 577.53125 291.21875 C 583.71348 " +
            "317.65595 606.74687 337.28851 634.21875 337.25 C 661.69062 337.21149 684.70441 317.51692 690.8125 " +
            "291.0625 L 784 290.9375 C 782.74787 296.42711 783.09072 302.42719 785.375 308.09375 C 790.95492 " +
            "321.93573 805.89707 328.57522 818.75 322.9375 C 822.46378 321.30851 825.58814 318.84495 828.03125 " +
            "315.8125 C 841.26148 304.25372 840.68355 279.42224 834 263.25 C 833.0677 260.99411 832.09406 259.23338 " +
            "831.0625 257.90625 C 837.95657 245.08799 836.55615 226.09242 831.09375 212.875 C 823.5738 194.67888 " +
            "812.88174 208.98123 798.15625 218.875 C 794.72672 221.17923 793.13589 222.7014 792.8125 223.90625 C " +
            "792.8098 223.91631 792.81502 223.92748 792.8125 223.9375 C 789.56153 225.9154 786.8473 228.5856 " +
            "784.8125 231.71875 C 739.08358 225.06694 705.77317 217.50474 680.25 212.09375 C 670.95033 195.24902 " +
            "654.36369 179.87771 634 179.90625 C 633.50393 179.90695 632.99188 179.88757 632.5 179.90625 z";

    @Override
    public String getGraphicsName() {
        return "ship3";
    }

    @Override
    public int getResourceID() {
        return R.drawable.spaceship3;
    }

    @Override
    public  int getReducedSizeResourceID() {
        return R.drawable.smallspaceship3;
    }

    @Override
    public String getEndgameGraphicsName() {
        return "endgame3";
    }

    @Override
    public int getEndgameResourceID() { return R.drawable.endgame3; }

    private final Path path = SVGParser.parsePath(pathString);
    private final RectF bounds = new RectF();

    public SpaceshipThree() {
        path.computeBounds(bounds, true);
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public RectF getBounds() {
        return bounds;
    }

}
