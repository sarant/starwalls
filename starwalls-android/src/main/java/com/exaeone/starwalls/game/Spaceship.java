package com.exaeone.starwalls.game;

import android.graphics.*;
import com.exaeone.starwalls.app.App;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saran on 28/04/2014.
 */
public abstract class Spaceship {

    // Sets the aspect ratio of all ship designs.
    // This is necessary for us to pre-render video frames
    // before the user selects a ship.
    public static final int WIDTH_UNITS = 5;
    public static final int HEIGHT_UNITS = 2;

    public abstract String getGraphicsName();

    abstract String getEndgameGraphicsName();

    public abstract int getResourceID();

    public abstract int getReducedSizeResourceID();

    abstract int getEndgameResourceID();

    public abstract Path getPath();
    public abstract RectF getBounds();

    public static final List<Class<? extends Spaceship>> SHIP_LIST = new ArrayList<>();
    static {
        SHIP_LIST.add(SpaceshipOne.class);
        SHIP_LIST.add(SpaceshipTwo.class);
        SHIP_LIST.add(SpaceshipThree.class);
    }

    public static Bitmap getBitmap(Class<? extends Spaceship> spaceshipClass) {
        try {
            int resourceID = spaceshipClass.newInstance().getResourceID();
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            return BitmapFactory.decodeResource(App.getContext().getResources(), resourceID, options);
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
        catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        }
    }

}
