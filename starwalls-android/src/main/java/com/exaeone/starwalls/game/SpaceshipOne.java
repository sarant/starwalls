package com.exaeone.starwalls.game;

import android.graphics.*;
import com.exaeone.starwalls.R;
import com.larvalabs.svgandroid.SVGParser;

/**
 * Created by saran on 27/04/2014.
 */
public class SpaceshipOne extends Spaceship {

	// SVG path definition for the spaceship. Asset size is 400x160px. Use Matrix to rescale as desired.
	private static final String pathString =
			"M197.255,0.176c-5.459-19.622-19.742-32.802-32.047-40.873c-18.386-12.061-42.583-19.262-64.728-19.262" +
			"l-0.962,0.005c-9.933,0.096-16.938,2.208-21.333,6.434c-6.255-3.542-13.537-6.034-20.812-7.158C35.212-72.613,12.995-80-2.193-80" +
			"c-15.979,0-38.381,7.563-60.527,20.018c-5.625,1.226-11.147,3.24-16.084,5.899c-4.423-3.849-11.233-5.779-20.712-5.871" +
			"l-0.962-0.005c-22.146,0-46.343,7.201-64.729,19.262c-12.314,8.078-26.611,21.271-32.062,40.917" +
			"c-1.77,5.836-2.73,12.044-2.73,18.484C-200,52.503-173.69,80-141.351,80c0,0,0.001,0,0.001,0s0.001,0,0.001,0" +
			"c23.718,0,44.178-14.8,53.409-36.021c23.938,6.18,54.783,9.568,87.481,9.568c33.077,0,64.249-3.471,88.302-9.787" +
			"C97.028,65.099,117.548,80,141.347,80c0.001,0,0.001,0,0.002,0s0.001,0,0.002,0C173.689,80,200,52.503,200,18.704" +
			"C200,12.247,199.033,6.024,197.255,0.176z";

    @Override
    public String getGraphicsName() {
        return "ship1";
    }

    @Override
    public int getResourceID() {
        return R.drawable.spaceship1;
    }

    @Override
    public  int getReducedSizeResourceID() {
        return R.drawable.smallspaceship1;
    }

    @Override
    public String getEndgameGraphicsName() {
        return "endgame1";
    }

    @Override
    public int getEndgameResourceID() { return R.drawable.endgame1; }

    private final Path path = SVGParser.parsePath(pathString);
    private final RectF bounds = new RectF();

    public SpaceshipOne() {
        path.computeBounds(bounds, true);
    }

    @Override
    public Path getPath() {
        return path;
    }

    @Override
    public RectF getBounds() {
        return bounds;
    }

}
