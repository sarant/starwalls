package com.exaeone.starwalls.game;

import android.graphics.*;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.pool.DefaultPool;

import java.nio.ByteOrder;
import com.exaeone.starwalls.util.ConcurrentLinkedDeque;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/**
 * Created by saran on 11/02/15.
 */
public class Game {

    /*package*/ ConcurrentLinkedDeque<Wall> walls = new ConcurrentLinkedDeque<>();
    /*package*/ ConcurrentLinkedDeque<Star> stars = new ConcurrentLinkedDeque<>();

    /*package*/ GameConfiguration config;

    /*package*/ float x;
    /*package*/ float y;
    /*package*/ float roll;

    /*package*/ float shield;
    /*package*/ int score;
    /*package*/ int streak;
    /*package*/ int bestStreak;

    /*package*/ long z;
    /*package*/ long lastUpdateTime;

    /*package*/ GameRenderer renderer = new GameRenderer();
    /*package*/ CollisionDetector collisionDetector = new CollisionDetector();

    /*package*/ long lastRenderNearZ = 0;
    /*package*/ long lastRenderFarZ = 0;

    private FloatBuffer wallsVtxBuffer = ByteBuffer.allocateDirect(128 * 1024).order(ByteOrder.nativeOrder()).asFloatBuffer();
    private FloatBuffer starsVtxBuffer = ByteBuffer.allocateDirect(128 * 1024).order(ByteOrder.nativeOrder()).asFloatBuffer();

    public Game() {
        EventBus.DEFAULT.subscribe(this);
    }

    public void init(GameConfiguration config) {
        this.config = config;

        this.x = 0;
        this.y = 0;
        this.roll = 0;

        this.shield = 1;
        this.score = 0;
        this.streak = 0;
        this.bestStreak = 0;

        this.z = 0;
        this.lastUpdateTime = 0;

        lastRenderNearZ = 0;
        lastRenderFarZ = 0;

        while (!stars.isEmpty()) {
            stars.pollLast().release();
        }

        while (!walls.isEmpty()) {
            walls.pollLast().release();
        }

        if (config.autoSizePlayer) {
            float shipWidth = Spaceship.WIDTH_UNITS;
            float shipHeight = Spaceship.HEIGHT_UNITS;

            // Calculate the spaceship's width by two fitting methods
            float fitByWidth = 0.2f * config.width;
            float fitByHeight = (0.2f * config.height) * (shipWidth / shipHeight);

            if (fitByWidth < fitByHeight) {
                config.playerWidth = (int) fitByWidth;
                config.playerHeight = (int) (fitByWidth * shipHeight / shipWidth);
            }
            else {
                config.playerWidth = (int) fitByHeight;
                config.playerHeight = (int) (fitByHeight * shipHeight / shipWidth);
            }
        }

        adjustHoleSize(config.firstWallZ);
        walls.add(DefaultPool.obtain(Wall.class).configure(config.firstWallZ, config));
    }

    public void start() {
        collisionDetector.start(config);
    }

    public void stop() {
        stop(false);
    }

    public void stop(boolean synchronous) {
        collisionDetector.stop(synchronous);
    }

    private void adjustHoleSize(long z) {
        if (config.adaptiveHoleSize) {
            float t = (float) Math.min((z - config.firstWallZ) / config.holeTimescale, 1.0);
            float x = config.holeTimeCtrlX;
            float y = config.holeTimeCtrlY;

            float holeScale = (float) (2*x*(x-y) + t*(1-2*x)*(1-2*y) + 2*Math.abs(x-y)*Math.sqrt(x*x-2*t*x+t)) / ((1-2*x)*(1-2*x));

            config.minHoleWidth = config.startHoleWidth + holeScale * (config.endHoleWidth - config.startHoleWidth);
            config.minHoleHeight = config.startHoleHeight + holeScale * (config.endHoleHeight - config.startHoleHeight);
            config.maxHoleWidth = config.minHoleWidth + 0.25f;
            config.maxHoleHeight = config.minHoleHeight + 0.25f;
        }
    }

    public void initRenderer() {
        renderer.init(config);
    }

    public Bitmap getHitBitmap() {
        return collisionDetector.getHitBitmap();
    }

    private void addToScoreAndNotify(int scoreDelta) {
        if (scoreDelta != 0) {
            this.score += scoreDelta;
            EventBus.DEFAULT.post(DefaultPool.obtain(OnScoreChanged.class)
                            .setScore(this.score), this);
        }
    }

    private void addToShield(float shieldDelta) {
        if (shieldDelta != 0) {
            shield += shieldDelta;
            if (shield < 0) {
                shield = 0;
            } else if (shield > 1) {
                shield = 1;
            }
        }
    }

    private void resetStreak() {
        if (streak != 0) {
            streak = 0;
            notifyStreakChanged(0);
        }
    }

    private int incrementStreak() {
        streak += 1;
        if (streak > bestStreak) {
            bestStreak = streak;
            EventBus.DEFAULT.post(DefaultPool.obtain(Game.OnBestStreak.class)
                    .setStreak(bestStreak), this);
        }

        int streakBonus = 0;
        if (streak > 1) {
            streakBonus = 25 * Math.min(streak, 10);
        }

        notifyStreakChanged(streakBonus);
        return streakBonus;
    }

    private void notifyStreakChanged(int streakBonus) {
        EventBus.DEFAULT.post(DefaultPool.obtain(OnStreakChanged.class)
                .setBonus(streakBonus)
                .setStreak(streak), this);
    }

    public void onEvent(CollisionDetector.OnDetection evt) {

        float recoveryPerWall = (shield > 0) ? 0.1f : 0.0f;
        float damage = evt.getDamage();

        int scoreDelta = (int) Math.ceil(100 * (1 - damage) * (1 - damage));
        float shieldDelta = -damage;
        boolean death = false;

        if (damage == 0) {
            scoreDelta += incrementStreak();
            if (streak > 1) {
                if (streak == 2) { shieldDelta += recoveryPerWall; }
                shieldDelta += recoveryPerWall;
            }
        }
        else {
            if (shield > 0) {
                resetStreak();
            }
            else {
                death = true;
            }
        }

        if (death) {
            // DO NOT CALL THIS SYNCHRONOUSLY AS THIS IS EXECUTED ON THE COLLISION DETECTOR'S THREAD
            // WILL CAUSE A DEADLOCK
            EventBus.DEFAULT.post(DefaultPool.obtain(OnDeath.class)
                    .setFinalScore(this.score)
                    .setBestStreak(this.bestStreak), this);
        }
        else {
            addToScoreAndNotify(scoreDelta);
            addToShield(shieldDelta);

            EventBus.DEFAULT.postSynchronously(
                    DefaultPool.obtain(OnWallPassed.class)
                            .copyFromDetectionEvent(evt)
                            .setShield(shield),
                    this
            );
        }

    }

    public float getShield() { return this.shield; }

    public int getScore(){ return this.score; }

    public int getBestStreak(){ return this.bestStreak; }

    public void updateAtTime(long gameTimeMillis, float x, float y, float roll) {
        long lastUpdateZ = this.z;
        float lastUpdateX = this.x;
        float lastUpdateY = this.y;
        float lastUpdateRoll = this.roll;

        this.z = config.speed * gameTimeMillis;
        this.x = x;
        this.y = y;
        this.roll = roll;

        if (!walls.isEmpty()) {
            Wall nearestWall = walls.peekLast();

            if (this.z >= nearestWall.z) {
                walls.pollLast();

                // Interpolate ship's position back to the moment it reached the wall
                double weight = (nearestWall.z - (double) lastUpdateZ) / (this.z - (double) lastUpdateZ);
                int xInterp = (int) Math.round(lastUpdateX + weight * (this.x - lastUpdateX));
                int yInterp = (int) Math.round(lastUpdateY + weight * (this.y - lastUpdateY));
                double rollInterp = normalizeAngle(lastUpdateRoll + weight * normalizeAngle(this.roll - lastUpdateRoll));

                collisionDetector.detectAt(nearestWall, xInterp, yInterp, rollInterp);
            }

        }

        EventBus.DEFAULT.post(DefaultPool.obtain(OnRotation.class)
                .setRoll(this.roll), this);

        int frameScoreDelta = 0;
        while (lastUpdateTime + 50 < gameTimeMillis) {
            frameScoreDelta += 1;
            lastUpdateTime += 50;
        }
        addToScoreAndNotify(frameScoreDelta);
    }

    public void renderAtTime(long renderTimeMillis) {
        long renderZ = config.speed * renderTimeMillis;

        boolean newWalls = false;

        Wall farthestWall = walls.peekFirst();

        while (farthestWall.z + config.minWallGap <= renderZ + config.wallClipZ) {
            long nextWallZ = farthestWall.z + Math.round(config.minWallGap + config.random.nextDouble() * (config.maxWallGap - config.minWallGap));
            adjustHoleSize(nextWallZ);
            farthestWall = DefaultPool.obtain(Wall.class).configure(nextWallZ, config);
            walls.addFirst(farthestWall);
            newWalls = true;
        }

        if (newWalls || renderZ > lastRenderFarZ || renderZ < lastRenderNearZ) {
            updateWallVtxBuffer(renderZ);
            updateStars(renderZ, farthestWall.z);
        }

        renderer.render(renderZ);
    }

    private void updateStars(long frontZ, long backZ) {
        Star farthestStar = stars.peekFirst();

        long startZ = (farthestStar != null) ? farthestStar.z : 0;

        for (long z = startZ; z < backZ; ++z) {
            if (config.random.nextDouble() < 0.3) {
                stars.addFirst(DefaultPool.obtain(Star.class).configure(z, config));
            }
        }

        while (stars.peekLast().z < frontZ) {
            stars.pollLast().release();
        }

        starsVtxBuffer.clear();
        for (Star star : stars) {
            star.writeToVertexBuffer(starsVtxBuffer);
        }
        starsVtxBuffer.flip();
        renderer.updateStarsBuffer(starsVtxBuffer);
    }

    private void updateWallVtxBuffer(long renderZ) {
        wallsVtxBuffer.clear();

        boolean alphaWallFound = false;
        long prevWallZ = walls.peekFirst().z;

        for (Wall wall : walls) {
            if (wall.z < renderZ) {
                if (alphaWallFound) {
                    break;
                }
                else {
                    alphaWallFound = true;
                    lastRenderNearZ = wall.z;
                    lastRenderFarZ = prevWallZ;
                }
            }

            wall.writeToVertexBuffer(wallsVtxBuffer);
            prevWallZ = wall.z;
        }

        if (!alphaWallFound) {
            lastRenderNearZ = 0;
            lastRenderFarZ = prevWallZ;
        }

        wallsVtxBuffer.flip();
        renderer.updateWallsBuffer(wallsVtxBuffer);
    }

    // A la http://stackoverflow.com/questions/2320986/
    private double normalizeAngle(double angle) {
        final double TWO_PI = 2 * Math.PI;
        double normalized = angle;
        while (normalized < -Math.PI) normalized += TWO_PI;
        while (normalized >= Math.PI) normalized -= TWO_PI;
        return normalized;
    }

    public static class OnTextMessage extends Event {

        private int displayTime;
        private String message;

        public OnTextMessage setDisplayTime(int input) {
            this.displayTime = input;
            return this;
        }

        public OnTextMessage setMessage(String input) {
            this.message = input;
            return this;
        }

        public int getDisplayTime() {
            return this.displayTime;
        }

        public String getMessage() {
            return this.message;
        }

    }

    public static class OnDeath extends Event {

        private int finalScore;
        private int bestStreak;

        public OnDeath setFinalScore(int input) {
            this.finalScore = input;
            return this;
        }

        public OnDeath setBestStreak(int bestStreak) {
            this.bestStreak = bestStreak;
            return this;
        }


        public int getFinalScore() {
            return this.finalScore;
        }

        public int getBestStreak() {
            return bestStreak;
        }

    }

    public static class OnWallPassed extends Event {
        private float damage;
        private float shield;
        private int playerX;
        private int playerY;
        private long time;

        public OnWallPassed copyFromDetectionEvent(CollisionDetector.OnDetection evt) {
            this.damage = evt.getDamage();
            this.playerX = evt.getX();
            this.playerY = evt.getY();
            this.time = evt.getTime();
            return this;
        }

        public OnWallPassed setShield(float shieldsLevel) {
            this.shield = shieldsLevel;
            return this;
        }

        public float getDamage() {
            return damage;
        }

        public float getShield() {
            return shield;
        }

        public int getPlayerX() {
            return playerX;
        }

        public int getPlayerY() {
            return playerY;
        }

        public long getTime() {
            return time;
        }
    }

    public static class OnScoreChanged extends Event {

        private int score;

        public OnScoreChanged setScore(int score) {
            this.score = score;
            return this;
        }

        public int getScore() {
            return score;
        }

    }

    public static class OnStreakChanged extends Event {

        private int streak;
        private int bonus;

        public OnStreakChanged setStreak(int streak) {
            this.streak = streak;
            return this;
        }

        public OnStreakChanged setBonus(int bonus) {
            this.bonus = bonus;
            return this;
        }

        public int getBonus() {
            return bonus;
        }

        public int getStreak() {
            return streak;
        }
    }

    public static class OnBestStreak extends Event {

        private int streak;

        public OnBestStreak setStreak(int streak) {
            this.streak = streak;
            return this;
        }


        public int getStreak() {
            return streak;
        }

    }

    public static class OnRotation extends Event {

        private float roll;

        public OnRotation setRoll(float roll) {
            this.roll = roll;
            return this;
        }

        public float getRoll() {

            return roll;
        }
    }

}
