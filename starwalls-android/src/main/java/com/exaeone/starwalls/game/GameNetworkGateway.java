package com.exaeone.starwalls.game;

import android.content.Context;
import android.util.Log;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.app.App;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.msg.in.ClientHello;
import com.exaeone.starwalls.msg.in.GameInboundMessage;
import com.exaeone.starwalls.net.WebSocketConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by saran on 03/03/15.
 */
public class GameNetworkGateway {

    // TODO: Run a cleanup task to timeout waitingForHello
    private List<WebSocketConnection> waitingForHello = new ArrayList<>();
    private AtomicReference<AggregateConnection> aggregator = new AtomicReference<>(null);

    public static final String TAG = "GameNetworkGateway";
    public static final String WEBSOCKET_SUBPROTOCOL_NAME = "starwalls";

    private final int protocolVersion;

    public GameNetworkGateway(Context context) {
        protocolVersion = context.getResources().getInteger(R.integer.protocol_version);
        EventBus.DEFAULT.subscribe(this);
    }

    public AggregateConnection getAggregator() {
        return aggregator.get();
    }

    public void onEvent(WebSocketConnection.OnOpen evt) {
        WebSocketConnection conn = (WebSocketConnection) evt.source();
        conn.accept();
        Log.i(TAG, "Accepted new connection");
        synchronized (waitingForHello) {
            waitingForHello.add(conn);
        }
    }

    public void onEvent(WebSocketConnection.OnIncomingBinary evt) {
        WebSocketConnection conn = (WebSocketConnection) evt.source();
        GameInboundMessage inbound = GameInboundMessage.deserialize(evt.getPayload());

        AggregateConnection agg = aggregator.get();

        if (agg != null && agg.contains(conn)) {
            dispatchInboundMessage(inbound);
        }
        else {
            boolean removed;
            synchronized (waitingForHello) {
                removed = waitingForHello.remove(conn);
            }

            // This is true iff the gateway is currently expecting this connection to say Hello
            if (removed) {
                if (inbound.getClass() == ClientHello.class) {
                    ClientHello hello = (ClientHello) inbound;
                    int minVersion = hello.getMinimumProtocolVersion();
                    if (minVersion > protocolVersion) {
                        EventBus.DEFAULT.post(new OnVersionTooLow(), this);
                        conn.close();
                    }
                    else {
                        addToAggregator(conn);
                    }
                }
                else {
                    Log.e(TAG, "First message received from WebSocketConnection is not a ClientHello... closing");
                    conn.close();
                }
            }
        }
    }

    private void dispatchInboundMessage(GameInboundMessage inbound) {
        if (inbound.isRealtime()) {
            EventBus.DEFAULT.postSynchronously(inbound, this);
        }
        else {
            EventBus.DEFAULT.post(inbound, this);
        }
    }

    private void addToAggregator(WebSocketConnection conn) {
        boolean newAggregator = false;
        AggregateConnection currentAggregator = aggregator.get();

        if (currentAggregator == null) {
            newAggregator = aggregator.compareAndSet(null, new AggregateConnection(conn));
            if (!newAggregator) {
                currentAggregator = aggregator.get();
            }
        }

        if (!newAggregator)  {
            currentAggregator.add(conn);
        }
    }

    public void onEvent(AggregateConnection.OnClose evt) {
        AggregateConnection agg = (AggregateConnection) evt.source();
        if (agg == aggregator.get()) {
            aggregator.compareAndSet(agg, null);
        }
    }


    public static class OnVersionTooLow extends Event {
    }

}
