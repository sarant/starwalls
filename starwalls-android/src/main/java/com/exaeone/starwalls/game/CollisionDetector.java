package com.exaeone.starwalls.game;

import android.graphics.*;
import android.util.Log;
import com.exaeone.starwalls.event.Event;
import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.pool.DefaultPool;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;

/**
 * Created by saran on 03/03/15.
 */
public class CollisionDetector {

    public static final String TAG = "CollisionDetector";

    private volatile Wall wall;
    private volatile int collideX;
    private volatile int collideY;
    private volatile double collideRoll;

    private GameConfiguration config;
    private Region collisionRegion = new Region();
    private Matrix collisionTransform = new Matrix();
    private Rect collisionRect = new Rect();
    private Path collisionPath = new Path();

    private int hitSize;
    private Bitmap hitBitmap = null;
    private Canvas hitCanvas = null;

    private volatile Thread detectionThread = null;
    private final AtomicBoolean threadActive = new AtomicBoolean(false);

    public void start(GameConfiguration config) {
        // First, we stop any previously running instance
        this.stop();

        if (threadActive.compareAndSet(false, true)) {
            this.config = config;
            this.wall = null;

            if (hitBitmap != null) {
                hitBitmap.recycle();
            }

            hitSize = (int) Math.ceil(0.5 * Math.sqrt(config.playerWidth * config.playerWidth + config.playerHeight * config.playerHeight));
            hitBitmap = Bitmap.createBitmap(hitSize, hitSize, Bitmap.Config.ARGB_8888);
            hitCanvas = new Canvas(hitBitmap);
            hitCanvas.translate(hitSize / 2, hitSize / 2);
            hitCanvas.scale(0.5f, 0.5f);

            detectionThread = new Thread(new DetectionTask(), "CollisionDetector");
            detectionThread.start();
        }
    }

    public void stop() {
        stop(false);
    }

    public void stop(boolean synchronous) {
        if (threadActive.compareAndSet(true, false)) {
            LockSupport.unpark(detectionThread);
            if (synchronous) {
                //noinspection StatementWithEmptyBody
                while (detectionThread != null && detectionThread.isAlive()) ;
            }
            detectionThread = null;
        }
    }

    public void detectAt(Wall wall, int collideX, int collideY, double collideRoll) {
        synchronized (this) {
            this.wall = wall;
            this.collideX = collideX;
            this.collideY = collideY;
            this.collideRoll = collideRoll;
        }
        LockSupport.unpark(detectionThread);
    }

    public Bitmap getHitBitmap() {
        return hitBitmap;
    }

    private class DetectionTask implements Runnable {
        @Override
        public void run() {
            Log.i(TAG, "Started");

            while (threadActive.get()) {
                // Make a local copy of the parameters to minimise synchronisation block
                Wall wall;
                int collideX, collideY;
                double collideRoll;

                synchronized (CollisionDetector.this) {
                    wall = CollisionDetector.this.wall;
                    collideX = CollisionDetector.this.collideX;
                    collideY = CollisionDetector.this.collideY;
                    collideRoll = CollisionDetector.this.collideRoll;
                    CollisionDetector.this.wall = null;
                }

                if (wall == null) {
                    LockSupport.park(this);
                    continue;
                }

                Path shipPath = config.ship.getPath();
                RectF shipBounds = config.ship.getBounds();

                collisionTransform.setTranslate(-shipBounds.centerX(), -shipBounds.centerY());
                collisionTransform.postScale(config.playerWidth / shipBounds.width(), config.playerHeight / shipBounds.height());
                collisionTransform.postRotate((float) (collideRoll * 180 / Math.PI));
                collisionTransform.postTranslate(collideX, collideY);
                shipPath.transform(collisionTransform, collisionPath);

                collisionRegion.set(-config.width / 2, -config.height / 2, config.width / 2, config.height / 2);
                collisionRegion.setPath(collisionPath, collisionRegion);

                RegionIterator it;

                it = new RegionIterator(collisionRegion);
                float totalArea = 0;
                while (it.next(collisionRect)) {
                    totalArea += collisionRect.width() * collisionRect.height();
                }

                float damage;

                if (collisionRegion.setPath(wall.path, collisionRegion)) {
                    it = new RegionIterator(collisionRegion);
                    float clipArea = 0;
                    while (it.next(collisionRect)) {
                        clipArea += collisionRect.width() * collisionRect.height();
                    }

                    damage = clipArea / totalArea;

                    hitCanvas.drawColor(Color.argb(0, 255, 255, 255), PorterDuff.Mode.SRC);
                    hitCanvas.save();
                    hitCanvas.translate(-collideX, -collideY);
                    hitCanvas.clipPath(wall.path, Region.Op.REPLACE);
                    hitCanvas.clipPath(collisionPath, Region.Op.INTERSECT);
                    hitCanvas.drawColor(Color.RED, PorterDuff.Mode.SRC);
                    hitCanvas.restore();
                }
                else {
                    damage = 0;
                }

                EventBus.DEFAULT.postSynchronously(DefaultPool.obtain(OnDetection.class)
                            .setDamage(damage)
                            .setPosition(collideX, collideY, wall.z / config.speed), this);

                wall.release();
            }

            Log.i(TAG, "Stopped");
        }

    }

    public static class OnDetection extends Event {

        private float damage;
        private int playerX;
        private int playerY;
        private long detectionTime;

        public OnDetection setPosition(int x, int y, long time) {
            this.playerX = x;
            this.playerY = y;
            this.detectionTime = time;
            return this;
        }

        public OnDetection setDamage(float damage) {
            this.damage = damage;
            return this;
        }

        public float getDamage() {
            return this.damage;
        }

        public int getX() {
            return this.playerX;
        }

        public int getY() {
            return this.playerY;
        }

        public long getTime() {
            return this.detectionTime;
        }

    }


}
