package com.exaeone.starwalls.game;

import android.graphics.Path;
import com.exaeone.starwalls.pool.Poolable;
import org.poly2tri.geometry.polygon.Polygon;
import org.poly2tri.geometry.polygon.PolygonPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saran on 24/02/15.
 */
public class Hole extends Poolable {

    private final List<PolygonPoint> pointsList = new ArrayList<>();
    private Polygon polygon = null;
    private final Path path = new Path();

    private static final double MIN_DELTA = 0.05 * Math.PI;
    private static final double MAX_DELTA = 0.08 * Math.PI;
    private static final double HOLE_PADDING_FACTOR = 2;

    public Polygon getPolygon() {
        return polygon;
    }

    public Path getPath() {
        return path;
    }

    public Hole randomize(Wall wall, GameConfiguration config) {

        double minMajor = 0.5 * config.playerWidth * config.minHoleWidth;
        double maxMajor = 0.5 * config.playerWidth * config.maxHoleWidth;

        double minMinor = 0.5 * config.playerHeight * config.minHoleHeight;
        double maxMinor = 0.5 * config.playerHeight * config.maxHoleHeight;

        double major = minMajor + (maxMajor - minMajor) * config.random.nextDouble();
        double minor = minMinor + (maxMinor - minMinor) * config.random.nextDouble();
        double rotation = 2 * Math.PI * config.random.nextDouble();

        double maxPad = 0.5 * (Math.sqrt((major - minor) * (major - minor) + 4 * major * minor * HOLE_PADDING_FACTOR) - (major + minor));
        double padMajor = major + maxPad;
        double padMinor = minor + maxPad;

        double centreX, centreY;

        // Make sure hole is always fully contained within wall
        double extremeX = Math.sqrt((padMajor * padMajor + padMinor * padMinor + Math.cos(2 * rotation) * (padMajor * padMajor - padMinor * padMinor)));
        centreX = (config.random.nextDouble() - 0.5) * (wall.width - 2 * extremeX);

        double extremeY = Math.sqrt((padMajor * padMajor + padMinor * padMinor - Math.cos(2 * rotation) * (padMajor * padMajor - padMinor * padMinor)));
        centreY = (config.random.nextDouble() - 0.5) * (wall.height - 2 * extremeY);

        this.clearPoints();

        for (double angle = 0;
             angle < 2 * Math.PI;
             angle += (MIN_DELTA + (MAX_DELTA - MIN_DELTA) * config.random.nextDouble())) {

            double pad = maxPad * config.random.nextDouble();
            double u = (major + pad) * Math.cos(angle);
            double v = (minor + pad) * Math.sin(angle);

            double vertexX = centreX + u * Math.cos(rotation) - v * Math.sin(rotation);
            double vertexY = centreY + u * Math.sin(rotation) + v * Math.cos(rotation);

            this.addPoint(vertexX, vertexY);
        }

        polygon = new Polygon(pointsList);
        path.close();

        return this;

    }

    private void clearPoints() {
        pointsList.clear();
        path.reset();
    }

    private void addPoint(double x, double y) {
        pointsList.add(new PolygonPoint(x, y));

        if (path.isEmpty()) {
            path.moveTo((float) x, (float) y);
        }
        else {
            path.lineTo((float) x, (float) y);
        }
    }

}
