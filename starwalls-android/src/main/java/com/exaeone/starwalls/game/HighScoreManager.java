package com.exaeone.starwalls.game;

import com.exaeone.starwalls.event.EventBus;
import com.exaeone.starwalls.app.PersistentStorage;

/**
 * Created by kathryn on 28/03/15.
 */
public class HighScoreManager {

    private static final String KEY_SCORE = "high_score";
    private static final String KEY_STREAK = "max_streak";

    private final PersistentStorage persistentStorage;

    private int highScore;
    private int longestStreak;

    public HighScoreManager(PersistentStorage persistentStorage) {
        this.persistentStorage = persistentStorage;
        init();
        EventBus.DEFAULT.subscribe(this);
    }

    private void init() {
        highScore = persistentStorage.getInt(KEY_SCORE, 0);
        longestStreak = persistentStorage.getInt(KEY_STREAK, 0);
    }

    public int getLongestStreak() {
        return longestStreak;
    }

    public int getHighScore() {
        return highScore;
    }

    public void onEvent(Game.OnScoreChanged evt) {
        if(evt.getScore() > highScore) {
            highScore = evt.getScore();
            persistentStorage.putInt(KEY_SCORE, evt.getScore());
        }
    }

    public void onEvent(Game.OnBestStreak evt) {
        if (evt.getStreak() > longestStreak) {
            longestStreak = evt.getStreak();
            persistentStorage.putInt(KEY_STREAK, evt.getStreak());
        }
    }

}
