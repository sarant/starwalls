package com.exaeone.starwalls.game;

import java.util.Random;

/**
 * Created by saran on 12/02/15.
 */
public class GameConfiguration {

    /**
     * Game speed, expressed as units of z moved per millisecond
     */
    public int speed = 1;

    /**
     * Game stage width
     */
    public int width;

    /**
     * Game stage height
     */
    public int height;

    /**
     * Spaceship choice
     */
    public Spaceship ship = new SpaceshipOne();

    /**
     * Automatically determine spaceship size based upon screen size
     */
    public boolean autoSizePlayer = true;

    /**
     * Player width
     */
    public int playerWidth = 0;

    /**
     * Player height
     */
    public int playerHeight = 0;

    /**
     * z coordinate of the first wall
     */
    public int firstWallZ = 2500;

    /**
     * Minimum gap between walls
     */
    public int minWallGap = 800;

    /**
     * Maximum gap between walls
     */
    public int maxWallGap = 1300;

    /**
     * Wall further than this distance away will not be rendered
     */
    public int wallClipZ = 5500;

    /**
     * Adaptive hole size as a function of game time
     */
    public boolean adaptiveHoleSize = true;
    public float holeTimescale = 90000;

    /**
     * Nonlinearity in hole resize timing, expressed in terms of a quatratic Bezier control point
     */
    public float holeTimeCtrlX = 0.2f;
    public float holeTimeCtrlY = 0.85f;

    /**
     * Size of holes at the start of the game, expressed as multiples of the ship's size
     */
    public float startHoleWidth = 1.75f;
    public float startHoleHeight = 2f;

    /**
     * Size of holes at maximum difficulty, expressed as multiples of the ship's width
     */
    public float endHoleWidth = 1.15f;
    public float endHoleHeight = 1.35f;

    /**
     * Minimum hole size, expressed as multiples of the ship's size
     * These values are OVERRIDDEN if adaptiveHoleSize = true
     */
    public float minHoleWidth = 0f;
    public float minHoleHeight = 0f;

    /**
     * Maximum hole size, expressed as multiples of the ship's size
     * These values are OVERRIDDEN if adaptiveHoleSize = true
     */
    public float maxHoleWidth = 0f;
    public float maxHoleHeight = 0f;

    /**
     * Number of holes per wall
     */
    public int holesPerWall = 1;

    /**
     * Random number source
     */
    public Random random = new Random(System.currentTimeMillis());

}
