package com.exaeone.starwalls.game;

import android.opengl.GLES20;
import android.util.Log;
import com.exaeone.starwalls.R;
import com.exaeone.starwalls.app.App;

import java.io.*;
import java.nio.FloatBuffer;

/**
 * Created by k8iful on 22/02/15.
 */
public class GameRenderer {

    private int glProgram = 0;

    // VBOs

    private int glWallsBufIdx;
    private int glWallsBufCount;

    private int glStarsBufIdx;
    private int glStarsBufCount;

    // Uniforms and attribs

    private int glRenderZIdx;
    private int glPosIdx;

	private static final int COORDS_PER_VERTEX = 4;
	private static final int BYTES_PER_FLOAT = 4;     // = 4 for GL_FLOAT TYPE

	public void updateWallsBuffer(FloatBuffer vertices){
		updateVertexBuffer(glWallsBufIdx, vertices);
        glWallsBufCount = vertices.remaining() / COORDS_PER_VERTEX;
	}

    public void updateStarsBuffer(FloatBuffer vertices){
        updateVertexBuffer(glStarsBufIdx, vertices);
        glStarsBufCount = vertices.remaining() / COORDS_PER_VERTEX;
    }

	public void init(GameConfiguration config) {
        glProgram = initGLProgram();

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        assertGLError("glClearColor");
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        assertGLError("glDisable (depth test)");
        GLES20.glEnable(GLES20.GL_BLEND);
        assertGLError("glEnable (blend)");
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        assertGLError("glBlendFunc");

        GLES20.glUniform1f(getUniformLocation("viewportDist"), 1000f);
        assertGLError("glUniform1f");
        GLES20.glUniform3f(getUniformLocation("limit"), config.width / 2, config.height / 2, config.wallClipZ);
        assertGLError("glUniform3f");

        glRenderZIdx = getUniformLocation("renderZ");
        glPosIdx = getAttribLocation("pos");
        GLES20.glEnableVertexAttribArray(glPosIdx);
        assertGLError("glEnableVertexAttribArray");

        int buffers[] = new int[2];
        GLES20.glGenBuffers(2, buffers, 0);
        assertGLError("glGenBuffers");

        glWallsBufIdx = buffers[0];
        glStarsBufIdx = buffers[1];
	}

    public void render(long renderZ) { //renderZ = 0 at screen and 1.0 at far clip plane
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        assertGLError("glClear");

        GLES20.glUniform1f(glRenderZIdx, (float) renderZ);
        assertGLError("glUniform1f");

        // DRAW STARS

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, glStarsBufIdx);
        assertGLError("glBindBuffer");

        GLES20.glVertexAttribPointer(glPosIdx, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0);
        assertGLError("glVertexAttribPointer");

        GLES20.glDrawArrays(GLES20.GL_POINTS, 0, glStarsBufCount);
        assertGLError("glDrawArrays");

        // DRAW WALLS

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, glWallsBufIdx);
        assertGLError("glBindBuffer");

        GLES20.glVertexAttribPointer(glPosIdx, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, 0);
        assertGLError("glVertexAttribPointer");

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, glWallsBufCount);
        assertGLError("glDrawArrays");

        // UNBIND BUFFER

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        assertGLError("glBindBuffer");
    }

    private int getUniformLocation(String name) {
        int loc = GLES20.glGetUniformLocation(glProgram, name);
        assertGLError("glGetUniformLocation");
        if (loc == -1) {
            throw new RuntimeException("Cannot find uniform '" + name + "'");
        }
        return loc;
    }

    private int getAttribLocation(String name) {
        int loc = GLES20.glGetAttribLocation(glProgram, name);
        assertGLError("glGetAttribLocation");
        if (loc == -1) {
            throw new RuntimeException("Cannot find attribute '" + name + "'");
        }
        return loc;
    }

    private void updateVertexBuffer(int idx, FloatBuffer data) {
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, idx);
        assertGLError("glBindBuffer");

        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, data.remaining() * BYTES_PER_FLOAT, data, GLES20.GL_DYNAMIC_DRAW);
        assertGLError("glBufferData");

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        assertGLError("glBindBuffer");
    }

    private String loadShaderCode(int resourceID) {
        try {
            InputStream inputStream = App.getContext().getResources().openRawResource(resourceID);
            int fileLength = (int) inputStream.available();
            byte[] data = new byte[fileLength];

            inputStream.read(data);

            inputStream.close();

            return new String(data);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private int compileShader(int shaderType, String source) {
        if (source != null) {
            int shader = GLES20.glCreateShader(shaderType);
            assertGLError("glCreateShader");
            GLES20.glShaderSource(shader, source);
            assertGLError("glShaderSource");
            GLES20.glCompileShader(shader);
            assertGLError("glCompileShader");

            int[] compiled = new int[1];
            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
            assertGLError("glGetShaderiv");

            if (compiled[0] == GLES20.GL_FALSE) {
                String log = GLES20.glGetShaderInfoLog(shader);
                GLES20.glDeleteShader(shader);

                if (shaderType == GLES20.GL_VERTEX_SHADER) {
                    Log.e("GLES20", log);
                    throw new RuntimeException("Error compiling vertex shader");
                } else if (shaderType == GLES20.GL_FRAGMENT_SHADER) {
                    Log.e("GLES20", log);
                    throw new RuntimeException("Error compiling fragment shader");
                }
            }

            return shader;

        }
        else {
            throw new NullPointerException();
        }
    }

    private int initGLProgram() {
        int program = GLES20.glCreateProgram();
        assertGLError("glCreateProgram");

        int vertexShader = compileShader(GLES20.GL_VERTEX_SHADER, loadShaderCode(R.raw.vertex_shader));
        int fragmentShader = compileShader(GLES20.GL_FRAGMENT_SHADER, loadShaderCode(R.raw.fragment_shader));

        try {
            GLES20.glAttachShader(program, vertexShader);
            assertGLError("glAttachShader (vertex)");
            GLES20.glAttachShader(program, fragmentShader);
            assertGLError("glAttachShader (fragment)");
            GLES20.glLinkProgram(program);
            assertGLError("glLinkProgram");
            GLES20.glUseProgram(program);
            assertGLError("glUseProgram");

            return program;
        }
        catch (Exception e) {
            if (vertexShader != 0) {
                GLES20.glDeleteShader(vertexShader);
            }

            if (fragmentShader != 0) {
                GLES20.glDeleteShader(fragmentShader);
            }

            if (program != 0) {
                GLES20.glDeleteProgram(program);
            }

            throw new RuntimeException(e);
        }
    }

	private static void assertGLError(String func) {
		int error =  GLES20.glGetError();
		if (error == GLES20.GL_INVALID_ENUM) {
			throw new RuntimeException("GL_INVALID_ENUM in " + func);
		}
        else if (error == GLES20.GL_INVALID_VALUE) {
            throw new RuntimeException("GL_INVALID_VALUE in " + func);
        }
        else if (error == GLES20.GL_INVALID_OPERATION) {
            throw new RuntimeException("GL_INVALID_OPERATION in " + func);
        }
        else if (error == GLES20.GL_INVALID_FRAMEBUFFER_OPERATION) {
            throw new RuntimeException("GL_INVALID_FRAMEBUFFER_OPERATION in " + func);
        }
        else if (error == GLES20.GL_OUT_OF_MEMORY) {
            throw new RuntimeException("GL_OUT_OF_MEMORY in " + func);
        }
	}

    private static void flushGLError(String func) {
        int error = GLES20.glGetError();
        if (error == GLES20.GL_INVALID_ENUM) {
            Log.w("GLES20", "GL_INVALID_ENUM in " + func);
        }
        else if (error == GLES20.GL_INVALID_VALUE) {
            Log.w("GLES20", "GL_INVALID_VALUE in " + func);
        }
        else if (error == GLES20.GL_INVALID_OPERATION) {
            Log.w("GLES20", "GL_INVALID_OPERATION in " + func);
        }
        else if (error == GLES20.GL_INVALID_FRAMEBUFFER_OPERATION) {
            Log.w("GLES20", "GL_INVALID_FRAMEBUFFER_OPERATION in " + func);
        }
        else if (error == GLES20.GL_OUT_OF_MEMORY) {
            Log.w("GLES20", "GL_OUT_OF_MEMORY in " + func);
        }
    }

}