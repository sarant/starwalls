package com.exaeone.starwalls.game;

import android.graphics.Path;
import android.util.Log;
import com.exaeone.starwalls.pool.DefaultPool;
import com.exaeone.starwalls.pool.Poolable;
import org.poly2tri.Poly2Tri;
import org.poly2tri.geometry.polygon.Polygon;
import org.poly2tri.geometry.polygon.PolygonPoint;
import org.poly2tri.triangulation.TriangulationPoint;
import org.poly2tri.triangulation.delaunay.DelaunayTriangle;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by saran on 11/02/15.
 */
public class Wall extends Poolable {

    /*package*/ int width;
    /*package*/ int height;
    /*package*/ long z;

    /*package*/ final List<Hole> holes = new ArrayList<>();

    /*package*/ Polygon polygon = null;
    /*package*/ final Path path = new Path();
    /*package*/ final Path holesPath = new Path();

    @Override
    public void onRecycle() {
        for (Hole hole : holes) {
            hole.release();
        }

        holes.clear();

        super.onRecycle();
    }

    public Wall configure(long z, GameConfiguration config) {
        boolean success = false;

        while (!success) {
            holes.clear();

            this.width = config.width;
            this.height = config.height;
            this.z = z;

            this.clearPoints();

            for (int i = 0; i < config.holesPerWall; ++i) {
                this.addHole(DefaultPool.obtain(Hole.class).randomize(this, config));
            }

            try {
                Poly2Tri.triangulate(polygon);
                success = true;
            } catch (RuntimeException e) {
                // TODO: HACK! Figure out what's wrong with the maths so that the hole never clips the wall!
                Log.e("Triangulator", e.getMessage());
            }
        }

        return this;
    }

    public void writeToVertexBuffer(FloatBuffer vertexBuffer) {

        List<DelaunayTriangle> triangles = polygon.getTriangles();
        for (DelaunayTriangle triangle : triangles) {
            for (TriangulationPoint point : triangle.points) {
                vertexBuffer.put(point.getXf());
                vertexBuffer.put(point.getYf());
                vertexBuffer.put((float) z);
                vertexBuffer.put(0);    // point size, unused for walls
            }
        }
    }

    private void clearPoints() {

        polygon = new Polygon(new PolygonPoint[] {
            new PolygonPoint(-width / 2, -height / 2),
            new PolygonPoint(width / 2, -height / 2),
            new PolygonPoint(width / 2, height / 2),
            new PolygonPoint(-width / 2, height / 2)
        });


        path.reset();
        path.addRect(-width / 2, -height / 2, width / 2, height / 2, Path.Direction.CCW);

        holesPath.reset();
    }

    private void addHole(Hole hole) {
        holes.add(hole);
        polygon.addHole(hole.getPolygon());
        path.addPath(hole.getPath());
        holesPath.addPath(hole.getPath());
    }

}
