package com.google.android.gms.cast;

import com.google.android.gms.common.images.WebImage;

import java.util.List;

/**
 * Created by saran on 18/03/15.
 */
public abstract class CastDeviceFactory {
    public static CastDevice create(int versionCode, String deviceId, String hostAddress, String friendlyName, String modelName, String deviceVersion, int servicePort, List<WebImage> icons, int capabilities, int status) {
        return new CastDevice(versionCode, deviceId, hostAddress, friendlyName, modelName, deviceVersion, servicePort, icons, capabilities, status);
    }
}
