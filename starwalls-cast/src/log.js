var LogLevel = {
	DEBUG: 5,
	VERBOSE: 4,
	INFO: 3,
	WARN: 2,
	ERROR: 1
}

var Config = {
	loglevel: LogLevel.VERBOSE
}

var GameLog = (function () {

	var Public = {};

	var loglevel = function () {
		if (typeof Config === 'undefined' || typeof Config.loglevel !== 'number') {
			return LogLevel.INFO;
		}
		else {
			return Config.loglevel;
		}
	}

	Public.debug = function (tag, msg) {
		if (console && loglevel() >= LogLevel.DEBUG) {
			console.debug(tag, '::', msg);
		}
	}

	Public.verbose = function (tag, msg) {
		if (console && loglevel() >= LogLevel.VERBOSE) {
			console.log(tag, '::', msg);
		}
	}

	Public.info = function (tag, msg) {
		if (console && loglevel() >= LogLevel.INFO) {
			console.info(tag, '::', msg);
		}
	}

	Public.warn = function (tag, msg) {
		if (console && loglevel() >= LogLevel.WARN) {
			console.warn(tag, '::', msg);
		}
	}

	Public.error = function (tag, msg) {
		if (console && loglevel() >= LogLevel.ERROR) {
			console.error(tag, '::', msg);
		}
	}

	return Public;

})();
