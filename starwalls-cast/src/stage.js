var GameStage = function () {

    var Public = {};

    var TAG = 'GameStage';

    var proto = Object.create(HTMLDivElement.prototype);

    var dom = {};
    var curtains = {};
    Public.curtains = curtains;

    proto.createdCallback = function () {
        // Retrieve DOM elements
        dom.stage = this;
        dom.curtainHolder = this.querySelector('#curtain');
        dom.hud = this.querySelector('#hud');
        dom.spaceship = this.querySelector('canvas#spaceship');

        dom.video = this.querySelector('video#video');
        Public.video = dom.video;
        Public.video.src = GameVideoStream.url;
        Public.video.addEventListener('error', function (e) { GameLog.error('VideoError', e); }, false);
        
        // load curtains
        var curtain_templates = this.querySelectorAll('template.curtain');
        for (i = 0; i < curtain_templates.length; ++i) {
            var curtain = document.createElement('curtain-' + curtain_templates[i].id);
            curtain.id = curtain_templates[i].id;
            curtain.className = 'curtain fullscreen opaque';
            curtain.appendChild(document.importNode(curtain_templates[i].content, true));
            curtain.addEventListener("webkitAnimationEnd", curtainTransitionEnd, false);
            curtains[curtain.id] = curtain;
        }
        dom.curtainHolder.addEventListener("webkitAnimationEnd", curtainTransitionEnd, false);
        
        Public.displayCurtain('splash');
        //GameBootstrap.videoSize(dom.video.offsetWidth, dom.video.offsetHeight);
        GameBootstrap.videoSize(window.innerWidth, window.innerHeight);
    }

    Public.setSplash = function (gfxName) {
        var splashImage = curtains['splash'].querySelector('img');
        GameGraphics.get(gfxName, function (gfx) {
            splashImage.src = gfx.src;
        })
    }

    var isVideoPositioned = false;
    
    var positionVideo = function () {
        if (isVideoPositioned) {
            return;
        }
        else {
            isVideoPositioned = true;
        }

        var videoBox = dom.video.getBoundingClientRect();

        dom.video.width = 1280;
        dom.video.height = 720;

        dom.video.style.width = dom.video.width + 'px';
        dom.video.style.height = dom.video.height + 'px';
        dom.video.style.position = 'absolute';
        dom.video.style.left = videoBox.left + 'px';
        dom.video.style.top = videoBox.top + 'px';

        if (videoBox.width != dom.video.width || videoBox.height != dom.video.height) {
            // Need to do this so that the video gets stretched to fill the DOM properly
            dom.video.style.transformOrigin = '0 0';
            dom.video.style.transform = 'scale(' + (videoBox.width / dom.video.width) + ',' + (videoBox.height / dom.video.height) + ')';
        }
        else {
            dom.video.style.transformOrigin = '';
            dom.video.style.transform = '';
        }
    }
    
    Public.reset = function () {
        positionVideo();
        GameHUD.reset();
        if (dom.hit) {
            dom.stage.removeChild(dom.hit);
            URL.revokeObjectURL(dom.hit.src);
            dom.hit = undefined;
        }
    }

    Public.setSpaceship = function (width, height, gfxName) {
        if (GameGraphics.has(gfxName)) {
            var stageBox = dom.stage.getBoundingClientRect();

            dom.spaceship.width = width;
            dom.spaceship.height = height;

            dom.spaceship.style.width = width + 'px';
            dom.spaceship.style.height = height + 'px';
            dom.spaceship.style.position = 'absolute';
            dom.spaceship.style.left = ((stageBox.width - width) / 2) + 'px';
            dom.spaceship.style.top = ((stageBox.height - height) / 2) + 'px';

            GameGraphics.get(gfxName, function (img) {
                var ctx = dom.spaceship.getContext('2d');
                ctx.clearRect(0, 0, width, height);
                ctx.drawImage(img, 0, 0, width, height);
                GameLog.verbose(TAG, 'Finished drawing spaceship into canvas');
            });
        }
    }

    Public.setHit = function (hit) {
        hit.style.position = 'absolute';
        hit.style.left = (dom.stage.offsetWidth / 2 - hit.width) + 'px';
        hit.style.top = (dom.stage.offsetHeight / 2 - hit.height) + 'px';

        if (dom.hit) {
            dom.stage.removeChild(dom.hit);
            URL.revokeObjectURL(dom.hit.src);
            dom.hit = undefined;
        }

        if (hit) {
            dom.hit = hit;
            redrawHit();
            dom.stage.insertBefore(hit, dom.spaceship);
        }
    }

    Public.redraw = function () {
        dom.spaceship.style.transform = 'translate(' + Game.x + 'px,' + Game.y + 'px) rotate(' + Game.roll + 'rad)';
        redrawHit();
        GameHUD.redraw();
    }

    var redrawHit = function () {
        if (dom.hit) {
            var scale = 1 / (1 + (dom.hit.hitTime - Math.floor(25 * dom.video.currentTime) / 25));
            if (scale < 4) {
                var alpha = 1 / (1 + (scale - 1) * (scale - 1));
                alpha = (alpha * alpha) * (alpha * alpha);
                dom.hit.style.opacity = alpha;
                dom.hit.style.transform = 'scale(' + scale +') translate(' + dom.hit.hitX + 'px, ' + dom.hit.hitY +'px)';
            }
            else {
                try {
                    dom.stage.removeChild(dom.hit);
                }
                catch (err) {
                    GameLog.warn(TAG, 'hit is not currently a child of stage, ignoring...');
                }
                URL.revokeObjectURL(dom.hit.src);
                dom.hit = undefined;
            }
        }
    }

    Public.displayCurtain = function (curtainName) {
        displayCurtainHolder();
        var curtain = curtains[curtainName];
        curtain.style.webkitAnimation = '';
        dom.curtainHolder.appendChild(curtain);
    }

    Public.fadeInCurtain = function (curtainName) {
        displayCurtainHolder();
        var curtain = curtains[curtainName];
        if (typeof curtain.onDraw == 'function') {
            curtain.onDraw();
        }

        // TODO: FIX THIS HACK!!!
        if (curtainName == 'stuck') {
            curtain.style.webkitAnimation = 'fadeinstuck 500ms linear';
        }
        else {
            curtain.style.webkitAnimation = 'fadein 500ms linear';
        }
        
        dom.curtainHolder.appendChild(curtain);
    }

    Public.fadeOutCurtain = function () {
        curtain.style.webkitAnimation = 'fadeout 500ms linear';
    }

    var curtainTransitionEnd = function (e) {
        if (e.animationName == 'fadein') {
            removeCurtainsUnderneath(e.srcElement);
        }
        else if (e.animationName == 'fadeout') {
            hideCurtainHolder();
        }
    }

    var displayCurtainHolder = function () {
        dom.curtainHolder.style.display = '';
        dom.curtainHolder.style.webkitAnimation = '';
    }

    var hideCurtainHolder = function () {
        dom.curtainHolder.style.display = 'none';
        while (dom.curtainHolder.firstChild)  {
            dom.curtainHolder.removeChild(dom.curtainHolder.firstChild);
        }
    }

    var removeCurtainsUnderneath = function (curtain) {
        while (dom.curtainHolder.firstChild !== curtain)  {
            dom.curtainHolder.removeChild(dom.curtainHolder.firstChild);
        }
    }

    document.registerElement('sw-stage', {
        prototype: proto
    });

    return Public;

}();
