var GameBootstrap = function () {

    var Public = {};

    var TAG = "GameBootstrap";

    var hasWsAddress = false;
    var wsAddress = '';

    var hasGameSocketLoaded = false;

    var hasServerHello = false;

    var hasMediaSourceReady = false;
    var streamSerialNumber = 0;
    var sentMediaSourceReady = false;

    var hasVideoSize =  false;
    var sentVideoSize = false;
    var videoWidth = 0;
    var videoHeight = 0;

    var hasWindowOnLoad = false;
    var sentWindowOnload = false;

    var checkAndConnect = function () {
        if (hasGameSocketLoaded && hasWsAddress) {
            GameSocket.open(wsAddress);
            //GameSocket.open(wsAddress);
        }
    }

    Public.setWsAddress = function (addr) {
        hasWsAddress = true;
        wsAddress = addr;
        GameLog.verbose(TAG, 'CastReady ' + addr);
        checkAndConnect();
    }

    Public.gameSocketLoaded = function () {
        hasGameSocketLoaded = true;
        GameLog.verbose(TAG, 'GameSocketLoaded');
        checkAndConnect();
    }

    var checkAndSend = function () {
        if (hasServerHello) {
            if (hasMediaSourceReady && !sentMediaSourceReady) {
                sentMediaSourceReady = true;
                GameLog.verbose(TAG, 'Sent MediaSourceReady');
                GameSocket.send(GameOutbound.mediaSourceReady(streamSerialNumber));
            }

            if (hasVideoSize && !sentVideoSize) {
                sentVideoSize = true;
                GameLog.verbose(TAG, 'Sent VideoSize(' + videoWidth + ',' + videoHeight + ')');
                GameSocket.send(GameOutbound.videoSize(videoWidth, videoHeight));
            }

            if (hasWindowOnLoad && !sentWindowOnload) {
                sentWindowOnload = true;
                GameLog.verbose(TAG, 'Sent WindowOnLoad');
                GameSocket.send(GameOutbound.windowOnLoad());
            }
        }
    }

    Public.serverHello = function () {
        GameLog.info(TAG, 'ServerHello');
        hasServerHello = true;
        GameSocket.open(wsAddress);
        checkAndSend();
    }

    Public.mediaSourceReady = function (serialNumber) {
        hasMediaSourceReady = true;
        sentMediaSourceReady = false;
        streamSerialNumber = serialNumber;
        checkAndSend();
    }

    Public.videoSize = function (width, height) {
        hasVideoSize = true;
        videoWidth = width;
        videoHeight = height;
        checkAndSend();
    }

    window.addEventListener('load', function (e) {
        hasWindowOnLoad = true;
        checkAndSend();
    });

    return Public;

}();
