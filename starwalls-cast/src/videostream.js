var GameVideoStream = (function () {

    var TAG = 'GameVideoStream'
	var Public = {};

	var mediaSource = new MediaSource();
    Public.url = URL.createObjectURL(mediaSource);

    Public.streamSerialNumber = 0;

	var sourceBuffer = null;	
	var available = 0;

    var pendingData = new Uint8Array(512 * 1024);
    var pendingSize = 0;
    var updating = false;

    var addSourceBuffer = function () {
        sourceBuffer = mediaSource.addSourceBuffer('video/mp4; codecs="avc1.428020"');
        sourceBuffer.mode = 'segments';
        sourceBuffer.addEventListener('updateend', onUpdateEnd, false);
        GameLog.info(TAG, 'MediaSource ready: serial number ' + Public.streamSerialNumber);
        GameBootstrap.mediaSourceReady(Public.streamSerialNumber);
    }

    mediaSource.addEventListener('sourceopen', addSourceBuffer, false);

	Public.clear = function () {
        Public.streamSerialNumber += 1;
        sourceBuffer.removeEventListener('updateend', onUpdateEnd);
        sourceBuffer = null;
        available = 0;
        pendingSize = 0;
        updating = false;
        
        GameStage.video.pause();
		GameStage.video.load();
    }

    Public.available = function () {
        return available;
    }

	var head = null;
	var tail = null;

	Public.append = function (data, fragmentStreamSerialNumber) {
        if (Public.streamSerialNumber != fragmentStreamSerialNumber) {
            return;
        }

		var timecode = data.getInt32(72);

        if (!updating) {
            updating = true;
            sourceBuffer.appendBuffer(data);
            pendingSize = 0;
        }
        else {
            if (pendingData.byteLength - pendingSize < data.byteLength) {
                var grownData = new Uint8Array(2 * pendingData.byteLength);
                grownData.set(pendingData);
                pendingData = grownData;
                grownData = undefined;
            }

            pendingData.set(new Uint8Array(data.buffer, data.byteOffset, data.byteLength), pendingSize);
            pendingSize += data.byteLength;
        }
	}

	var onUpdateEnd = function (e) {
        if (sourceBuffer.buffered.length > 0) {
            available = sourceBuffer.buffered.end(0);
            GameSocket.send(GameOutbound.videoBufferLevel(Math.round(1000 * available)));
        }
		
        if (pendingSize > 0) {
            sourceBuffer.appendBuffer(new DataView(pendingData.buffer, 0, pendingSize));
            pendingSize = 0;
        }
        else {
            updating = false;
        }
	}

	return Public;

})();