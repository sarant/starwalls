var GameCast = function () {
    var Public = {};

    cast.receiver.logger.setLevelValue(cast.receiver.LoggerLevel.DEBUG);
    Public.manager = cast.receiver.CastReceiverManager.getInstance();
    Public.messageBus = Public.manager.getCastMessageBus('urn:x-cast:com.exaeone.starwalls', cast.receiver.CastMessageBus.MessageType.STRING);

    Public.messageBus.onMessage = function (e) {
        GameBootstrap.setWsAddress(e.data);
    }

    Public.manager.start();

    return Public;
}();
