var GameSocket = (function () {

	var Public = {};

	var TAG = 'GameSocket';

	var sockets = [];

	Public.open = function (url) {
		var ws = new WebSocket(url, 'starwalls');
		ws.onopen = function () { onWsOpen(ws); };
		ws.onclose = function () { onWsClose(ws); };
		ws.onmessage = onWsMessage;
		ws.binaryType = 'arraybuffer';
	}

	Public.send = function (buf) {
		if (sockets.length > 0) {
			sockets[0].send(buf);
		}
	}

	var onWsOpen = function (ws) {
		sockets.push(ws);
        ws.send(GameOutbound.clientHello());
		GameLog.info(TAG, 'WebSocket[' + (sockets.length - 1) + '] opened');
	}

	var onWsClose = function (ws) {
		var index = sockets.indexOf(ws);
		if (index >= 0) {
			sockets.splice(index, 1);
			GameLog.info(TAG, 'WebSocket[' + index + '] closed');
		}
		if (sockets.length == 0) {
			GameStage.fadeInCurtain('disconnect');
			window.setTimeout(function () {
				GameCast.manager.stop();
			}, 5000);
		}
	}

	var onWsMessage = function (e) {
		GameInbound.handle(e.data);
	}

	return Public;

})();

GameBootstrap.gameSocketLoaded();
