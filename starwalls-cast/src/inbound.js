/* Deserialises and dispatches inbound messages */
var GameInbound = (function () {

	var Public = {};

	var TAG = 'GameInbound';

	Public.handle = function (buf) {
		var cmd = new DataView(buf).getUint16(0);
		var cmd_handler = command[cmd];
		var data = new DataView(buf, 2);

		if (cmd_handler) {
			cmd_handler(data);
		}
		else {
			GameLog.warn(TAG, 'Unknown protocol command: ' + cmd);
		}
	}

	var serverHello = function (data) {
		GameBootstrap.serverHello();
	}

	var setSpaceship = function (data) {
		var width = data.getUint16(0);
		var height = data.getUint16(2);
		var nameLength = data.getUint16(4);
		var nameView = new DataView(data.buffer, 6 + data.byteOffset, nameLength);
		var gfxName = new TextDecoder('utf-8').decode(nameView)
		Game.setSpaceship(width, height, gfxName);
	}

	var graphicsData = function (data) {
		var nameLength = data.getUint16(0);
		var nameView = new DataView(data.buffer, 2 + data.byteOffset, nameLength);
		var name = new TextDecoder('utf-8').decode(nameView);

		var blob = new Blob([new DataView(data.buffer, 2 + nameLength + data.byteOffset)], {type: 'image/png'});
		var gfx = new Image();
		gfx.src = URL.createObjectURL(blob);

		GameGraphics.set(name, gfx);
	}

	var setSplash = function (data) {
		var nameLength = data.getUint16(0);
		var nameView = new DataView(data.buffer, 2 + data.byteOffset, nameLength);
		var name = new TextDecoder('utf-8').decode(nameView);
		GameStage.setSplash(name);
	}

	var playerMove = function (data) {
		Game.setPlayerPosition(data.getFloat32(0), data.getFloat32(4), data.getFloat32(8));
	}

	var gameStart = function (data) {
	    Game.reset();
		Game.start();
	}

	var gameStop = function (data) {
		Game.score = data.getInt32(0);
		Game.bestStreak = data.getInt32(4);

		var nameLength = data.getUint16(8);
		var nameView = new DataView(data.buffer, 10 + data.byteOffset, nameLength);
		Game.endGameGfx = new TextDecoder('utf-8').decode(nameView);

    	Game.stop();
	}

	var updateShield = function (data) {
		var shield = data.getFloat32(0);
		Game.setShield(shield);

        if (data.byteLength > 4) {
			var hitX = data.getInt32(4);
			var hitY = data.getInt32(8);
			var hitTime = data.getUint32(12);

			if (data.byteLength > 16) {
				var hitWidth = data.getInt32(16);
				var hitHeight = data.getInt32(20);

				var blob = new Blob([new DataView(data.buffer, 24 + data.byteOffset)], {type: 'image/png'});

				var hit = new Image();
				hit.src = URL.createObjectURL(blob);
				hit.width = hitWidth;
				hit.height = hitHeight;
				hit.style.width = (2 * hit.width) + 'px';
        		hit.style.height = (2 * hit.height) + 'px';

        		// metadata
				hit.hitX = hitX;
				hit.hitY = hitY;
				hit.hitTime = hitTime / 1000;

				hit.onload = function () {
					Game.setHit(hit);
				}
			}
		}
	}

	var updateScore = function (data) {
		var score = data.getInt32(0);
    	Game.setScore(score);
	}

	var gameText = function(data){
		var duration = data.getUint16(0)
		var length = data.getUint16(2);
		var textView = new DataView(data.buffer, 4 + data.byteOffset, length);
		var text = new TextDecoder('utf-8').decode(textView);

		if (duration > 0) {
			GameHUD.setForegroundMessage(text, duration);
		}
		else {
			GameHUD.setBackgroundMessage(text);
		}
	}

	var clearVideoBuffer = function (data) {
		GameVideoStream.clear();
	}

	var videoFragment = function (data) {
		var streamSerialNumber = data.getInt32(0);
		GameVideoStream.append(new DataView(data.buffer, 4 + data.byteOffset), streamSerialNumber);
	}

	var command = {
		// 1xxx: host-to-remote commands

        // 10xx: game setup
		1000: serverHello,
		1001: setSpaceship,
		1002: graphicsData,
		1003: setSplash,

		// 11xx: game control
		1100: gameStart,
		1101: gameStop,
		1102: playerMove,
		1103: updateShield,
		1104: updateScore,
		1105: gameText,

		// 12xx: video stream control
		1200: clearVideoBuffer,
		1201: videoFragment
		
	};

	return Public;

})();