var Game = function () {
    
    var Public = {};

    var TAG = 'Game';

    Public.x = 0;
    Public.y = 0;
    Public.roll = 0;
    Public.shield = 1;
    Public.score = 0;
    Public.bestStreak = 0;
    Public.endGameGfx = '';

    var lastUpdate = 0;

    var latencyList = new Array(30);

    var clearLatency = function () {
        for (i = 0; i < latencyList.length; ++i) {
            latencyList[i] = 0;
        }
    }

    var updateLatency = function (latency) {
        for (i = 0; i < latencyList.length - 1; ++i) {
            latencyList[i] = latencyList[i + 1];
        }
        latencyList[latencyList.length - 1] = latency;
    }

    var maxLatency = function () {
        if (lastUpdate < 0) {
            return 0;
        }
        else {
            return Math.max(performance.now() - lastUpdate, Math.max.apply(null, latencyList));
        }
    }

    var getNetworkHealth = function (threshold) {
        var lastOverThreshold;

        if ((performance.now() - lastUpdate >= threshold)) {
            lastOverThreshold = latencyList.length ;
        }
        else {
            lastOverThreshold = 0;
            for (i = 0; i < latencyList.length; ++i) {
                if (latencyList[i] > threshold) {
                    lastOverThreshold = i;
                }
            }
        }

        return (latencyList.length + 1 - lastOverThreshold) / (latencyList.length + 2);
    }

    Public.reset = function () {
        Public.x = 0;
        Public.y = 0;
        Public.roll = 0;
        Public.shield = 1;
        Public.score = 0;
        Public.bestStreak = 0;

        lastUpdate = -1;
        clearLatency();

        GameStage.reset();
    }

    Public.setSpaceship = function (width, height, gfxName) {
        GameStage.setSpaceship(width, height, gfxName);
    }

    Public.setPlayerPosition = function (x, y, roll) {
        Public.x = x;
        Public.y = y;
        Public.roll = roll;

        var now = performance.now();
        if (lastUpdate > 0) {
            updateLatency(now - lastUpdate);
        }
        lastUpdate = now;

        GameLog.debug(TAG, 'Player position: (' + x + ', ' + y + ', ' + roll + ')');
    }

    Public.setShield = function (shield, hit) {
        Public.shield = shield;
    }

    Public.setHit = function (hit) {
        GameStage.setHit(hit);
    }

    Public.setScore = function (score) {
        Public.score = score;
    }

    var playing = false;

    Public.start = function () {
        GameLog.info(TAG, 'Game starting');
        GameStage.video.play();
        playing = true;
        window.requestAnimationFrame(refreshStage);
        GameStage.fadeOutCurtain();
    }

    Public.stop = function () {
        GameLog.info(TAG, 'Game stopping');
        playing = false;
        GameStage.video.pause();
        GameStage.fadeInCurtain('gameover');
    }

    var refreshStage = function () {
        if (playing) {
            var bufferRemaining = GameVideoStream.available() - video.currentTime;

            var pauseThreshold = 300;
            var restartThreshold = 200;

            if (!video.paused && maxLatency() > pauseThreshold) {
                GameStage.fadeInCurtain('stuck');
                GameStage.video.pause();
            }
            else if (video.paused && maxLatency() < restartThreshold) {
                GameStage.fadeOutCurtain();
                GameStage.video.play();
            }

            if (!GameStage.video.paused) {
                GameStage.redraw();
                GameSocket.send(GameOutbound.gameSync(Math.round(video.currentTime * 1000), Public.x, Public.y, Public.roll));
            }
            else {
                var networkBar = document.querySelector('#network');
                var networkHealth = getNetworkHealth(restartThreshold);

                networkBar.style.width = Math.round(networkHealth * 100) + '%';
                networkBar.style.backgroundColor = 'rgba(' + Math.round(255 * (1 - networkHealth)) + ',' + Math.round(255 * networkHealth) + ',0,1)';
            }

            window.requestAnimationFrame(refreshStage);
        }
    }

    return Public;

}();
