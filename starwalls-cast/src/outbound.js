var GameOutbound = (function () {

	var Public = {};

    Public.minProtocolVersion = 1;

    Public.clientHello = function () {
        var buf = new ArrayBuffer(6);
        var view = new DataView(buf);
        view.setUint16(0, command[Public.clientHello]);
		view.setInt32(2, Public.minProtocolVersion);
        return buf;
    }

    Public.windowOnLoad = function () {
        var buf = new ArrayBuffer(2);
        var view = new DataView(buf);
        view.setUint16(0, command[Public.windowOnLoad]);
        return buf;
    }

	Public.videoSize = function (width, height) {
		var buf = new ArrayBuffer(6);
		var view = new DataView(buf);
		view.setUint16(0, command[Public.videoSize]);
		view.setUint16(2, width);
		view.setUint16(4, height);
		return buf;
	}

	Public.gameSync = function (time, x, y, roll) {
		var buf = new ArrayBuffer(18);
		var view = new DataView(buf);
		view.setUint16(0, command[Public.gameSync]);
		view.setUint32(2, time);
		view.setFloat32(6, x);
		view.setFloat32(10, y);
		view.setFloat32(14, roll);
		return buf;
	}

    Public.mediaSourceReady = function (streamSerialNumber) {
        var buf = new ArrayBuffer(6);
        var view = new DataView(buf);
        view.setUint16(0, command[Public.mediaSourceReady]);
        view.setUint32(2, streamSerialNumber);
        return buf;
    }

    Public.videoBufferLevel = function (bufferLevel) {
        var buf = new ArrayBuffer(6);
        var view = new DataView(buf);
        view.setUint16(0, command[Public.videoBufferLevel]);
        view.setUint32(2, bufferLevel);
        return buf;
    }

	var command = {};

	// 2xxx: remote-to-host commands

    // 20xx: game setup
    command[Public.clientHello] = 2000;
	command[Public.videoSize] = 2001;
    command[Public.windowOnLoad] = 2002;

	// 21xx: game control
	command[Public.gameSync] = 2100;

    // 22xx: video control
    command[Public.mediaSourceReady] = 2200;
    command[Public.videoBufferLevel] = 2201;

	return Public;

})();