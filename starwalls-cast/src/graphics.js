var GameGraphics = (function () {

	var Public = {};

	var TAG = 'GameGraphics';

	var graphics = {};

	Public.set = function (name, data) {
		graphics[name] = data;
		GameLog.verbose(TAG, 'Received graphics data \'' + name + '\'');
	}

	Public.get = function (name, callback) {
		var gfx = graphics[name];

		if (typeof gfx !== 'undefined' && typeof callback !== 'undefined') {
			if (gfx.complete) {
				callback(gfx);
			}
			else {
				gfx.onload = function () {
					callback(gfx)
				};
			}
		}

		if (typeof gfx === 'undefined') {
			GameLog.warn(TAG, 'Undefined graphics \'' + name + '\'');
		}

		return gfx;
	}

	Public.has = function (name) {
		return (typeof graphics[name] !== 'undefined');
	}

	return Public;

})();
