var cast = {};

cast.receiver = {};
cast.receiver.logger = {};

cast.receiver.logger.setLevelValue = function (level) {};

cast.receiver.LoggerLevel = {};
cast.receiver.LoggerLevel.DEBUG;

cast.receiver.CastReceiverManager = {};
cast.receiver.CastReceiverManager.getInstance = function () {};
cast.receiver.CastReceiverManager.prototype.getCastMessageBus = function (namespace, type) {};
cast.receiver.CastReceiverManager.prototype.onMessage = function (e) {};

cast.receiver.CastMessageBus = {};
cast.receiver.MessageType = {};
cast.receiver.MessageType.STRING;
