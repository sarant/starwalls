var SplashCurtain = function () {
    var Public = {};

    var proto = Object.create(HTMLDivElement.prototype);

    proto.createdCallback = function (e) {
        var splashCurtain = this;
        /*window.addEventListener('load', function (e) {
            splashCurtain.querySelector('img').src = 'res/splash.png';
        });*/
    };

    document.registerElement('curtain-splash', {
        prototype: proto
    });

    return Public;
}();

var GameOverCurtain = function () {
    var Public = {};

    var proto = Object.create(HTMLDivElement.prototype);

    proto.onDraw = function () {
        this.querySelector('#highscore').innerHTML = Game.score.toString();
        this.querySelector('#streak').innerHTML = Game.bestStreak.toString();
        this.querySelector('img').src = GameGraphics.get(Game.endGameGfx).src;
    }

    document.registerElement('curtain-gameover', {
        prototype: proto
    });

    return Public;
}();
