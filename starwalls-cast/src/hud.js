var GameHUD = function () {

    var Public = {};

    var proto = Object.create(HTMLDivElement.prototype);

    var dom = {};

    var backgroundMessage;
    var messageStartTime;
    var messageDuration;

    var defaultBackgroundMessage = 'StarWalls';
    var defaultMessageDuration = 500;

    var previousShield;
    var currentShield;
    var deltaStartTime;

    var score = 0;

    // Inflate HUD layout from template
    proto.createdCallback = function () {
        dom.hud = this;
        dom.bottompanel = this.querySelector('#bottompanel');
        dom.message = bottompanel.querySelector('#message');
        dom.shield = bottompanel.querySelector('#shield');
        dom.shieldDelta = bottompanel.querySelector('#delta');
        dom.score = bottompanel.querySelector('#score');
        dom.skin = bottompanel.querySelector('#skin');
        dom.shieldWarn = this.querySelector('#shieldwarn');   
    }
    
    var isBottomPanelPositioned = false;

    var positionBottomPanel = function () {
        if (isBottomPanelPositioned) {
            return;
        }
        else {
            isBottomPanelPositioned = true;
        }

        dom.bottompanel.style.height = (0.084375 * dom.hud.offsetWidth) + 'px';
        dom.message.style.lineHeight = dom.message.offsetHeight + 'px';
        dom.score.style.lineHeight = dom.score.offsetHeight + 'px';
        ShieldWarningOverlay.createInto(dom.shieldWarn);
    }
    
    Public.reset = function () {
        positionBottomPanel();

        backgroundMessage = defaultBackgroundMessage;
        messageStartTime = 0;
        messageDuration = 0;
        redrawMessage();
        
        previousShield = 1.0;
        currentShield = 1.0;
        deltaStartTime = 0;
        redrawShield();
        
        redrawScore();
        
        dom.shieldWarn.style.display = 'none';
        dom.shieldWarn.style.webkitAnimation = '';
    }

    Public.setBackgroundMessage = function (msg) {
        if (typeof msg == 'undefined' || msg == '') {
            backgroundMessage = defaultBackgroundMessage;
        }
        else {
            backgroundMessage = msg;
        }
        redrawMessage();
    }

    Public.setForegroundMessage = function (msg, duration) {
        if (typeof msg == 'undefined' || msg == '') {
            return;
        }
        else {
            dom.message.className = 'foreground';
            dom.message.innerHTML = msg;
            messageStartTime = performance.now();

            if (typeof duration == 'undefined') {
                messageDuration = defaultMessageDuration;
            }
            else {
                messageDuration = duration;
            }
        }
    }

    Public.setShield = function (shield) {
        if (shield != currentShield) {
            previousShield = currentShield;
            currentShield = shield;
            dom.shield.style.width = (100 * currentShield) + '%';
            redrawShield();

            if (shield == 0) {
                dom.shieldWarn.style.display = '';
                dom.shieldWarn.style.webkitAnimation = 'fadein 500ms linear';
            }
        }
    }

    Public.redraw = function () {
        Public.setShield(Game.shield);

        if (messageStartTime > 0) {
            redrawMessage();
        }

        if (deltaStartTime > 0) {
            redrawDelta();
        }

        redrawScore();
    }

    var redrawMessage = function () {
        if (performance.now() - messageStartTime > messageDuration) {
            dom.message.className = 'background';
            dom.message.innerHTML = backgroundMessage;
            messageStartTime = -1;
        }
    }

    var redrawShield = function () {
        dom.shield.style.width = (100 * currentShield) + '%';

        if (currentShield != previousShield) {
            // unhide the div
            dom.shieldDelta.style.display = '';

            // different anchoring behaviour depending on whether we're going up or down
            if (previousShield < currentShield) {
                dom.shieldDelta.style.left = '';
                dom.shieldDelta.style.right = (100 * (1 - currentShield)) + '%';
                dom.shieldDelta.className = 'positive';
            }
            else {
                dom.shieldDelta.style.left = (100 * currentShield) + '%';
                dom.shieldDelta.style.right = '';
                dom.shieldDelta.className = 'negative';
            }

            deltaStartTime = performance.now();
        }
        else {
            hideDelta();
        }
    }

    var redrawDelta = function () {
        var delta = Math.abs(currentShield - previousShield);
        var duration = Math.max(200, 1500 * delta);
        var elapsed = (performance.now() - deltaStartTime) / duration;
        
        if (elapsed < 1) {
            dom.shieldDelta.style.width = (100 * (1 - elapsed) * delta) + '%';
        }
        else {
            hideDelta();
        }
    }

    var hideDelta = function () {
        dom.shieldDelta.style.display = 'none';
        deltaStartTime = -1;
    }

    var redrawScore = function () {
        dom.score.innerHTML = Game.score.toString();
    }

    document.registerElement('sw-hud', {
        prototype: proto
    });

    return Public;

}();
