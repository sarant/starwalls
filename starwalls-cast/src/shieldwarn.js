var ShieldWarningOverlay = function () {

    var Public = {};

    var red = 'rgba(255, 0, 0, 0.5)';
    var defaultExtent = 0.07;
    var size;

    var px = function (num) {
        return num + 'px';
    }

    var linearGradient = function (direction) {
        return 'linear-gradient(to ' + direction + ', transparent, ' + red + ')';
    }

    var radialGradient = function (x, y) {
        return 'radial-gradient(circle ' + px(size) + ' at ' + px(x) + ' ' + px(y) + ', transparent, ' + red + ')';
    }

    Public.createInto = function (div, extent) {
        if (typeof extent == 'undefined') {
            extent = defaultExtent;
        }

        size = Math.ceil(extent * div.offsetWidth, extent * div.offsetHeight);

        var topleft = document.createElement('div');
        topleft.style.position = 'absolute';
        topleft.style.top = px(0);
        topleft.style.left = px(0);
        topleft.style.width = px(size);
        topleft.style.height = px(size);
        topleft.style.background = radialGradient(size, size);
        div.appendChild(topleft);

        var top = document.createElement('div');
        top.style.position = 'absolute';
        top.style.top = px(0);
        top.style.left = px(size);
        top.style.width = px(div.offsetWidth - 2 * size);
        top.style.height = px(size);
        top.style.background = linearGradient('top');
        div.appendChild(top);

        var topright = document.createElement('div');
        topright.style.position = 'absolute';
        topright.style.top = px(0);
        topright.style.right = px(0);
        topright.style.width = px(size);
        topright.style.height = px(size);
        topright.style.background = radialGradient(0, size);
        div.appendChild(topright);

        var left = document.createElement('div');
        left.style.position = 'absolute';
        left.style.top = px(size);
        left.style.left = px(0);
        left.style.width = px(size);
        left.style.height = px(div.offsetHeight - 2 * size);
        left.style.background = linearGradient('left');
        div.appendChild(left);

        var right = document.createElement('div');
        right.style.position = 'absolute';
        right.style.top = px(size);
        right.style.right = px(0);
        right.style.width = px(size);
        right.style.height = px(div.offsetHeight - 2 * size);
        right.style.background = linearGradient('right');
        div.appendChild(right);

        /*
        var bottomleft = document.createElement('div');
        bottomleft.style.position = 'absolute';
        bottomleft.style.bottom = px(0);
        bottomleft.style.left = px(0);
        bottomleft.style.width = px(size);
        bottomleft.style.height = px(size);
        bottomleft.style.background = radialGradient(size, 0);
        div.appendChild(bottomleft);

        var bottom = document.createElement('div');
        bottom.style.position = 'absolute';
        bottom.style.bottom = px(0);
        bottom.style.left = px(size);
        bottom.style.width = px(div.offsetWidth - 2 * size);
        bottom.style.height = px(size);
        bottom.style.background = linearGradient('bottom');
        div.appendChild(bottom);

        var bottomright = document.createElement('div');
        bottomright.style.position = 'absolute';
        bottomright.style.bottom = px(0);
        bottomright.style.right = px(size);
        bottomright.style.width = px(size);
        bottomright.style.height = px(size);
        bottomright.style.background = radialGradient(0, 0);
        div.appendChild(bottomright);
        */
    }

    return Public;

}();
