#!/bin/bash

closure --externs cast_extern.js -O advanced \
    log.js \
    bootstrap.js \
    cast.js \
    inbound.js \
    outbound.js \
    socket.js \
    graphics.js \
    videostream.js \
    shieldwarn.js \
    hud.js \
    curtains.js \
    stage.js \
    game.js > starwalls.min.js

closure --formatting PRETTY_PRINT -O advanced \
    log.js \
    bootstrap.js \
    inbound.js \
    outbound.js \
    socket.js \
    graphics.js \
    videostream.js \
    shieldwarn.js \
    hud.js \
    curtains.js \
    stage.js \
    game.js \
    manual.onload.js > manual.min.js
